#!/usr/bin/env python3

from __future__ import print_function

import paramiko
import re
import json

files_to_watch = ['/var/log/cloud-init-output.log:1', '/run/cloud-init/result.json:9']


class InstancesMonitorUbuntu20_04:
    def __init__(self, image_launcher):
        self.image_launcher = image_launcher


    def instance_steps(self):
        return files_to_watch


    def get_instance_state(self, image_launcher, instance_id, ssh_transport):
        for ubuntu_step in files_to_watch:
            current_state = ""
            if ubuntu_step in image_launcher.instance_step_states[instance_id]:
                current_state = image_launcher.instance_step_states[instance_id][ubuntu_step]

            if current_state == image_launcher.STATE__FAILED:
                return image_launcher.STATE__FAILED

            if current_state == image_launcher.STATE__COMPLETED:
                continue

            if ubuntu_step == '/var/log/cloud-init-output.log:1':
                scp_client = paramiko.sftp_client.SFTP.from_transport(ssh_transport)
                try:
                    fh = scp_client.open("/var/log/cloud-init-output.log")
                    data = "".join(fh.readlines())
                    fh.close()
                    m = re.search(   "(main.py\[WARNING\]: Stdout, stderr changing to \(> /var/log/cloud-init.log, > /var/log/cloud-init.log\))", data)
                    if m is not None:
                        state = image_launcher.STATE__COMPLETED
                        #print("  - instance %s has %s the step, `%s`" %(instance_id, state, ubuntu_step))
                        image_launcher.instance_step_states[instance_id][ubuntu_step] = state
                        continue
                    else:
                        image_launcher.instance_step_states[instance_id][ubuntu_step] = image_launcher.STATE__RETRY
                        return image_launcher.STATE__RETRY
                except FileNotFoundError:
                    image_launcher.instance_step_states[instance_id][ubuntu_step] = image_launcher.STATE__RETRY
                    return image_launcher.STATE__RETRY

            if ubuntu_step == '/run/cloud-init/result.json:9':
                ssh_transport.open_session().exec_command('sudo chmod a+rwX /run/cloud-init/result.json')
                scp_client = paramiko.sftp_client.SFTP.from_transport(ssh_transport)
                try:
                    fh = scp_client.open("/run/cloud-init/result.json")
                    data = "".join(fh.readlines())
                    fh.close()
                    dataj = json.loads(data)

                    if "errors" in dataj["v1"] and len(dataj["v1"]["errors"]) > 0:
                        image_launcher.instance_step_states[instance_id][ubuntu_step] = image_launcher.STATE__FAILED
                        return image_launcher.STATE__FAILED
                    image_launcher.instance_step_states[instance_id][ubuntu_step] = image_launcher.STATE__COMPLETED
                    continue
                except IOError:
                    # Permission denied.
                    image_launcher.instance_step_states[instance_id][ubuntu_step] = image_launcher.STATE__RETRY
                    return image_launcher.STATE__RETRY
                except FileNotFoundError:
                    # file not there yet.
                    image_launcher.instance_step_states[instance_id][ubuntu_step] = image_launcher.STATE__RETRY
                    return image_launcher.STATE__RETRY

            raise Exception("NOT HANDLED: " + ubuntu_step)

        return image_launcher.STATE__COMPLETED

