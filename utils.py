
import json
import hashlib
import base64
import boto3
import os.path
import praws.utils


def as_nice_json(data):
    return json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))


def get_lambda_signature(zip_file_bytes):
    """
    Determine code_size and code_sha256 for the bytes of a zip file.

    :param zip_file_bytes:
    :return:
    """
    code_size = len(zip_file_bytes)
    code_sha256 = base64.encodebytes(hashlib.sha256(zip_file_bytes).digest()).replace(b"\n", b"").decode('ascii')
    return code_sha256, code_size


def get_s3_zip_lambda_signature(s3_bucket, s3_key, s3_client=None):
    """
    Determines the S3 lambda zip signature.

    :param s3_bucket:
    :param s3_key:
    :param s3_client:
    :return:
    """
    if s3_client is None:
        s3_client = boto3.client("s3")
    r = s3_client.get_object(Bucket=s3_bucket, Key=s3_key)
    return get_lambda_signature(r['Body'].read())


def cached_get_s3_zip_lambda_signature(s3_bucket, s3_key, cache_filename, use_cache=True, s3_client=None):
    s3_url = "s3://%s/%s" % (s3_bucket, s3_key)

    cache_content = {}
    if cache_filename is not None and use_cache and os.path.isfile(cache_filename):
        with open(cache_filename, "r") as f:
            cache_content = json.load(f)
        if s3_url in cache_content:
            return cache_content[s3_url]['CodeSha256'], cache_content[s3_url]['CodeSize']

    code_sha256, code_size = get_s3_zip_lambda_signature(s3_bucket, s3_key, s3_client)

    cache_content[s3_url] = {'CodeSha256': code_sha256, 'CodeSize': code_size}
    with open(cache_filename, "w") as f:
        f.write(as_nice_json(cache_content))

    return cache_content[s3_url]['CodeSha256'], cache_content[s3_url]['CodeSize']


def find_matches_in_cache(code_sha256, code_size, cache_filename):
    with open(cache_filename, "r") as f:
        cache_content = json.load(f)
    matching_s3_keys = []
    for s3_key in cache_content:
        if cache_content[s3_key]['CodeSha256'] == code_sha256 and cache_content[s3_key]['CodeSize'] == code_size:
            matching_s3_keys.append(s3_key)

    return matching_s3_keys


def lambda_args_to_policy(sid, resource_arn, aa):
    if aa['Principal'] in["s3.amazonaws.com", "sns.amazonaws.com", "events.amazonaws.com", 'apigateway.amazonaws.com']:
        policy =  {
            'Sid': sid,
            'Resource': resource_arn,
            "Principal": { "Service": aa['Principal'] },
            "Effect": "Allow",
            "Action": aa["Action"],
            "Condition": {
                "ArnLike": {  "AWS:SourceArn": aa["SourceArn"] }
            }
        }
        if 'SourceAccount' in aa:
            policy['Condition']['StringEquals'] = { "AWS:SourceAccount": aa["SourceAccount"] }
        return policy

    return {
        "PrawsTODO": "This still needs implementing",
        "callingArgs": aa
    }
