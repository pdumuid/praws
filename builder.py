#!/usr/bin/env python3

import hashlib
import boto3
import botocore
import copy
import deepdiff
import json
import os
import praws.exceptions as exceptions
import re
import sys
import time
from functools import reduce
from pprint import pprint
from collections import defaultdict
import collections

from praws.route53_utils import get_best_parent_hosted_zone_for_dn
from praws.route53_utils import get_hosted_zones_for_domain_name
from praws.utils import as_nice_json
from six import string_types
from termcolor import colored
import datetime

from praws.exceptions import SNSNotFoundException
from praws.exceptions import SQSNotFoundException
from praws.exceptions import VPCParameterReplacementException
import praws.utils
from praws.builder_apigateway import APIGatewayBuilder


# Used by the reduce function to create a dict key'ed by the value of 'State' within records.
def _state_grouper(s, r):
    state = r['State']
    if state not in s:
        s[state]= []
    s[state].append(r)
    return s


class Builder:
    def __init__(
            self, region_name, system_tag_value, use_unicode=False, boto3_sts_role_arns=None,
            boto3_credentials_json_file=None,
            log_fn=print
    ):
        """[summary]

        :param boto3_sts_role_arns: this is an optional list of roles to assume to get a final role used to play the steps, defaults to None
        :type boto3_sts_role_arns: list of str, optional
        """
        self.region_name = region_name
        self.show_verify_details = False
        self.system_tag_value = system_tag_value

        self._organization_record = None
        self._all_accounts_in_organization = None

        self.boto3_sts_role_arns = boto3_sts_role_arns
        self.boto3_credentials_json_file = boto3_credentials_json_file
        self._boto3_sts_sessions = []
        self.boto3_session = None

        self._boto_clients = {}
        self._boto_resources = {}

        self._current_sns_topic_arn = None
        self._aws_account_identity = None

        self._current_iam_role_name = None
        self._current_sqs_queue_url = None
        self._current_sqs_queue_arn = None

        self.steps = []

        self.hosted_zones = None

        self._api_gateway_rest_apis_by_name = None

        self._sns_topic_arns_by_name = {}
        self._sqs_queue_arns_by_name = {}
        self._elbv2_target_group_arns_by_name = {}
        self._iam_server_certificate_arns_by_name = {}

        self._acm_certificate_summary_list = None
        self._acm_certificates_by_tag_name = {}
        self._acm_certificate_tags_by_arn = {}

        if use_unicode:
            self.plus = colored(u" \u2713 ", 'green', attrs=['bold'])
            self.s_info = colored(u"(\u2139)", 'grey', attrs=['bold'])
            self.s_cross = colored(u" \u2718 ", 'red', attrs=['bold'])
        else:
            self.plus = " + "
            self.s_info = colored("(i)", 'grey', attrs=['bold'])
            self.s_cross = colored(" - ", 'red', attrs=['bold'])

        self.temp_dir = "/tmp/"
        self.delta_dir = None
        self.log_fn = log_fn
        self._vpn_ids_by_name = {}
        self._subnets_in_vpc = {}
        self._security_groups_in_vpc = {}

        self._builders = {
            'apigateway': APIGatewayBuilder(self)
        }

    def _get_lambda_cache_filename(self):
        return self.temp_dir + "/lambda-signature-cache.json"

    def log_fail(self, message):
        self._logs_failures.append(message)
        self.log_fn(" {crossIcon} {message}".format(crossIcon=self.s_cross, message=message))

    def log_pass(self, message):
        self._logs_passes.append(message)
        self.log_fn(" {passIcon} {message}".format(passIcon=self.plus, message=message))

    def log_info(self, message):
        self._logs_info.append(message)
        self.log_fn(" {icon} {message}".format(icon=self.s_info, message=message))

    def clear_logs(self):
        self._logs_failures = []
        self._logs_passes = []
        self._logs_info = []

    def get_logs(self):
        return {
            "failures": copy.deepcopy(self._logs_failures),
            "passes": copy.deepcopy(self._logs_passes),
            "info": copy.deepcopy(self._logs_info),
        }

    def _get_vpc_id_for_name(self, vpc_name):
        if vpc_name not in self._vpn_ids_by_name:
            r = self._get_boto_client('ec2').describe_vpcs(Filters=[{'Name': 'tag:Name', 'Values': [vpc_name]}])
            if len(r.get("Vpcs")) == 0:
                return None
            if len(r.get("Vpcs")) > 1:
                raise Exception("Got too many VPC for the name `%s`" % vpc_name)
            self._vpn_ids_by_name[vpc_name] = r.get("Vpcs")[0]['VpcId']
        return self._vpn_ids_by_name[vpc_name]

    def _get_subnets_in_vpc_id(self, vpc_id):
        if vpc_id not in self._subnets_in_vpc:
            r = self._get_boto_client('ec2').describe_subnets(
                Filters=[
                    {
                        'Name': 'vpc-id',
                        'Values': [vpc_id]
                    }
                ]
            )
            self._subnets_in_vpc[vpc_id] = Builder.subnet_response_to_keyed_list(r.get('Subnets', []))
        return self._subnets_in_vpc[vpc_id]

    def _get_security_groups_in_vpc_id(self, vpc_id):
        if vpc_id not in self._security_groups_in_vpc:
            r = self._get_boto_client('ec2').describe_security_groups(
                Filters=[
                    {
                        'Name': 'vpc-id',
                        'Values': [vpc_id]
                     }
                ]
            )
            self._security_groups_in_vpc[vpc_id] = {x['GroupName']: x for x in r.get('SecurityGroups', [])}
        return self._security_groups_in_vpc[vpc_id]

    def get_session_for_boto3_sts_role_arns(self):
        if self.boto3_session is None:
            self._boto3_sts_sessions = []

            last_session = self._get_root_boto3_session()
            self._boto3_sts_sessions.append(last_session)

            for role_arn in self.boto3_sts_role_arns:
                self.log_fn("Assuming role - %s" % role_arn)
                r = last_session.client('sts').assume_role(RoleArn=role_arn, RoleSessionName='PRAWS-Builder')
                last_session = boto3.session.Session(
                    aws_access_key_id=r['Credentials']['AccessKeyId'],
                    aws_secret_access_key=r['Credentials']['SecretAccessKey'],
                    aws_session_token=r['Credentials']['SessionToken']
                )
                self._boto3_sts_sessions.append(last_session)

            self.boto3_session = last_session

        return self.boto3_session

    def _get_root_boto3_session(self):
        if self.boto3_credentials_json_file is None:
            boto3.setup_default_session()
            return boto3.DEFAULT_SESSION

        with open(self.boto3_credentials_json_file, 'r') as f:
            r = json.load(f)
            args = {
                'aws_access_key_id': r['Credentials']['AccessKeyId'],
                'aws_secret_access_key': r['Credentials']['SecretAccessKey'],
                'aws_session_token': r['Credentials']['SessionToken']
            }
            return boto3.session.Session(**args)

    def _get_boto_client(self, service_name, region_name=None):
        if region_name is None:
            service_key = "%s@%s" % (service_name, region_name)
            region_name = self.region_name
        else:
            service_key = service_name

        if service_key not in self._boto_clients:
            if self.boto3_sts_role_arns is not None:
                boto3_session = self.get_session_for_boto3_sts_role_arns()
            else:
                boto3_session = self._get_root_boto3_session()

            self._boto_clients[service_key] = boto3_session.client(service_name, region_name=region_name)

        return self._boto_clients[service_key]

    def _get_boto_client_for_step(self, service_name, step_config):
        return self._get_boto_client(service_name, region_name=step_config.get('aws_region_name', None))

    def show_shortkeys(self):
        self.log_fn("The possible short keys are:")
        for step in self.steps:
            self.log_fn("  * " + step['shortKey'])

    def get_step_for_shortkey(self, short_key):
        for step in self.steps:
            if step['shortKey'] == short_key:
                return step
        raise ValueError("Unable to find step, with short key `" + short_key + "`")

    def play_for_shortkey(self, short_key):
        self.play_step(self.get_step_for_shortkey(short_key))

    def verify_for_shortkey(self, short_key):
        for step in self.steps:
            if step['shortKey'] == short_key:
                return self.verify_step(step)
        raise ValueError("Unable to find step, with short key `" + short_key + "`")

    def update_for_shortkey(self, short_key):
        for step in self.steps:
            if step['shortKey'] == short_key:
                return self.update_step(step)
        raise ValueError("Unable to find step, with short key `" + short_key + "`")

    def revert_for_shortkey(self, short_key):
        self.revert_step(self.get_step_for_shortkey(short_key))

    def play_step(self, step_config):
        """
        Play a single step.

        :param step_config: The parameters for a single step.
        :return:
        """
        self.log_fn(
            colored("Playing step", 'green')
            + ("%-70s" % colored(" : " + step_config['shortKey'], "white"))
            + " - " + step_config['description']
        )
        self._play_step(step_config)

    def verify_step(self, step_config):
        self.log_fn(
            colored("Verifying step", 'green')
            + ("%-70s" % colored(" : " + step_config['shortKey'], "white"))
            + " - " + step_config['description']
        )
        return self._verify_step(step_config)

    def update_step(self, step_config, is_substep=False):
        if not is_substep:
            self.log_fn(
                colored("Updating for step", 'green')
                + ("%-70s" % colored(" : " + step_config['shortKey'], "white"))
                + " - " + step_config['description']
            )
        else:
            self.log_fn(
                colored(" => Update sub-step:", 'green') + " - " + step_config['description']
            )
        return self._update_step(step_config)

    def revert_step(self, step_config):
        self.log_fn(
            colored("Reverting step", 'green')
            + ("%-70s" % colored(" : " + step_config['shortKey'], "white"))
            + " - " + step_config['description']
        )
        return self._revert_step(step_config)

    def replacer(self, item, ignore_not_found=False):
        """
        This function replaces strings in list or dictionaries and populates them with the associated AWS resource IDs.

        e.g.

        {
            foo:[
                 "__SUBNETS_RX__[.*Public.*]"
            ]
        }

        will be converted to
        {
            foo:[
                 "subnet-e0d1b885",
                 "subnet-2f8de34a"
            ]
        }

        where subnets with id, subnet-2f8de34a, and subnet-e0d1b885 match the regexp ".*Public.*"

        :param item:
        :param ignore_not_found: boolean Ignore when a replacement is not found.
        :return:
        """
        if isinstance(item, dict):
            return self.dict_replacer(item, ignore_not_found)

        if isinstance(item, list):
            return self.list_replacer(item, ignore_not_found)

        if isinstance(item, string_types):
            result, keep_replacing = self.string_replacer(item, ignore_not_found)
            return result

        # Otherwise...
        return item


    def _get_api_gateway_rests_api_by_name(self):
        if self._api_gateway_rest_apis_by_name is None:
            apigc = self._get_boto_client('apigateway')
            rest_apis_by_name = defaultdict(list)
            for x in apigc.get_paginator('get_rest_apis').paginate():
                for r in x.get('items', []):
                    rest_apis_by_name[r['name']].append({'id': r['id'], 'type': 'rest'})

            self._api_gateway_rest_apis_by_name = rest_apis_by_name
        return self._api_gateway_rest_apis_by_name

    def dict_replacer(self, item, ignore_not_found=False):
        new_item = {}
        for key, value in item.items():
            new_item[key] = self.replacer(item[key], ignore_not_found)
        return new_item

    def _get_account_id(self):
        if self._aws_account_identity is None:
            self._aws_account_identity = self._get_boto_client('sts').get_caller_identity().get('Account')
        return self._aws_account_identity

    def string_replacer(self, item, ignore_not_found=False):
        Response = collections.namedtuple("Response", "result keep_replacing")

        item, keep_replacing = self._builders['apigateway'].string_replacer(item, ignore_not_found)
        if not keep_replacing:
            return Response(item, keep_replacing)

        if "__ACCOUNT_ID__" in item:
            item = str.replace(item, "__ACCOUNT_ID__", self._get_account_id())

        if "__AWS_REGION__" in item:
            item = str.replace(item, "__AWS_REGION__", self.region_name)

        m = re.search("^__ROUTE53_HOSTED_ZONE_ARN__\[(.*)\]$", item)
        if m is not None:
            domain_name = m.group(1)
            self.hosted_zones = self._get_boto_client('route53').list_hosted_zones()['HostedZones']
            for hosted_zone in self.hosted_zones:
                if hosted_zone['Name'] == domain_name:
                    hosted_zone_id_path = re.sub("^/+", "", hosted_zone['Id'])
                    return Response("arn:aws:route53:::%s" % hosted_zone_id_path, keep_replacing=False)
            if ignore_not_found:
                return Response(item, keep_replacing=False)
            raise VPCParameterReplacementException(
                "Replacement, `__ROUTE53_HOSTED_ZONE_ARN__[%s]` not found" % domain_name
            )

        m = re.search("^__ROUTE53_HOSTED_ZONE_ID__\[(.*)\]$", item)
        if m is not None:
            domain_name = m.group(1)
            if self.hosted_zones is None:
                self.hosted_zones = self._get_boto_client('route53').list_hosted_zones()['HostedZones']
            for hosted_zone in self.hosted_zones:
                if hosted_zone['Name'] == domain_name:
                    return Response(hosted_zone['Id'], keep_replacing=False)
            if ignore_not_found:
                return Response(item, keep_replacing=False)
            raise VPCParameterReplacementException(
                "Replacement, `__ROUTE53_HOSTED_ZONE_ID__[%s]` not found" % domain_name
            )

        m = re.search("^__ORGANIZATION_TREE_ID__\[(.+)\]$", item)
        if m is not None:
            parents = m.group(1).split("/")
            if parents[0] != "Root":
                raise Exception("First item in tree_id must be root")
            client = self._get_boto_client('organizations')
            r = client.list_roots()
            if len(r["Roots"]) != 1:
                raise Exception("Not sure how to deal with non-1 roots.")
            last_parent_id = r["Roots"][0]["Id"]

            last_parent_idx = 0
            while last_parent_idx < len(parents) - 1:
                last_parent_idx += 1
                r2 = client.list_organizational_units_for_parent(ParentId=last_parent_id)
                matching_child_units = (list(filter(lambda r:r['Name'] == parents[last_parent_idx], r2["OrganizationalUnits"])))
                if len(matching_child_units) != 1:
                    raise Exception("Unable to find child OU with the name, %s" % parents[last_parent_idx])
                last_parent_id = matching_child_units[0]["Id"]

            return Response(last_parent_id, keep_replacing=False)

        m = re.search("^__ORGANIZATION_ACCOUNTS_CURRENT_PARENT_ID__\[(\d+)\]$", item)
        if m is not None:
            account_id = m.group(1)

            client = self._get_boto_client('organizations')
            try:
                rp = client.list_parents(ChildId=account_id)
                return Response(rp["Parents"][0]["Id"], keep_replacing=False)
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ChildNotFoundException":
                    if ignore_not_found:
                        return Response("", keep_replacing=False)
                    raise exceptions.AccountNotInOrganization("Account ID could not be found in organization.. Cannot get the current parent.")
                else:
                    raise e

        m = re.search("__API_GATEWAY__REST_API_ID__\[([^\]]+)\]", item)
        if m is not None:
            gateway_name = m.group(1)
            string_to_replace = '__API_GATEWAY__REST_API_ID__[%s]' % gateway_name
            rest_apis_by_name = self._get_api_gateway_rests_api_by_name()
            if gateway_name in rest_apis_by_name:
                if len(rest_apis_by_name[gateway_name]) > 1:
                    raise Exception("ERROR: There is more than one API called `%s`" % gateway_name)
                item = str.replace(item, string_to_replace, rest_apis_by_name[gateway_name][0]['id'])
            else:
                if not ignore_not_found:
                    raise exceptions.AccountNotInOrganization("Account ID could not be found in organization.. Cannot get the current parent.")

        if item == '__CURRENT_SQS_QUEUE_URL__':
            if self._current_sqs_queue_url is None:
                if ignore_not_found:
                    return Response(item, keep_replacing=False)
                raise VPCParameterReplacementException(
                    "Replacement, `__CURRENT_SQS_QUEUE_URL__` not possible when outside of a sub-step"
                )

            return Response(self._current_sqs_queue_url, keep_replacing=False)

        if item == '__CURRENT_IAM_ROLE_NAME__':
            if self._current_iam_role_name is None:
                if ignore_not_found:
                    return Response(item, keep_replacing=False)
                raise VPCParameterReplacementException(
                    "Replacement, `__CURRENT_IAM_ROLE_NAME__` not possible when outside of a sub-step"
                )
            return Response(self._current_iam_role_name, keep_replacing=False)

        if item == '__CURRENT_SQS_QUEUE_ARN__':
            if self._current_sqs_queue_arn is None:
                if ignore_not_found:
                    return Response(item, keep_replacing=False)
                raise VPCParameterReplacementException(
                    "Replacement, `__CURRENT_SQS_QUEUE_ARN__` not possible when outside of a sub-step"
                )

            return Response(self._current_sqs_queue_arn, keep_replacing=False)

        m = re.search("^__VPC_ID__\[(.*)\]$", item)
        if m is not None:
            vpc_name = m.group(1)
            vpc_id = self._get_vpc_id_for_name(vpc_name)
            if vpc_id is None:
                raise VPCParameterReplacementException("Could not determine the VPC with the name, `%s`" % vpc_name)
            return Response(vpc_id, keep_replacing=False)

        m = re.search("^__VPC_SECURITY_GROUP__\[(?P<vpc_name>.*)//(?P<security_group_name>.*)\]__$", item)
        if m is not None:
            item_parts = m.groupdict()
            vpc_id = self._get_vpc_id_for_name(item_parts['vpc_name'])
            if vpc_id is None:
                raise VPCParameterReplacementException("Could not determine the VPC with the name, `%s`" % item_parts['vpc_name'])

            all_security_groups = self._get_security_groups_in_vpc_id(vpc_id)
            if item_parts['security_group_name'] not in all_security_groups:
                raise VPCParameterReplacementException("Could not find the security group with the name, `%s`" % item_parts['security_group_name'])

            return Response(all_security_groups[item_parts['security_group_name']]['GroupId'], keep_replacing=False)

        m = re.search("^__ELBV2_TARGET_GROUP_ARN__\[(?P<target_group_name>.*)\]$", item)
        if m is not None:
            target_group_name = m.groupdict()['target_group_name']
            if target_group_name not in self._elbv2_target_group_arns_by_name:
                client = self._get_boto_client('elbv2')
                try:
                    r = client.describe_target_groups(Names=[target_group_name])
                except botocore.exceptions.ClientError as e:
                    if e.response['Error']['Code'] == "TargetGroupNotFound":
                        if ignore_not_found:
                            return Response(item, keep_replacing=False)
                        raise VPCParameterReplacementException("Could not find the target group name, `%s`" % target_group_name)
                    raise e

                self._elbv2_target_group_arns_by_name[target_group_name] = r['TargetGroups'][0]['TargetGroupArn']
            return Response(self._elbv2_target_group_arns_by_name[target_group_name], keep_replacing=False)

        m = re.search("^__VPC_SUBNET_REGEX_TO_ID_LIST__\[(?P<vpc_name>.*)//(?P<subnet_regexp>.*)\]__$", item)
        if m is not None:
            item_parts = m.groupdict()
            vpc_id = self._get_vpc_id_for_name(item_parts['vpc_name'])
            if vpc_id is None:
                raise VPCParameterReplacementException("Could not determine the VPC with the name, `%s`" % item_parts['vpc_name'])

            all_subnets = self._get_subnets_in_vpc_id(vpc_id)
            re_subnet_regexp = re.compile(item_parts['subnet_regexp'])
            the_list = []
            for praws_subnet_name in all_subnets:
                if re_subnet_regexp.match(praws_subnet_name):
                    the_list.append(all_subnets[praws_subnet_name]['SubnetId'])

            return Response(the_list, keep_replacing=False)

        m = re.search("^__SQS_QUEUE_ARN__\[(.*)\]$", item)
        if m is not None:
            queue_name = m.group(1)
            if queue_name in self._sqs_queue_arns_by_name:
                return Response(self._sqs_queue_arns_by_name[queue_name], keep_replacing=False)

            self.log_replace_lookup("sqsqueue.")
            self._sqs_queue_arns_by_name[queue_name] = self._get_sqs_queue_arn(queue_name)
            return Response(self._sqs_queue_arns_by_name[queue_name], keep_replacing=False)

        if item == '__CURRENT_SNS_TOPIC_ARN__':
            if self._current_sns_topic_arn is None:
                if ignore_not_found:
                    return Response(item, keep_replacing=False)
                raise VPCParameterReplacementException(
                    "Replacement, `__CURRENT_SNS_TOPIC_ARN__` not possible when outside of a sub-step"
                )

            return Response(self._current_sns_topic_arn, keep_replacing=False)

        m = re.search("^__SNS_TOPIC_ARN__\[(.*)\]$", item)
        if m is not None:
            topic_name = m.group(1)
            if topic_name in self._sns_topic_arns_by_name:
                return Response(self._sns_topic_arns_by_name[topic_name], keep_replacing=False)

            self.log_replace_lookup("snstopic.")
            try:
                self._sns_topic_arns_by_name[topic_name] = self._get_sns_topic_arn(topic_name)
            except SNSNotFoundException as e:
                if ignore_not_found:
                    return Response(item, keep_replacing=False)
                raise e

            return Response(self._sns_topic_arns_by_name[topic_name], keep_replacing=False)

        if item == '__SYSTEM_TAG_VALUE__':
            return Response(self.system_tag_value, keep_replacing=False)

        m = re.search("^__SSL_CERTIFICATE_ID__\[(.*)\]$", item)
        if m is not None:
            ssl_certificate_name = m.group(1)
            return Response(self.get_ssl_certificate_id_for_name(ssl_certificate_name), keep_replacing=False)

        m = re.search("^__ACM_CERTIFICATE_ARN__\[(?P<certificate_tag_name>.*)\]$", item)
        if m is not None:
            certificate_tag_name = m.groupdict()['certificate_tag_name']
            cert_arn = self.get_acm_certificates_for_tag_name(certificate_tag_name)
            if cert_arn is None:
                raise VPCParameterReplacementException('Unable to find ACM certificate with the tag Name having a value of `%s`' % certificate_tag_name)
            return Response(cert_arn, keep_replacing=False)

        # Otherwise..
        return Response(item, keep_replacing=True)

    def list_replacer(self, item, ignore_not_found=False):
        new_item = []
        for value in item:
            new_item.append(self.replacer(value, ignore_not_found))
        return new_item

    # ==================================== VERIFY STEPS ====================================
    def _verify_step(self, step_config):
        self.clear_logs()
        if step_config['client'] == 'apigateway':
            return self._builders['apigateway']._verify_step(step_config)

        if step_config['client'] == 'ecs':
            return self._verify_step_ecs(step_config)

        if step_config['client'] == 'elbv2':
            return self._verify_step_elbv2(step_config)

        if step_config['client'] == 'events':
            return self._verify_step_cloudwatch_events(step_config)

        if step_config['client'] == 'iam':
            return self._verify_step_iam(step_config)

        if step_config['client'] == 'lambda':
            return self._verify_step_lambda(step_config)

        if step_config['client'] == 'logs':
            return self._verify_step_cloud_watch_logs(step_config)

        if step_config['client'] == 's3':
            return self._verify_step_s3(step_config)

        if step_config['client'] == 'sdb':
            return self._verify_step_sdb(step_config)

        if step_config['client'] == 'ses':
            return self._verify_step_ses(step_config)

        if step_config['client'] == 'sns':
            return self._verify_step_sns(step_config)

        if step_config['client'] == 'sqs':
            return self._verify_step_sqs(step_config)

        if step_config['client'] == 'route53':
            return self._verify_step_route53(step_config)

        if step_config['client'] == 'organizations':
            return self._verify_step_organizations(step_config)

        raise ValueError("Verify step for boto client `%s` not implemented yet" % step_config['client'])

    def _verify_step_ecs(self, step_config):

        client = self._get_boto_client('ecs')
        function_name = step_config['function']

        # VERIFY::iam::create_cluster
        if function_name == 'create_cluster':
            arguments = copy.copy(step_config['arguments'])
            arguments = self.replacer(arguments)
            cluster_name = arguments['clusterName']
            try:
                cluster_instance_arns = client.list_container_instances(cluster=cluster_name)['containerInstanceArns']
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ClusterNotFoundException":
                    self.log_fail("Cluster does not exist")
                    return
                else:
                    raise e
            self.log_pass("Cluster exists")
            return

        # VERIFY::ecs::create_service
        if function_name == 'create_service':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            cluster_name = arguments['cluster']
            service_name = arguments['serviceName']
            r = client.describe_services(cluster=cluster_name, services=[service_name])

            if len(list(filter(lambda x: x['status'] == "ACTIVE", r['services']))) == 0:
                self.log_fail("There are no services with the name, `%s` in the cluster, `%s`" % (service_name, cluster_name))
                return
            self.log_pass("The service, `%s` exists in the cluster, `%s`" % (service_name, cluster_name))

            desired_step_details = {
                'cluster': cluster_name, 'service_name': service_name
            }
            current_step_details = {
                'cluster': cluster_name, 'service_name': service_name
            }

            task_arn_comment = ""
            desired_task_definition_name = arguments['taskDefinition']
            if 'arn:aws:ecs' in desired_task_definition_name:
                desired_task_definition_arn = rx['taskDefinitionArns'][0]

            elif ':' in desired_task_definition_name:
                desired_task_definition_arn = "arn:aws:ecs:%s:%s:task-definition/%s" % (
                    self.region_name,
                    self._get_account_id(),
                    desired_task_definition_name
                )
            else:
                task_arn_comment = " (latest revision of family)"
                rx = client.list_task_definitions(
                    familyPrefix=desired_task_definition_name,
                    status='ACTIVE',
                    sort='DESC',
                    maxResults=3
                )
                desired_task_definition_arn = rx['taskDefinitionArns'][0]

            current_task_definition_arn = r['services'][0]['taskDefinition']

            if current_task_definition_arn == desired_task_definition_arn:
                self.log_pass(
                    "The service is using the desired task definition, `%s`%s." % (
                        desired_task_definition_arn, task_arn_comment
                    )
                )
            else:
                self.log_fail("The service is not using the desired task definition arn, `%s`." % (desired_task_definition_arn))

            desired_step_details['taskDefinition'] = desired_task_definition_arn
            current_step_details['taskDefinition'] = current_task_definition_arn

            desired_step_details['loadBalancers'] = arguments.get('loadBalancers', [])
            current_step_details['loadBalancers'] = r['services'][0].get('loadBalancers', [])

            dd = deepdiff.DeepDiff(desired_step_details['loadBalancers'], current_step_details['loadBalancers'], ignore_order=True)
            if len(dd) == 0:
                self.log_pass("The 'loadBalancers' definitions match.")
            else:
                self.log_fail("The 'loadBalancers' definitions do not match.")

            desired_step_details['launchType'] = arguments.get('launchType', 'EC2')
            current_step_details['launchType'] = r['services'][0]['launchType']
            if desired_step_details['launchType'] == current_step_details['launchType']:
                self.log_pass("The 'launchType' matches.")
            else:
                self.log_fail("The 'launchType' does not matches.")

            if len(desired_step_details['loadBalancers']):
                desired_step_details['healthCheckGracePeriodSeconds'] = arguments.get('healthCheckGracePeriodSeconds', 0)
                current_step_details['healthCheckGracePeriodSeconds'] = r['services'][0]['healthCheckGracePeriodSeconds']
                if current_step_details['healthCheckGracePeriodSeconds'] == current_step_details['healthCheckGracePeriodSeconds']:
                    self.log_pass("The 'healthCheckGracePeriodSeconds' matches.")
                else:
                    self.log_fail("The 'healthCheckGracePeriodSeconds' does not matches.")


            dd = deepdiff.DeepDiff(desired_step_details, current_step_details, ignore_order=True)
            if (len(dd.keys()) != 0):
                self._create_comparison_json_files(
                    "ecs--service--{cluster}--{serviceName}".format(**arguments),
                    desired_step_details, current_step_details,
                    failure_message="Step Properties do NOT match"
                )

            self.log_info("(TODO) There might be more arguments to check.")

            return

        # VERIFY::ecs::register_task_definition
        if function_name == 'register_task_definition':
            arguments = self.replacer(copy.copy(step_config['arguments']))

            r = client.list_task_definitions(familyPrefix=arguments['family'])
            if len(r['taskDefinitionArns']) == 0:
                self.log_fail("There are no active task definitions for the family, `%s`" % arguments['family'])
                return

            desired_step_details = {
                'family': arguments['family'],
            }
            current_step_details = {
                'family': arguments['family'],
            }

            # Get the most recent version:
            r = client.describe_task_definition(taskDefinition=arguments['family'])

            self.log_pass("Task definition for the family, exists - latest is `%s`" % r['taskDefinition']['taskDefinitionArn'])
            task_def = copy.copy(r['taskDefinition'])

            if 'taskRoleArn' not in arguments:
                self.log_info("No task role Arn desired not sure what to do yet.")
            else:
                desired_step_details['taskRoleArn'] = arguments['taskRoleArn']
                current_step_details['taskRoleArn'] = task_def['taskRoleArn']

                if arguments['taskRoleArn'] == task_def['taskRoleArn']:
                    self.log_pass("The desired task role ({desiredRole}) matches.".format(
                        desiredRole=arguments['taskRoleArn'], actualRole=task_def['taskRoleArn'])
                    )
                else:
                    self.log_fail("The desired task role ({desiredRole}) does not match the actual role ({actualRole}).".format(
                        desiredRole=arguments['taskRoleArn'], actualRole=task_def['taskRoleArn'])
                    )

            # ------------------------------------------------------------ Container Definitions
            actual_container_defs = copy.copy(task_def['containerDefinitions'])
            desired_container_defs = copy.copy(arguments['containerDefinitions'])

            for idx, val in enumerate(actual_container_defs):
                actual_container_defs[idx]['environment'] = list(sorted(actual_container_defs[idx]['environment'], key=lambda x: x['name']))

            for idx, val in enumerate(desired_container_defs):
                desired_container_defs[idx]['environment'] = list(sorted(desired_container_defs[idx]['environment'], key=lambda x: x['name']))

            desired_step_details['containerDefinitions'] = desired_container_defs
            current_step_details['containerDefinitions'] = actual_container_defs

            dd = deepdiff.DeepDiff(desired_container_defs, actual_container_defs, ignore_order=True)
            if len(dd) == 0:
                self.log_pass("The container definitions match.")
            else:
                self.log_fail("The container definitions do NOT match.")

            # ------------------------------------------------------------ Volumes
            actual_volumes = copy.copy(task_def['volumes'])
            desired_volumes = copy.copy(arguments.get('volumes', []))

            current_step_details['volumes'] = actual_volumes
            desired_step_details['volumes'] = desired_volumes

            dd = deepdiff.DeepDiff(desired_volumes, actual_volumes, ignore_order=True)
            if len(dd) == 0:
                self.log_pass("The volumes definitions match.")
            else:
                self.log_fail("The volmes do not match")

            # ------------------------------------------------------------ Other
            current_step_details['placementConstraints'] = task_def['placementConstraints']
            desired_step_details['placementConstraints'] = arguments.get('placementConstraints', [])


            # See https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ecs.html#ECS.Client.register_task_definition
            dd = deepdiff.DeepDiff(desired_step_details, current_step_details, ignore_order=True)
            if (len(dd.keys()) != 0):
                self._create_comparison_json_files(
                    "ecs--taskDefinition--{family}-step".format(**arguments),
                    desired_step_details, current_step_details,
                    failure_message="Step Properties do NOT match"
                )

            # Default value is "bridge"
            self.log_info("There might be more to compare still - networkMode is non-trivial to compare!")
            self.log_info("There might be more to compare still!")
            return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")

    def _verify_step_elbv2(self, step_config):
        client = self._get_boto_client('elbv2')
        function_name = step_config['function']

        # VERIFY::elbv2::create_target_group
        if function_name == 'create_target_group':
            arguments = self.replacer(copy.copy(step_config['arguments']))

            try:
                r = client.describe_target_groups(Names=[arguments['Name']])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "TargetGroupNotFound":
                    self.log_fail("The target group with the name, `{Name}` does not exist.".format(Name=arguments['Name']))
                    return
                raise e
            target_groups = r.get('TargetGroups', [])

            if len(target_groups) > 1:
                raise Exception("Got too many target groups!")

            if len(target_groups) == 0:
                self.log_fail("The target group with the name, `{Name}` does not exist.".format(Name=arguments['Name']))
                return

            self.log_pass("Target Group with name `{Name}` exists".format(Name=arguments['Name']))

            if arguments['Protocol'] in ['HTTP', 'HTTPS']:
                default_attributes = {
                    'TargetType': 'instance',
                    'HealthyThresholdCount': 5,
                    'UnhealthyThresholdCount': 2,
                    "Matcher": { "HttpCode": "200" },
                    'HealthCheckProtocol': "HTTP",
                    'HealthCheckPort': "traffic-port",
                    'HealthCheckPath': "/",
                    'HealthCheckIntervalSeconds': 30,
                    'HealthCheckTimeoutSeconds': 5
                }
            else:
                # See https://boto3.readthedocs.io/en/latest/reference/services/elbv2.html#ElasticLoadBalancingv2.Client.create_target_group
                raise Exception("Default attributes for the protocol, {Protocol} not added yet".format(**arguments))

            desired_attributes = default_attributes
            desired_attributes.update(copy.copy(arguments))
            desired_attributes['TargetGroupName'] = desired_attributes.pop('Name')

            current_attributes = copy.copy(target_groups[0])
            current_attributes.pop('TargetGroupArn')
            current_attributes.pop('LoadBalancerArns')

            dd = deepdiff.DeepDiff(desired_attributes, current_attributes)
            if (len(dd.keys()) == 0):
                self.log_pass("Properties match")
            else:
                self._create_comparison_json_files(
                    "elbv2-targetGroup--{Name}".format(Name=arguments['Name']),
                    desired_attributes, current_attributes,
                    failure_message="Properties do NOT match"
                )
            return

        # VERIFY::elbv2::create_load_balancer
        if function_name == 'create_load_balancer':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            try:
                r = client.describe_load_balancers(Names=[arguments['Name']])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "LoadBalancerNotFound":
                    self.log_fail("The load balancer with the name, `{Name}` does not exist.".format(Name=arguments['Name']))
                    return
                raise e
            self.log_pass("The load balancer with the name, `{Name}` exists.".format(Name=arguments['Name']))

            desired_step_details = {
                'client': 'elbv2',
                'function': 'create_load_balancer'
            }

            current_step_details = {
                'client': 'elbv2',
                'function': 'create_load_balancer'
            }

            # --------------------------------------------------
            # 1. Comparing the create_load_balancer() parameters
            # --------------------------------------------------
            # See https://boto3.readthedocs.io/en/latest/reference/services/elbv2.html#ElasticLoadBalancingv2.Client.create_load_balancer
            default_settings = {'Scheme': "internet-facing", 'Type': 'application'}

            desired_settings = default_settings
            desired_settings.update(copy.copy(arguments))
            current_settings = copy.copy(r['LoadBalancers'][0])

            current_settings['Subnets'] = [az['SubnetId'] for az in current_settings.pop('AvailabilityZones')]
            current_settings['Name'] = current_settings.pop('LoadBalancerName')

            load_balancer_arn = current_settings['LoadBalancerArn']

            # These attributes are derived
            for attribute_to_pop in [
                    'CreatedTime',            # An artifact created by AWS
                    'DNSName',                # An artifact created by AWS based on the name, account etc.
                    'CanonicalHostedZoneId',  # An artifact created by AWS based on the name, account etc.
                    'LoadBalancerArn',        # An artifact created by AWS based on the name, account etc.
                    'State',                  # An artifact created by AWS
                    'VpcId'                   # An artifact created by AWS (related to the subnets)
            ]:
                current_settings.pop(attribute_to_pop)

            current_settings['Subnets'] = list(sorted(current_settings['Subnets']))
            desired_settings['Subnets'] = list(sorted(desired_settings['Subnets']))

            current_settings['SecurityGroups'] = list(sorted(current_settings['SecurityGroups']))
            desired_settings['SecurityGroups'] = list(sorted(desired_settings['SecurityGroups']))

            dd = deepdiff.DeepDiff(desired_settings, current_settings, ignore_order=True)
            if (len(dd.keys()) == 0):
                self.log_pass("settings match")
            else:
                self.log_fail("settings NOT match")

            desired_step_details['arguments'] = desired_settings
            current_step_details['arguments'] = current_settings

            # --------------------------------------------------
            # 2. Comparing the Listeners
            # --------------------------------------------------
            desired_listeners = self.replacer(copy.copy(step_config['listeners']), ignore_not_found=True)
            def _enriched_cert(record):
                try:
                    record['_prawsInjectedTags'] = self._get_acm_certificate_tags(record['CertificateArn'])
                except:
                    pass
                return record

            for i, _ in enumerate(desired_listeners):
                if desired_listeners[i]['arguments']['Protocol'] not in ['HTTP']:
                    desired_listeners[i]['nonDefaultCertificates'] = list(sorted([x for x in desired_listeners[i]['nonDefaultCertificates']], key=lambda r: r.get('CertificateArn', [])))

            current_listeners_1 = client.describe_listeners(LoadBalancerArn=load_balancer_arn)['Listeners']
            current_listeners_1 = [{'arguments': ld} for ld in current_listeners_1]

            current_listeners = []
            for current_listener in current_listeners_1:
                listener_arn = current_listener['arguments'].pop('ListenerArn')
                load_balancer_arn = current_listener['arguments'].pop('LoadBalancerArn')

                # ------------------------------
                # 2.1 The listener (nonDefault) rules
                # ------------------------------
                the_rules_1 = list(sorted(
                    client.describe_rules(ListenerArn=listener_arn)['Rules'],
                    key=lambda r: r['Priority']
                ))
                the_rules = []
                for the_rule in the_rules_1:
                   if the_rule['IsDefault']:
                       continue
                   the_rule.pop('Priority')
                   the_rule.pop('IsDefault')
                   the_rule.pop('RuleArn')
                   the_rules.append(the_rule)

                current_listener['nonDefaultRules'] = the_rules
                current_listeners.append(current_listener)

                # ------------------------------
                # 2.2 The listener (nonDefault) certificates
                # ------------------------------
                if current_listener['arguments']['Protocol'] not in ['HTTP']:
                    the_certs = client.describe_listener_certificates(ListenerArn=listener_arn).get('Certificates', [])

                    def _clean_cert_info(record):
                        record.pop('IsDefault')
                        return record

                    the_certs = list(sorted([_clean_cert_info(x) for x in the_certs if not x['IsDefault']], key=lambda r: r['CertificateArn']))
                    current_listener['nonDefaultCertificates'] = the_certs

            desired_listeners = list(sorted(desired_listeners, key=lambda r: "{Protocol}:{Port}".format(**r['arguments'])))
            current_listeners = list(sorted(current_listeners, key=lambda r: "{Protocol}:{Port}".format(**r['arguments'])))

            dd = deepdiff.DeepDiff(desired_listeners, current_listeners)
            if (len(dd.keys()) == 0):
                self.log_pass("listeners and their rules and certificates match")
            else:
                self.log_fail("listeners and their rules and certificates do NOT match")
                self.log_fn("  - enriching nonDefaultCertificates with the tags to make comparison easier")
                for listener in desired_listeners:
                    for certificate in listener.get('nonDefaultCertificates', []):
                        _enriched_cert(certificate)
                for listener in current_listeners:
                    for certificate in listener.get('nonDefaultCertificates', []):
                        _enriched_cert(certificate)


            current_step_details['listeners'] = current_listeners
            desired_step_details['listeners'] = desired_listeners

            # --------------------------------------------------
            # 3. Comparing the attributes
            # --------------------------------------------------
            default_attributes = {
                'idle_timeout.timeout_seconds': '60',
                'access_logs.s3.enabled': 'false',
                'routing.http2.enabled': 'true',
                "routing.http.drop_invalid_header_fields.enabled": "false",
                'deletion_protection.enabled': 'false',
                'access_logs.s3.bucket': '',
                'access_logs.s3.prefix': ''
            }
            desired_attributes = default_attributes
            desired_attributes.update(self.replacer(copy.copy(step_config.get('attributes', {}))))
            current_attributes = {av['Key']: av['Value'] for av in client.describe_load_balancer_attributes(LoadBalancerArn=load_balancer_arn)['Attributes']}

            dd = deepdiff.DeepDiff(desired_attributes, current_attributes)
            if (len(dd.keys()) == 0):
                self.log_pass("desired attributes match")
            else:
                self.log_fail("desired attributes do NOT match")

            current_step_details['attributes'] = current_attributes
            desired_step_details['attributes'] = desired_attributes

            # --------------------------------------------------
            # Make the step diff file.
            # --------------------------------------------------
            dd = deepdiff.DeepDiff(desired_step_details, current_step_details)
            if (len(dd.keys()) != 0):
                self._create_comparison_json_files(
                    "elbv2-loadBalancer--{Name}--step".format(Name=arguments['Name']),
                    desired_step_details, current_step_details,
                    failure_message=" step does NOT match"
                )
            return

        raise ValueError("Unhandled verification for function, {client}::{function}`".format(**step_config))

    def _verify_step_cloudwatch_events(self, step_config):
        client = self._get_boto_client('events')
        function_name = step_config['function']

        # VERIFY::events::put_rule
        if function_name == "put_rule":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            try:
                r = client.describe_rule(Name=(arguments['Name']))
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ResourceNotFoundException":
                    self.log_fail("The event rule, `%s` does not exist." % arguments['Name'])
                    return
                else:
                    raise e

            desired_step_details = {}
            current_step_details = {}

            self.log_pass("The rule, `%s` exists." % arguments['Name'])
            desired_step_details['arguments'] = {
                'Name': arguments['Name'],
                'Description': arguments.get('Description', ''),
                'ScheduleExpression': arguments.get('ScheduleExpression', ''),
                'State': arguments.get('State', 'ENABLED'),
                'EventPattern': arguments.get('EventPattern', '')
            }
            current_step_details['arguments'] = {
                'Name': r['Name'],
                'Description': r.get('Description', ''),
                'ScheduleExpression': r.get('ScheduleExpression', ''),
                'State': r.get('State', 'ENABLED'),
                'EventPattern': r.get('EventPattern', '')
            }
            dd = deepdiff.DeepDiff(desired_step_details['arguments'], current_step_details['arguments'], ignore_order=True)
            if (len(dd.keys()) == 0):
                self.log_pass("The properties of the rule match.")
            else:
                self.log_fail("Rule Properties do NOT match")

            desired_targets = list(sorted(
                self.replacer(copy.copy(step_config.get('targets',[]))),
                key=lambda r: r['Id']
            ))
            r2 = client.list_targets_by_rule(
                Rule= arguments['Name']
            )
            actual_targets = list(sorted(r2.get('Targets', []), key=lambda r: r['Id']))
            desired_step_details['targets'] = desired_targets
            current_step_details['targets'] = actual_targets
            dd = deepdiff.DeepDiff(desired_targets, actual_targets, ignore_order=True)
            if (len(dd.keys()) == 0):
                self.log_pass("The targets of the rule match.")
            else:
                self.log_fail("The targets of the rule do NOT match")

            dd = deepdiff.DeepDiff(desired_step_details, current_step_details, ignore_order=True)
            if (len(dd.keys()) != 0):
                self._create_comparison_json_files(
                    "cloudwatchEvents-rule--%s--step" % arguments['Name'],
                    desired_step_details, current_step_details,
                    failure_message="Step Properties do NOT match"
                )
            return

        raise Exception("Unhandled verification of cloudwatch events function, `%s`" % function_name)

    def _verify_step_iam(self, step_config):
        client = self._get_boto_client('iam')
        function_name = step_config['function']

        # VERIFY::iam::create_instance_profile
        if function_name == 'create_instance_profile':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            try:
                r = client.get_instance_profile(
                    InstanceProfileName=arguments['InstanceProfileName']
                )
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fail("Instance profile non-existent")
                    return
                else:
                    raise e
            self.log_pass(" %s Instance profile exists")
            return

        # VERIFY::iam::update_account_password_policy
        if function_name == 'update_account_password_policy':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            try:
                r = client.get_account_password_policy()
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fail("Account password policy doesn't exist!")
                    return
                else:
                    raise e
            current_policy = copy.copy(r['PasswordPolicy'])

            # We cannot currently set ExpirePasswords and thus can't "Play" such a step.
            self.log_info("Warning - ignoring value of `ExpirePasswords` in the validation.")
            current_policy.pop("ExpirePasswords")

            dd1 = deepdiff.DeepDiff(current_policy, arguments, ignore_order=True)
            dd2 = deepdiff.DeepDiff(arguments, current_policy, ignore_order=True)
            if len(dd1) == 0 and len(dd2) == 0:
                self.log_pass("Password Policies match")
            else:
                self.log_fail("Password Policies do NOT match")
            return

        # VERIFY::iam::create_policy
        if function_name == 'create_policy':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            policy_arn = self.replacer("arn:aws:iam::__ACCOUNT_ID__:policy/") + arguments['PolicyName']
            try:
                r = client.get_policy(
                    PolicyArn=policy_arn
                )
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fail("Policy, `%s` non-existent" % arguments['PolicyName'])
                    return
                else:
                    raise e

            self.log_pass("Policy, `%s` exists" % (arguments['PolicyName']))
            r2 = client.get_policy_version(PolicyArn=policy_arn, VersionId=r['Policy']['DefaultVersionId'])
            current_policy_document = r2['PolicyVersion']['Document']
            desired_policy_document = arguments['PolicyDocument']
            dd = deepdiff.DeepDiff(desired_policy_document, current_policy_document, ignore_order=True)
            if (len(dd.keys()) == 0):
                self.log_pass("PolicyDocument match!")
            else:
                self.log_fail("PolicyDocument do NOT match!")

            if 'Description' not in arguments:
                self.log_fn(" - ignoring the description as it was not specified")
            else:
                if r['Policy']['Description'] == arguments['Description']:
                    self.log_pass("The descriptions match.")
                else:
                    self.log_fail(
                        "The descriptions differ (WARNING: Policy description can only be set when creating the policy)."
                    )

            return

        # VERIFY::iam::create_user
        if function_name == 'create_user':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            current_user_name = arguments['UserName']

            try:
                r = client.get_user(UserName=current_user_name)
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fail("User, `%s` non-existent" % current_user_name)
                    return
                else:
                    raise e
            self.log_pass("User, `%s` exists" % (arguments['UserName']))

            if r['User']['UserName'] == arguments['UserName']:
                self.log_pass("User, `{UserName}` matches by case correctly".format(**arguments))
            else:
                current_user_name = r['User']['UserName']
                desired_user_name = arguments['UserName']
                difference_string1 = ""
                difference_string2 = ""
                for i, c in enumerate(current_user_name):
                    difference_string1 += " " if current_user_name[i] == desired_user_name[i] else "^"
                    difference_string2 += " " if current_user_name[i] == desired_user_name[i] else "|"

                    self.log_fail(
                    """Desire user and actual user are not case-wise identical!
 - Desired:     {desiredUserName}
 - Actual :     {actualUserName}
 - Differences: {differenceString1}
""".format(
                        desiredUserName=arguments['UserName'],
                        actualUserName=r['User']['UserName'],
                        differenceString1=difference_string1,
                        differenceString2=difference_string2
                    )
                )

            # There will be used for a "big" deepDiff"
            desired_step_details = {}
            current_step_details = {}

            if 'linkedPolicyArns' not in step_config:
                self.log_fn("- there are no linked policies requested - skipping verification.")
                desired_step_details['linkedPolicyArns'] = "NotVerified"
                current_step_details['linkedPolicyArns'] = "NotVerified"
            else:
                r2 = client.list_attached_user_policies(UserName=current_user_name)

                desired_linked_policy_arns = set(self.replacer(copy.copy(step_config['linkedPolicyArns'])))
                current_linked_policy_arns = set(map(lambda rr: rr['PolicyArn'], r2['AttachedPolicies']))

                desired_step_details['linkedPolicyArns'] = list(sorted(list(desired_linked_policy_arns)))
                current_step_details['linkedPolicyArns'] = list(sorted(list(current_linked_policy_arns)))

                additional_policies = current_linked_policy_arns - desired_linked_policy_arns
                missing_policies = desired_linked_policy_arns - current_linked_policy_arns

                if len(additional_policies) == 0 and len(missing_policies) == 0:
                    self.log_pass("All desired linked policies are present (and no additional ones)")

                if len(additional_policies) > 0:
                    self.log_fail(
                        "There are additional linked policies than desired: \n       - %s" % "\n       - ".join(additional_policies)
                    )

                if len(missing_policies) > 0:
                    self.log_fail(
                        "There are desired linked policies that are not actually linked: \n       - %s" % "\n       - ".join(missing_policies)
                    )

            if 'userDescription' not in step_config:
                self.log_fn(" - ignoring the description as it was not specified")
                desired_step_details['userDescription'] = "NotVerified"
                current_step_details['userDescription'] = "NotVerified"

            else:
                desired_step_details['userDescription'] = self.replacer(step_config['userDescription'])
                current_step_details['userDescription'] = r['User'].get('Description', '')

                if r['User'].get('Description', '') == self.replacer(step_config['userDescription']):
                    self.log_pass("The description matches.")
                else:
                    self.log_fn("== Desired ==============")
                    self.log_fn(self.replacer(step_config['userDescription']))
                    self.log_fn("== Actual ===============")
                    self.log_fn(r['User'].get('Description', ''))
                    self.log_fn("=========================")
                    self.log_fail("The description differs.")

            if 'inlinePolicies' not in step_config:
                self.log_fn(" %s Skipping verification of inline policies (as `inlinePolicies` is not provided)." % self.s_info)
                desired_step_details['inlinePolicies'] = "NotVerified"
                current_step_details['inlinePolicies'] = "NotVerified"
            else:
                desired_step_details['inlinePolicies'] = sorted(self.replacer(step_config['inlinePolicies']), key=lambda r: r['PolicyName'])
                current_step_details['inlinePolicies'] = []

                desired_policy_names = set([d['PolicyName'] for d in step_config['inlinePolicies']])
                policy_document_by_name = {d['PolicyName']: d['PolicyDocument'] for d in step_config['inlinePolicies']}

                r_rp = client.list_user_policies(UserName=arguments['UserName'])
                actual_policy_names = set(r_rp['PolicyNames'])

                policies_not_actually_desired = actual_policy_names - desired_policy_names
                policies_not_actually_present = desired_policy_names - actual_policy_names

                policy_names_match = True
                if len(policies_not_actually_desired) > 0:
                    self.log_fail("There exists policies with names not actually desired (%s)" % ",".join(policies_not_actually_desired))
                    policy_names_match = False

                if len(policies_not_actually_present) > 0:
                    self.log_fail("There exists inline policies desired (%s) but not present." % ",".join(policies_not_actually_present))
                    policy_names_match = False
                if policy_names_match:
                    self.log_pass("The expected inline policies by name are present")

                for inline_policy_name in desired_policy_names:
                    if inline_policy_name in policies_not_actually_present:
                        continue
                    r = client.get_user_policy(
                        UserName=current_user_name,
                        PolicyName=inline_policy_name
                    )
                    desired_policy = self.replacer(copy.copy(policy_document_by_name[inline_policy_name]))
                    current_policy = r['PolicyDocument']
                    current_step_details['inlinePolicies'].append({'PolicyName': inline_policy_name, 'PolicyDocument': r['PolicyDocument']})

                    dd = deepdiff.DeepDiff(desired_policy, current_policy, ignore_order=True)
                    if (len(dd.keys()) == 0):
                        self.log_pass("User inline policy, `%s`, match." % inline_policy_name)
                    else:
                        self.log_fail("Inline policy, `%s`, do not match" % inline_policy_name)

                for inline_policy_name in policies_not_actually_desired:
                    r = client.get_user_policy(
                        UserName=current_user_name,
                        PolicyName=inline_policy_name
                    )
                    current_step_details['inlinePolicies'].append({'PolicyName': inline_policy_name, 'PolicyDocument': r['PolicyDocument']})

                current_step_details['inlinePolicies'] = sorted(current_step_details['inlinePolicies'], key=lambda r: r['PolicyName'])

                dd = deepdiff.DeepDiff(desired_step_details, current_step_details, ignore_order=True)
                if (len(dd.keys()) == 0):
                    self.log_pass("User `%s` details match." % current_user_name)
                else:
                    self._create_comparison_json_files(
                        "iam-user--%s--step" % current_user_name,
                        desired_step_details, current_step_details,
                        failure_message="User step details, do not match"
                    )

            if 'groupNames' not in step_config:
                self.log_fn("- there are no groupNames requested - skipping verification.")
                desired_step_details['groupNames'] = "NotVerified"
                current_step_details['groupNames'] = "NotVerified"
            else:
                r2 = client.list_groups_for_user(UserName=current_user_name)

                desired_groups = set(self.replacer(copy.copy(step_config['groupNames'])))
                current_groups = set(map(lambda rr: rr['GroupName'], r2['Groups']))

                desired_step_details['groupNames'] = list(sorted(list(desired_groups)))
                current_step_details['groupNames'] = list(sorted(list(current_groups)))

                additional_groups = current_groups - desired_groups
                missing_groups = desired_groups - current_groups

                if len(additional_groups) == 0 and len(missing_groups) == 0:
                    self.log_pass("All desired groups are present (and no additional ones)")

                if len(additional_groups) > 0:
                    self.log_fail(
                        "There are additional groups than desired: \n       - %s" % "\n       - ".join(additional_groups)
                    )

                if len(missing_groups) > 0:
                    self.log_fail(
                        "There are desired groups that of which the user is not member: \n       - %s" % "\n       - ".join(missing_groups)
                    )

            return

        # VERIFY::iam::create_role
        if function_name == 'create_role':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            current_role_name = arguments['RoleName']

            try:
                r = client.get_role(RoleName=current_role_name)
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fail("Role, `%s` non-existent" % current_role_name)
                    return
                else:
                    raise e
            self.log_pass("Role, `%s` exists" % arguments['RoleName'])

            if r['Role']['RoleName'] == arguments['RoleName']:
                self.log_pass("Role, `%s` matches by case correctly" % arguments['RoleName'])
            else:
                current_role_name = r['Role']['RoleName']
                desired_role_name = arguments['RoleName']
                difference_string1 = ""
                difference_string2 = ""
                for i, c in enumerate(current_role_name):
                    difference_string1 += " " if current_role_name[i] == desired_role_name[i] else "^"
                    difference_string2 += " " if current_role_name[i] == desired_role_name[i] else "|"

                self.log_fail(
                    """Desire role and actual role are not case-wise identical!
 - Desired:     {desiredRoleName}
 - Actual :     {actualRoleName}
 - Differences: {differenceString1}
""".format(
                        desiredRoleName=arguments['RoleName'],
                        actualRoleName=r['Role']['RoleName'],
                        differenceString1=difference_string1,
                        differenceString2=difference_string2
                    )
                )

            # There will be used for a "big" deepDiff"
            desired_step_details = {"arguments": {}}
            current_step_details = {"arguments": {}}

            if 'linkedPolicyArns' not in step_config:
                self.log_fn(
                    " %s Skipping verification of linked policies (as `linkedPolicyArns` is not provided)." % self.s_info
                )
                desired_step_details['linkedPolicyArns'] = "NotVerified"
                current_step_details['linkedPolicyArns'] = "NotVerified"
            else:
                r2 = client.list_attached_role_policies(RoleName=current_role_name)

                desired_linked_policy_arns = set(self.replacer(copy.copy(step_config['linkedPolicyArns'])))
                current_linked_policy_arns = set(map(lambda rr: rr['PolicyArn'], r2['AttachedPolicies']))

                desired_step_details['linkedPolicyArns'] = list(sorted(list(desired_linked_policy_arns)))
                current_step_details['linkedPolicyArns'] = list(sorted(list(current_linked_policy_arns)))

                additional_policies = current_linked_policy_arns - desired_linked_policy_arns
                missing_policies = desired_linked_policy_arns - current_linked_policy_arns

                if len(additional_policies) == 0 and len(missing_policies) == 0:
                    self.log_pass("All desired linked policies are present (and no additional ones)")

                if len(additional_policies) > 0:
                    self.log_fail(
                        "There are additional linked policies than desired: \n       - %s" % "\n       - ".join(additional_policies)
                    )

                if len(missing_policies) > 0:
                    self.log_fail(
                        "There are desired linked policies that are not actually linked: \n       - %s" % "\n       - ".join(missing_policies)
                    )

            if 'AssumeRolePolicyDocument' not in arguments:
                desired_step_details['arguments']['AssumeRolePolicyDocument'] = "NotVerified"
                current_step_details['arguments']['AssumeRolePolicyDocument'] = "NotVerified"
            else:
                desired_step_details['arguments']['AssumeRolePolicyDocument'] = arguments['AssumeRolePolicyDocument']
                current_step_details['arguments']['AssumeRolePolicyDocument'] = r['Role']['AssumeRolePolicyDocument']

                for statement in desired_step_details['arguments']['AssumeRolePolicyDocument'].get('Statement', []):
                    if 'AWS' in statement['Principal'] and (isinstance(statement['Principal']['AWS'], list)):
                        statement['Principal']['AWS'] = list(sorted(statement['Principal']['AWS']))
                    if 'Service' in statement['Principal'] and (isinstance(statement['Principal']['Service'], list)):
                        statement['Principal']['Service'] = list(sorted(statement['Principal']['Service']))


                for statement in current_step_details['arguments']['AssumeRolePolicyDocument']['Statement']:
                    if 'AWS' in statement['Principal'] and (isinstance(statement['Principal']['AWS'], list)):
                        statement['Principal']['AWS'] = list(sorted(statement['Principal']['AWS']))
                    if 'Service' in statement['Principal'] and (isinstance(statement['Principal']['Service'], list)):
                        statement['Principal']['Service'] = list(sorted(statement['Principal']['Service']))


                dd = deepdiff.DeepDiff(arguments['AssumeRolePolicyDocument'], r['Role']['AssumeRolePolicyDocument'], ignore_order=True)
                if (len(dd.keys()) == 0):
                    self.log_pass("AssumeRolePolicyDocument match.")
                else:
                    self.log_fail("AssumeRolePolicyDocument does NOT match.")

            if 'MaxSessionDuration' not in arguments:
                self.log_info("assuming MaxSessionDuration should be 3600 as it was not specified")
                desired_step_details['arguments']['MaxSessionDuration'] = 3600
            else:
                desired_step_details['arguments']['MaxSessionDuration'] = arguments['MaxSessionDuration']
            current_step_details['arguments']['MaxSessionDuration'] = r['Role']['MaxSessionDuration']

            if desired_step_details['arguments']['MaxSessionDuration'] == current_step_details['arguments']['MaxSessionDuration']:
                self.log_pass("MaxSessionDuration matches.")
            else:
                self.log_fail("MaxSessionDuration does not match")

            if 'roleDescription' in step_config and 'Description' in arguments:
                raise Exception("Both `['roleDescription']` and exist `['arguments']['Description']`. Only one of these should exist.")

            if 'roleDescription' not in step_config and 'Description' not in arguments:
                self.log_fn(" - ignoring the description as it was not specified")
                desired_step_details['arguments']['Description'] = "NotVerified"
                current_step_details['arguments']['Description'] = "NotVerified"
            else:
                if 'roleDescription' in step_config:
                    desired_step_details['arguments']['Description'] = self.replacer(step_config['roleDescription'])
                else:
                    desired_step_details['arguments']['Description'] = self.replacer(step_config['arguments']['Description'])

                current_step_details['arguments']['Description'] = r['Role'].get('Description', '')

                if current_step_details['arguments']['Description'] == desired_step_details['arguments']['Description']:
                    self.log_pass("The description matches.")
                else:
                    self.log_fail("The description differs.")

            if 'inlinePolicies' not in step_config:
                self.log_fn(
                    " %s Skipping verification of inline policies (as `inlinePolicies` is not provided)." % self.s_info
                )
                desired_step_details['inlinePolicies'] = "NotVerified"
                current_step_details['inlinePolicies'] = "NotVerified"
            else:
                desired_step_details['inlinePolicies'] = sorted(
                    self.replacer(step_config['inlinePolicies']), key=lambda r: r['PolicyName']
                )
                current_step_details['inlinePolicies'] = []

                desired_policy_names = set([d['PolicyName'] for d in step_config['inlinePolicies']])
                policy_document_by_name = {d['PolicyName']: d['PolicyDocument'] for d in step_config['inlinePolicies']}

                r_rp = client.list_role_policies(RoleName=arguments['RoleName'])
                actual_policy_names = set(r_rp['PolicyNames'])

                policies_not_actually_desired = actual_policy_names - desired_policy_names
                policies_not_actually_present = desired_policy_names - actual_policy_names

                policy_names_match = True
                if len(policies_not_actually_desired) > 0:
                    self.log_fail(
                        "There exists policies with names not actually desired (%s)" % ",".join(policies_not_actually_desired)
                    )
                    policy_names_match = False

                if len(policies_not_actually_present) > 0:
                    self.log_fail(
                        "There exists inline policies desired (%s) but not present." % ",".join(policies_not_actually_present)
                    )
                    policy_names_match = False
                if policy_names_match:
                    self.log_pass("The expected inline policies by name are present")

                for inline_policy_name in desired_policy_names:
                    if inline_policy_name in policies_not_actually_present:
                        continue
                    r = client.get_role_policy(
                        RoleName=current_role_name,
                        PolicyName=inline_policy_name
                    )
                    desired_policy = self.replacer(copy.copy(policy_document_by_name[inline_policy_name]))
                    current_policy = r['PolicyDocument']
                    current_step_details['inlinePolicies'].append({'PolicyName': inline_policy_name, 'PolicyDocument': r['PolicyDocument']})

                    dd = deepdiff.DeepDiff(desired_policy, current_policy, ignore_order=True)
                    if (len(dd.keys()) == 0):
                        self.log_pass("Role inline policy, `%s`, match." % inline_policy_name)
                    else:
                        self.log_pass("Role inline policy, `%s`, does NOT match." % inline_policy_name)

                for inline_policy_name in policies_not_actually_desired:
                    r = client.get_role_policy(
                        RoleName=current_role_name,
                        PolicyName=inline_policy_name
                    )
                    current_step_details['inlinePolicies'].append(
                        {'PolicyName': inline_policy_name, 'PolicyDocument': r['PolicyDocument']}
                    )

                current_step_details['inlinePolicies'] = sorted(
                    current_step_details['inlinePolicies'], key=lambda r: r['PolicyName']
                )

                dd = deepdiff.DeepDiff(desired_step_details, current_step_details, ignore_order=True)
                if (len(dd.keys()) == 0):
                    self.log_pass("Role `%s` details match." % current_role_name)
                else:
                    self._create_comparison_json_files(
                        "iam-role--%s--step" % current_role_name,
                        desired_step_details, current_step_details,
                        failure_message="Role step details, do not match"
                    )

            return

        # VERIFY::iam::create_group
        if function_name == 'create_group':
            arguments = self.replacer(copy.copy(step_config['arguments']))

            current_group_name = arguments['GroupName']
            try:
                r = client.get_group(GroupName=arguments['GroupName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fail("Group, `%s` non-existent" % arguments['GroupName'])
                    return
                else:
                    raise e
            self.log_pass("Group, `%s` exists" % arguments['GroupName'])

            # There will be used for a "big" deepDiff"
            desired_step_details = {}
            current_step_details = {}

            if 'linkedPolicyArns' not in step_config:
                self.log_fn("   => %s Skipping verification of linked policies (as `linkedPolicyArns` is not provided)." % self.s_info)
                desired_step_details['linkedPolicyArns'] = "NotVerified"
                current_step_details['linkedPolicyArns'] = "NotVerified"
            else:
                r = client.list_attached_group_policies(GroupName=arguments['GroupName'])

                desired_linked_policy_arns = set(self.replacer(copy.copy(step_config['linkedPolicyArns'])))
                current_linked_policy_arns = set(map(lambda rr: rr['PolicyArn'], r['AttachedPolicies']))

                desired_step_details['linkedPolicyArns'] = list(sorted(list(desired_linked_policy_arns)))
                current_step_details['linkedPolicyArns'] = list(sorted(list(current_linked_policy_arns)))

                additional_policies = current_linked_policy_arns - desired_linked_policy_arns
                missing_policies = desired_linked_policy_arns - current_linked_policy_arns

                if len(additional_policies) == 0 and len(missing_policies) == 0:
                    self.log_pass("All desired linked policies are present (and no additional ones)")

                if len(additional_policies) > 0:
                    self.log_fail(
                        "There are additional linked policies than desired:\n - %s" % additional_policies.join("\n - ")
                    )

                if len(missing_policies) > 0:
                    self.log_fail(
                        "There are desired linked policies that are not actually linked: \n       - %s" % "\n       - ".join(missing_policies)
                    )

            if 'inlinePolicies' not in step_config:
                self.log_fn(" %s Skipping verification of inline policies (as `inlinePolicies` is not provided)." % self.s_info)
                desired_step_details['inlinePolicies'] = "NotVerified"
                current_step_details['inlinePolicies'] = "NotVerified"
            else:
                desired_step_details['inlinePolicies'] = sorted(self.replacer(step_config['inlinePolicies']), key=lambda r: r['PolicyName'])
                current_step_details['inlinePolicies'] = []

                desired_policy_names = set([d['PolicyName'] for d in step_config['inlinePolicies']])
                policy_document_by_name = {d['PolicyName']: d['PolicyDocument'] for d in step_config['inlinePolicies']}

                r_rp = client.list_group_policies(GroupName=arguments['GroupName'])
                actual_policy_names = set(r_rp['PolicyNames'])

                policies_not_actually_desired = actual_policy_names - desired_policy_names
                policies_not_actually_present = desired_policy_names - actual_policy_names

                policy_names_match = True
                if len(policies_not_actually_desired) > 0:
                    self.log_fail(
                        "There exists policies with names not actually desired (%s)" % ",".join(policies_not_actually_desired)
                    )
                    policy_names_match = False

                if len(policies_not_actually_present) > 0:
                    self.log_fail(
                        "There are desired policies (%s) that are not present." % ",".join(policies_not_actually_present)
                    )
                    policy_names_match = False
                if policy_names_match:
                    self.log_pass(
                        "The expected inline policies by name are present (and none that are not desired)."
                    )

                for inline_policy_name in desired_policy_names:
                    if inline_policy_name in policies_not_actually_present:
                        continue
                    r = client.get_group_policy(
                        GroupName=current_group_name,
                        PolicyName=inline_policy_name
                    )
                    desired_policy = self.replacer(copy.copy(policy_document_by_name[inline_policy_name]))
                    current_policy = r['PolicyDocument']
                    current_step_details['inlinePolicies'].append({'PolicyName': inline_policy_name, 'PolicyDocument': r['PolicyDocument']})

                    dd = deepdiff.DeepDiff(desired_policy, current_policy, ignore_order=True)
                    if (len(dd.keys()) == 0):
                        self.log_pass("Group inline policy, `%s`, match." % inline_policy_name)
                    else:
                        self.log_pass("Group inline policy, `%s`, does NOT match." % inline_policy_name)

                for inline_policy_name in policies_not_actually_desired:
                    r = client.get_group_policy(
                        GroupName=current_group_name,
                        PolicyName=inline_policy_name
                    )
                    current_step_details['inlinePolicies'].append({'PolicyName': inline_policy_name, 'PolicyDocument': r['PolicyDocument']})

                current_step_details['inlinePolicies'] = sorted(current_step_details['inlinePolicies'], key=lambda r: r['PolicyName'])

                dd = deepdiff.DeepDiff(desired_step_details, current_step_details, ignore_order=True)
                if (len(dd.keys()) == 0):
                    self.log_pass("Group `%s` details match." % current_group_name)
                else:
                    self._create_comparison_json_files(
                        "iam-group--%s--step" % current_group_name,
                        desired_step_details, current_step_details,
                        failure_message="Group step details, do not match"
                    )

            return

        # VERIFY::iam::create_account_alias
        if function_name == "create_account_alias":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            r = client.list_account_aliases()
            if len(r['AccountAliases']) == 0:
                self.log_fail("The account alias is not set (desired it to be `%s`)." % arguments['AccountAlias'])
                return

            if r['AccountAliases'][0] == arguments['AccountAlias']:
                self.log_pass("Account alias is as desired (%s)." % arguments['AccountAlias'])
            else:
                self.log_fail("Account alias (%s) differs to that desired (%s)." % (
                    r['AccountAliases'][0], arguments['AccountAlias']
                ))
            return

        raise Exception("Unhandled verification of iam function, %s" % function_name)

    def _verify_step_lambda(self, step_config):
        client = self._get_boto_client_for_step('lambda', step_config)
        function_name = step_config['function']

        # VERIFY::lambda::create_function
        if function_name == 'create_function':
            replaced_step_config = self.replacer(copy.copy(step_config))
            arguments = replaced_step_config['arguments']
            try:
                r = client.get_function(FunctionName=arguments['FunctionName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ResourceNotFoundException":
                    self.log_fail("The lambda function, `%s` does not exist." % arguments['FunctionName'])
                    return
                else:
                    raise e
            self.log_fn("The lambda function, `%s` exists." % arguments['FunctionName'])

            current_function_configuration = copy.copy(r['Configuration'])
            lambda_arn = current_function_configuration.pop('FunctionArn', '')
            current_function_configuration.pop('LastModified', '')
            current_function_configuration.pop('LastUpdateStatus', '')
            current_function_configuration.pop('State', '')

            desired_function_configuration = {}
            desired_function_configuration['Description'] = arguments['Description'] if 'Description' in arguments else ''
            desired_function_configuration['Environment'] = arguments['Environment'] if 'Environment' in arguments else {'Variables': {}}
            desired_function_configuration['FunctionName'] = arguments['FunctionName']
            desired_function_configuration['Handler'] = arguments['Handler']
            desired_function_configuration['MemorySize'] = arguments['MemorySize'] if 'MemorySize' in arguments else 128
            desired_function_configuration['Role'] = arguments['Role']
            desired_function_configuration['Runtime'] = arguments['Runtime']
            desired_function_configuration['Timeout'] = arguments['Timeout'] if 'Timeout' in arguments else 3
            desired_function_configuration['TracingConfig'] = arguments['TracingConfig'] if 'TracingConfig' in arguments else {'Mode': 'PassThrough'}
            desired_function_configuration['Version'] = '$LATEST'
            default_vpc_config = {
                "SecurityGroupIds": [],
                "SubnetIds": []
            }
            desired_function_configuration['VpcConfig'] = arguments['VpcConfig'] if 'VpcConfig' in arguments else default_vpc_config
            desired_function_configuration['VpcConfig']['SubnetIds'] = list(sorted(desired_function_configuration['VpcConfig']['SubnetIds']))

            if 'Layers' in current_function_configuration:
                current_function_configuration['Layers'] = [x['Arn'] for x in current_function_configuration['Layers']]
                desired_function_configuration['Layers'] = arguments['Layers'] if 'Layers' in arguments else []
            elif 'Layers' in arguments:
                current_function_configuration['Layers'] = []

            current_function_configuration['VpcConfig'] = \
                current_function_configuration['VpcConfig'] if 'VpcConfig' in current_function_configuration else default_vpc_config

            current_function_configuration['VpcConfig']['SubnetIds'] = list(sorted(current_function_configuration['VpcConfig']['SubnetIds']))

            # We'll ignore the vpc-id, because verification of subnetId's will likewise verify the vpc-id. (i.e. being in a same subnets will be in the correct VPC)
            current_function_configuration['VpcConfig'].pop('VpcId', None)

            arguments_code = arguments.get('Code', {})

            and_code_is_checked_str = ""
            if 'S3Bucket' in arguments_code and 'S3Key' in arguments_code:
                argument_request_s3_url = "s3://%s/%s" % (arguments_code['S3Bucket'], arguments_code['S3Key'])
                and_code_is_checked_str = " (and source code as per " + argument_request_s3_url + ")"

                code_sha256, code_size = praws.utils.cached_get_s3_zip_lambda_signature(
                    arguments_code["S3Bucket"],
                    arguments_code["S3Key"],
                    self._get_lambda_cache_filename(),
                    use_cache=True,
                    s3_client=self._get_boto_client("s3")
                )
                desired_function_configuration['CodeSha256'] = code_sha256
                desired_function_configuration['CodeSize'] = code_size
                desired_function_configuration['_prawsInjectedS3Url'] = argument_request_s3_url

                matching_s3_urls = praws.utils.find_matches_in_cache(
                    current_function_configuration['CodeSha256'],
                    current_function_configuration['CodeSize'],
                    self._get_lambda_cache_filename(),
                )
                if argument_request_s3_url in matching_s3_urls:
                    current_function_configuration['_prawsInjectedS3Url'] = argument_request_s3_url
                else:
                    current_function_configuration['_prawsInjectedS3Urls'] = matching_s3_urls

            else:
                self.log_fn(" %s Not sure how to verify the contents of the URL", self.s_info)

            # Remove the 'RevisionId' - this will be captured by the `CodeSha256`
            # and there's a change that Many-1 relationship between RevisionId and CodeSha256 that wouldn't matter.
            current_function_configuration.pop('RevisionId', None)

            if 'Environment' not in current_function_configuration:
                current_function_configuration['Environment'] = {
                    "Variables": {}
                }

            dd = deepdiff.DeepDiff(
                as_nice_json(desired_function_configuration),
                as_nice_json(current_function_configuration),
                ignore_order=True
            )
            if len(dd) == 0:
                self.log_pass("function configuration matches%s." % and_code_is_checked_str)
            else:
                self._create_comparison_json_files(
                    "lambda-function--%s--function-config" % arguments['FunctionName'],
                    desired_function_configuration,
                    current_function_configuration,
                    failure_message="function configurations do not match"
                )

            if 'permissionsByStatementId' in replaced_step_config:
                desired_policy_by_statement_id = {
                    sid: praws.utils.lambda_args_to_policy(
                        sid,
                        lambda_arn,
                        replaced_step_config['permissionsByStatementId'][sid]
                    ) for sid in replaced_step_config['permissionsByStatementId'].keys()
                }
                try:
                    r2 = client.get_policy(FunctionName=arguments['FunctionName'])
                    current_policy_statements = json.loads(r2['Policy'])["Statement"]
                    current_policy_statements_by_sid = {x['Sid']: x for x in current_policy_statements}
                except botocore.exceptions.ClientError as e:
                    if e.response['Error']['Code'] == "ResourceNotFoundException":
                        current_policy_statements_by_sid = {}
                    else:
                        raise

                dd = deepdiff.DeepDiff(
                    as_nice_json(desired_policy_by_statement_id),
                    as_nice_json(current_policy_statements_by_sid),
                    ignore_order=True
                )
                if len(dd) == 0:
                    self.log_pass("function permissionsByStatementId match.")
                else:
                    self._create_comparison_json_files(
                        "lambda-function--%s--function-permissionsByStatementId" % arguments['FunctionName'],
                        desired_policy_by_statement_id,
                        current_policy_statements_by_sid,
                        failure_message="function permissionsByStatementId do not match"
                    )
            return

        # VERIFY::lambda::publish_layer_version
        if function_name == 'publish_layer_version':
            replaced_step_config = self.replacer(copy.copy(step_config))
            arguments = replaced_step_config['arguments']
            print(" - Verifying lambda with name, `%s`" % arguments['LayerName'])
            r = client.list_layer_versions(LayerName=arguments['LayerName'])
            if len(r['LayerVersions']) == 0:
                self.log_fail("There are no versions for the lambda with layer name, %s" % arguments['LayerName'])
                return
            layer_versions = list(sorted(r['LayerVersions'], key=lambda x: x['Version']))
            last_layer_version = layer_versions[-1]

            layer_version_arn = last_layer_version['LayerVersionArn']
            r2 = client.get_layer_version_by_arn(Arn=layer_version_arn)
            layer_version_info = r2['Content']

            desired_step_dict = {
                'client': 'lambda',
                'function': 'publish_layer_version',
                'arguments': arguments
            }

            actual_step_dict = {
                'client': 'lambda',
                'function': 'publish_layer_version',
                'arguments': {
                    'LayerName': arguments['LayerName'],
                    "Description": last_layer_version.get('Description', ''),
                    'CompatibleRuntimes': last_layer_version['CompatibleRuntimes']
                }
            }

            if 'S3Bucket' in arguments['Content'] and 'S3Key' in arguments['Content']:
                argument_request_s3_url = "s3://%s/%s" % (arguments['Content']['S3Bucket'], arguments['Content']['S3Key'])

                print(" - Getting lambda signature")
                code_sha256, code_size = praws.utils.cached_get_s3_zip_lambda_signature(
                    arguments['Content']["S3Bucket"],
                    arguments['Content']["S3Key"],
                    self._get_lambda_cache_filename(),
                    use_cache=True,
                    s3_client=self._get_boto_client("s3")
                )
                actual_step_dict['_prawsInjected'] = {
                    'CodeSha256': layer_version_info['CodeSha256'],
                    'CodeSize': layer_version_info['CodeSize']
                }
                desired_step_dict['_prawsInjected'] = {
                    'CodeSha256': code_sha256,
                    'CodeSize': code_size,
                }

                # If the code is the same, make the Content the same.
                if code_sha256 == layer_version_info['CodeSha256'] and code_size == layer_version_info['CodeSize']:
                    actual_step_dict['arguments']['Content'] = arguments['Content']
                else:
                    desired_step_dict['_prawsInjected']['desiredURL'] = argument_request_s3_url

                    actual_step_dict['_prawsInjected']['matchingS3URLs'] = praws.utils.find_matches_in_cache(
                        layer_version_info['CodeSha256'],
                        layer_version_info['CodeSize'],
                        self._get_lambda_cache_filename(),
                    )

            dd = deepdiff.DeepDiff(
                as_nice_json(desired_step_dict),
                as_nice_json(actual_step_dict),
                ignore_order=True
            )
            if len(dd) == 0:
                self.log_pass(
                    "Last version (%d) of lambda matches" % last_layer_version['Version']
                )
            else:
                self._create_comparison_json_files(
                    "lambda--layer--%s" % arguments['LayerName'],
                    desired_step_dict,
                    actual_step_dict,
                    failure_message="Last version of lambda layer does not match"
                )
            return

        raise Exception("Unhandled verification of function, `%s`" % function_name)

    def _verify_step_cloud_watch_logs(self, step_config):
        client = self._get_boto_client('logs')
        function_name = step_config['function']

        # VERIFY::logs::create_log_group
        if function_name == 'create_log_group':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            log_group_name = arguments["logGroupName"]
            r = client.describe_log_groups(logGroupNamePrefix=log_group_name)

            if len(list(filter(lambda x: x['logGroupName'] == log_group_name, r["logGroups"]))) > 0:
                self.log_pass("The log group, `%s` exists." % log_group_name)
            else:
                self.log_fail("The log group, `%s` does not exists." % log_group_name)
            return

        raise Exception("Unhandled verification of function, `%s`" % function_name)

    def _verify_step_organizations(self, step_config):
        function_name = step_config['function']

        client = self._get_boto_client('organizations')

        # VERIFY::organizations::custom:accept_handshake
        if function_name == 'custom:accept_handshake':
            fromOrganizationId = step_config["fromOrganizationId"]
            if callable(fromOrganizationId):
                fromOrganizationId = fromOrganizationId()

            try:
                r = client.describe_organization()
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "AWSOrganizationsNotInUseException":
                    self.log_fail("Account not connected to an organization..")
                    return
                else:
                    raise e

            connected_organization_id = r["Organization"]["Id"].replace("o-", "")
            if connected_organization_id == fromOrganizationId:
                self.log_pass("The account is part of the desired organization.")
            else:
                self.log_fail("Account is connected to a different organization (%s) that desired (%s).." % (connected_organization_id, fromOrganizationId))

            return

        # VERIFY::organizations::invite_account_to_organization
        if function_name == 'move_account':
            try:
                arguments = self.replacer(copy.copy(step_config['arguments']))
            except exceptions.AccountNotInOrganization:
                self.log_fail("The account is not even in the organization.")
                return

            r = client.list_parents(ChildId=arguments['AccountId'])
            if r['Parents'][0]['Id'] == arguments['DestinationParentId']:
                self.log_pass("The account is attached to the correct parent")
            else:
                self.log_fail("The account is not attached to the correct parent")
            return


        # VERIFY::organizations::invite_account_to_organization
        if function_name == 'invite_account_to_organization':
            arguments = copy.copy(step_config['arguments'])
            arguments = self.replacer(arguments)

            target_account_id = arguments['Target']["Id"]
            target_type = arguments['Target']["Type"]

            account_handshakes = self.get_organization_handshakes_for_account(target_account_id, target_type)

            if len(account_handshakes) == 0:
                self.log_fail("No handshakes exist.")
                return

            handshakes_by_states = reduce(_state_grouper, account_handshakes, {})

            if 'OPEN' in handshakes_by_states:
                self.log_pass(" %s A handshake request is currently open.")
                return

            if 'ACCEPTED' in handshakes_by_states:
                #self.log_fn(" - Handshake already accepted, checking if account is within the organisation")
                accounts = self._get_all_accounts_in_organization()
                linked_accounts_info = list(filter(lambda r: r['Id'] == target_account_id, accounts))
                if len(linked_accounts_info) > 1:
                    raise Exception("Not sure how to handle more than 1 record for an account");

                if len(linked_accounts_info) == 1:
                    if linked_accounts_info[0]['Status'] == 'ACTIVE':
                        self.log_pass("A handshake exists, and has been ACCEPTED and the account is within the organization.")
                        return
                    raise Exception("Need to handle the other states")

                if len(linked_accounts_info) == 0:
                    self.log_fn(" - A handshake was previously accepted, but was subsequently removed.")

                del handshakes_by_states["ACCEPTED"]

            if 'CANCELED' in handshakes_by_states:
                self.log_fn(" - A handshake requested, but but subsequently cancelled")
                del handshakes_by_states["CANCELED"]

            if len(handshakes_by_states.keys()) > 0:
                pprint(handshakes_by_states.keys())
                raise Exception("Not sure how to handle the other states")

            self.log_fail("Account not connected, and no handshake requests are active.")
            return

        raise Exception("Unhandled verification of iam function, `%s`" % function_name)

    def _get_all_accounts_in_organization(self):
        if self._all_accounts_in_organization is None:
            c = self._get_boto_client('organizations')
            r = c.list_accounts()
            if 'NextToken' in r:
                raise Exception("Need to handle next token")

            self._all_accounts_in_organization = r['Accounts']
        return self._all_accounts_in_organization

    def get_organization_id(self):
        return self._get_organization_details()["Id"]

    def _get_organization_details(self):
        if self._organization_record is None:
            self._organization_record = self._get_boto_client('organizations').describe_organization()['Organization']
        return self._organization_record

    def _verify_step_s3(self, step_config):
        function_name = step_config['function']

        # VERIFY::s3::create_bucket
        if function_name == "create_bucket":
            client = self._get_boto_client('s3')
            rb = client.list_buckets()
            if len(rb["Buckets"]) == 0:
                self.log_fail("the bucket does not exist (there are none)")
                return
            existing_buckets = [x["Name"] for x in rb["Buckets"]]
            arguments = copy.copy(step_config['arguments'])

            if arguments['Bucket'] not in existing_buckets:
                self.log_fail("the bucket with the name `%s`, does not exist." % arguments["Bucket"])
                return
            else:
                self.log_pass("the bucket with the name `%s` exists." % arguments["Bucket"])

            if 'CreateBucketConfiguration' in arguments and 'LocationConstraint' in arguments['CreateBucketConfiguration']:
                rb = client.get_bucket_location(Bucket=arguments['Bucket'])
                if rb['LocationConstraint'] == arguments['CreateBucketConfiguration']['LocationConstraint']:
                    self.log_pass("the bucket has the desired location.")
                else:
                    self.log_fail("the bucket is not in the desired region.")

            return

        # VERIFY::s3::delete_bucket_policy
        if function_name == "delete_bucket_policy":
            arguments = copy.copy(step_config['arguments'])
            client = self._get_boto_client('s3')
            try:
                r = client.get_bucket_policy(Bucket=arguments['Bucket'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchBucket":
                    self.log_fail("bucket does not exist.")
                    return
                if e.response['Error']['Code'] == "NoSuchBucketPolicy":
                    self.log_pass("There is no bucket policy (as desired).")
                    return
                else:
                    raise e
            self.log_fail(" %s bucket policy exist (when it is NOT desired).")
            return

        # VERIFY::s3::put_bucket_policy
        if function_name == "put_bucket_policy":
            arguments = copy.copy(step_config['arguments'])
            client = self._get_boto_client('s3')
            try:
                r = client.get_bucket_policy(Bucket=arguments['Bucket'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchBucket":
                    self.log_fail("bucket does not exist.")
                    return

                if e.response['Error']['Code'] == "NoSuchBucketPolicy":
                    self.log_fail("bucket policy does not exist.")
                    return
                else:
                    raise e

            current_policy = json.loads(r['Policy'])

            dd = deepdiff.DeepDiff(
                as_nice_json(arguments['Policy']),
                as_nice_json(current_policy),
                ignore_order=True
            )
            if len(dd) == 0:
                self.log_pass("bucket policy matches.")
            else:
                self._create_comparison_json_files(
                    "s3-bucket--%s--bucketPolicy" % arguments['Bucket'],
                    arguments['Policy'],
                    current_policy,
                    failure_message="bucket policy does not match"
                )
            return

        # VERIFY::s3::put_bucket_versioning
        if function_name == "put_bucket_versioning":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            client = self._get_boto_client('s3')
            try:
                r = client.get_bucket_versioning(Bucket=arguments['Bucket'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchBucket":
                    self.log_fail("bucket does not exist.")
                    return
                else:
                    raise e

            bucket_name = arguments['Bucket']

            if 'Status' in arguments['VersioningConfiguration']:
                if 'Status' not in r:
                    self.log_fail("The 'Status' versioning attribute on the bucket, `%s` has not been set yet." % bucket_name)
                elif r['Status'] == arguments['VersioningConfiguration']['Status']:
                    self.log_pass(
                        "The 'Status' versioning attribute on the bucket, `%s` is set to the desired state of `%s`."
                        % (bucket_name, arguments['VersioningConfiguration']['Status'])
                    )
                else:
                    self.log_fail(
                        "The 'Status' versioning attribute on the bucket, `%s` is not set to the desired state of `%s`."
                        % (bucket_name, arguments['VersioningConfiguration']['Status'])
                    )
            else:
                if 'Status' in r:
                    self.log_fn(" %s The 'Status' versioning attribute on the bucket, `%s` has no desired value but is set to `%s`"
                          % (self.s_info, bucket_name, r['Status'])
                    )

            if 'MFADelete' in arguments['VersioningConfiguration']:
                if 'MFADelete' not in r:
                    self.log_fn("The 'MFADelete' versioning attribute on the bucket, `%s` has not been set yet." % bucket_name)
                elif r['MFADelete'] == arguments['VersioningConfiguration']['MFADelete']:
                    self.log_pass(
                        "The 'MFADelete' versioning attribute on the bucket, `%s` is set to the desired state of `%s`."
                        % (bucket_name, arguments['VersioningConfiguration']['MFADelete'])
                    )
                else:
                    self.log_fail(
                        "The 'MFADelete' versioning attribute on the bucket, `%s` is not set to the desired state of `%s`."
                        % (bucket_name, arguments['VersioningConfiguration']['MFADelete'])
                    )
            else:
                if 'MFADelete' in r:
                    self.log_fn(" %s The 'MFADelete' versioning attribute on the bucket, `%s` has no desired value but is set to `%s`"
                          % (self.s_info, bucket_name, r['MFADelete'])
                    )

            return

        # VERIFY::s3::put_bucket_notification_configuration
        # - compares the documents.
        if function_name == "put_bucket_notification_configuration":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            bucket_name = arguments['Bucket']
            client = self._get_boto_client('s3')
            try:
                r = client.get_bucket_notification_configuration(Bucket=bucket_name)
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchBucket":
                    self.log_fail("bucket does not exist.")
                    return
                raise e

            desired_configuration = copy.copy(arguments['NotificationConfiguration'])
            current_configuration = copy.copy(r)
            current_configuration.pop('ResponseMetadata')
            possible_sections = ["TopicConfigurations", "QueueConfigurations", 'LambdaFunctionConfigurations']

            for possible_section in possible_sections:
                if possible_section not in current_configuration:
                    current_configuration[possible_section] = []
                if possible_section not in desired_configuration:
                    desired_configuration[possible_section] = []


            dd = deepdiff.DeepDiff(
                desired_configuration,
                current_configuration,
                ignore_order=True
            )
            if len(dd) == 0:
                self.log_pass("The notification configuration on the bucket, `%s` matches." % bucket_name)
            else:
                self._create_comparison_json_files(
                    "s3-bucket--%s--notificationConfiguration" % bucket_name,
                    desired_configuration,
                    current_configuration,
                    failure_message="The notification configuration on the bucket, `%s` does not match" % bucket_name
                )
            return

        # VERIFY::s3::put_bucket_lifecycle_configuration
        if function_name == "put_bucket_lifecycle_configuration":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            bucket_name = arguments['Bucket']
            client = self._get_boto_client('s3')
            try:
                r = client.get_bucket_lifecycle_configuration(Bucket=bucket_name)
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchBucket":
                    self.log_fail("bucket does not exist.")
                    return
                if e.response['Error']['Code'] == "NoSuchLifecycleConfiguration":
                    self.log_fail("bucket does not have any lifecycle configuration.")
                    return
                raise e

            desired_rules = sorted(arguments['LifecycleConfiguration']['Rules'], key=lambda r: r['ID'])
            current_rules = sorted(r['Rules'], key=lambda r: r['ID'])

            dd = deepdiff.DeepDiff(desired_rules, current_rules, ignore_order=True)
            if len(dd) == 0:
                self.log_pass("The lifecycle rules match.")
            else:
                self._create_comparison_json_files(
                    "s3-bucket--%s--lifecycleRules" % bucket_name,
                    desired_rules,
                    current_rules,
                    failure_message="The lifecycle rules for the bucket, `%s` do not match" % bucket_name
                )
            return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")

    def _verify_step_sdb(self, step_config):
        function_name = step_config['function']
        client = self._get_boto_client_for_step('sdb', step_config)

        # VERIFY::sdb::create_domain
        if function_name == "create_domain":
            arguments = self.replacer(copy.copy(step_config['arguments']))

            r = client.list_domains()
            if arguments['DomainName'] in r.get('DomainNames', []):
                self.log_pass("The SimpleDB domain, `%s` is present." % arguments['DomainName'])
            else:
                self.log_fail("The SimpleDB domain, `%s` is NOT present." % arguments['DomainName'])
            return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")

    def _verify_step_ses(self, step_config):
        function_name = step_config['function']
        client = self._get_boto_client_for_step('ses', step_config)

        # VERIFY::ses::custom:verify_domain_identity_and_add_txt_record_to_route53
        if function_name == "custom:verify_domain_identity_and_add_txt_record_to_route53":

            arguments = self.replacer(copy.copy(step_config['arguments']))
            email_domain = re.sub("\.+$", "", arguments['Domain'])

            r2 = client.get_identity_verification_attributes(Identities=[email_domain])
            if email_domain not in r2['VerificationAttributes']:
                self.log_fail("No verification attributes could be found for the identity, `%s`." % email_domain)
                return

            va = r2['VerificationAttributes'][email_domain]
            if va['VerificationStatus'] == 'Pending':
                self.log_fn(" %s No verification attributes for identity, %s but the verification status is pending." % (self.s_info, email_domain))
                self.log_fn(" %s TODO - verify that the token, %s is in the route 53 record set" % (self.s_info, va['VerificationToken']))
                self.log_fn(" %s TODO - verify that the MX record is in the route 53 record set" % (self.s_info))
                return
            if va['VerificationStatus'] == 'Success':
                self.log_pass("The email domain, %s has been successfully verified." % email_domain)
                self.log_fn(" %s TODO - verify that the MX record is in the route 53 record set" % (self.s_info))

            return

        # VERIFY::ses::custom:set_active_receipt_rule_set
        if function_name == 'custom:set_active_receipt_rule_set':
            arguments = self.replacer(copy.copy(step_config['arguments']))

            ses_client = self._get_boto_client_for_step('ses', step_config)
            r = ses_client.describe_active_receipt_rule_set()
            current_rule_name = r['Metadata']['Name']

            if current_rule_name.startswith(arguments['ruleNamePrefix']):
                self.log_pass("The current active rule name, (`%s`) start with `%s`." % (r['Metadata']['Name'], arguments['ruleNamePrefix']))
            else:
                self.log_fn(" %s Warning - The current active rule name, (`%s`) does not start with `%s`." % (self.s_info, r['Metadata']['Name'], arguments['ruleNamePrefix']))

            desired_rules = arguments['Rules']
            active_rules = list(
                sorted(
                    r['Rules'],
                    key=lambda x: x['Name']
                )
            )
            def _add_defaults(r):
                r['Enabled'] = r.get('Enabled', True)
                r['ScanEnabled'] = r.get('ScanEnabled', True)
                r['TlsPolicy'] = r.get('TlsPolicy', 'Optional')
                return r

            desired_rules = list(
                sorted(
                    list(map(_add_defaults, desired_rules)),
                    key=lambda r: r['Name']
                )
            )

            dd = deepdiff.DeepDiff(
                desired_rules,
                active_rules,
                ignore_order=True
            )
            if len(dd) == 0:
                self.log_pass("The receipt rules match.")
            else:
                self._create_comparison_json_files(
                    "ses--recieptRules",
                    desired_rules,
                    active_rules,
                    failure_message="SES Reciept rules differ"
                )
            return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")

    def _verify_step_sns(self, step_config):
        client = self._get_boto_client('sns')
        function_name = step_config['function']

        # VERIFY::sns::create_topic
        # - that it exists.
        if function_name == 'create_topic':
            arguments = self.replacer(copy.copy(step_config['arguments']))

            r = client.list_topics()
            topic_names_by_arn = {x['TopicArn'].split(":")[-1]:x['TopicArn'] for x in r['Topics']}
            if arguments['Name'] not in topic_names_by_arn:
                self.log_fail("SNS Topic with the name, `%s` does NOT exist" % arguments['Name'])
                return
            self.log_pass("SNS Topic with the name, `%s` exists" % arguments['Name'])

            topic_arn = topic_names_by_arn[arguments['Name']]

            if 'topicPolicy' not in step_config:
                self.log_fn("%s topicPolicy not specified - not verifying!" % (self.s_info))
            else:
                self._current_sns_topic_arn = topic_arn
                desired_topic_policy = self.replacer(copy.copy(step_config['topicPolicy']))
                r = client.get_topic_attributes(TopicArn=topic_arn)
                self._current_sns_topic_arn = None

                current_topic_policy = json.loads(r['Attributes']['Policy'])

                dd = deepdiff.DeepDiff(
                    desired_topic_policy,
                    current_topic_policy,
                    ignore_order=True
                )
                if len(dd) == 0:
                    self.log_pass("The topic policy matches.")
                else:
                    self._create_comparison_json_files(
                        "sns-topic--%s--policy" % topic_arn,
                        desired_topic_policy,
                        current_topic_policy,
                        failure_message="The topic policy does not matches"
                    )

            return

        # VERIFY::sns::subscribe
        if function_name == 'subscribe':
            client = self._get_boto_client('sns')
            arguments = self.replacer(copy.copy(step_config['arguments']))

            topic_arn = arguments['TopicArn']

            subscription_endpoint = arguments['Endpoint']
            subscription_protocol = arguments['Protocol']

            try:
                r = client.list_subscriptions_by_topic(
                    TopicArn=arguments['TopicArn']
                )
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NotFound":
                    self.log_fail("Topic arn, `%s` does not exist." % topic_arn)
                    return
                else:
                    raise e

            matching_subscriptions = list(
                filter(
                    lambda x: x['TopicArn'] == topic_arn \
                    and x['Endpoint'] == subscription_endpoint \
                    and x['Protocol'] == subscription_protocol,
                    r['Subscriptions']
                )
            )
            if len(matching_subscriptions) == 0:
                self.log_fail("%s subscription not in effect." % subscription_protocol)
                return
            if len(matching_subscriptions) > 1:
                raise Exception("Too many matches")
            self.log_pass("%s subscription in effect" % subscription_protocol)
            return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")

    def _verify_step_sqs(self, step_config):
        client = self._get_boto_client('sqs')
        function_name = step_config['function']

        # VERIFY::sqs::create_queue
        if function_name == 'create_queue':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            queue_name = arguments['QueueName']

            resp = client.list_queues()
            if 'QueueUrls' not in resp:
                self.log_fail("Queue with name, `%s` not found (There are none!)" % queue_name)
                return
            matching_queue_urls = list(filter(lambda x: x.split("/")[-1] == queue_name, resp['QueueUrls']))

            if len(matching_queue_urls) == 0:
                self.log_fail("Queue with name, `%s` not found" % queue_name)
                return
            self.log_pass("Queue URL `%s` found" % queue_name)

            sqs_queue_url = matching_queue_urls[0]

            if 'queueAttributes' not in step_config:
                self.log_fn("%s WARNING, 'queueAttributes', this won't be verifiable later!" % self.s_info)
            else:
                r = client.get_queue_attributes(
                    QueueUrl=sqs_queue_url,
                    AttributeNames=['All']
                )
                q_region, q_account, q_name = re.match("https://([^.]+).queue.amazonaws.com/([^/]+)/(.*)$", sqs_queue_url).groups()
                queue_arn = "arn:aws:sqs:%s:%s:%s" % (q_region, q_account, q_name)

                # Start with the defaults:
                expected_sqs_attributes = {
                    'MaximumMessageSize': "262144",
                    "MessageRetentionPeriod": "345600",
                    "DelaySeconds": "0",
                    "QueueArn": queue_arn
                }
                if 'Attributes' in arguments:
                    expected_sqs_attributes.update(arguments['Attributes'])
                if 'queueAttributes' in step_config:
                    self._current_sqs_queue_url = sqs_queue_url
                    self._current_sqs_queue_arn = queue_arn

                    extra_attributes = self.replacer(copy.copy(step_config['queueAttributes']))

                    self._current_sqs_queue_url = None
                    self._current_sqs_queue_arn = None

                    expected_sqs_attributes.update(extra_attributes)

                actual_attributes = r['Attributes']
                keys_to_ignore = [
                    'ApproximateNumberOfMessages',
                    'ApproximateNumberOfMessagesDelayed',
                    'ApproximateNumberOfMessagesNotVisible',
                    'CreatedTimestamp',
                    'LastModifiedTimestamp'
                ]
                for key_to_ignore in keys_to_ignore:
                    actual_attributes.pop(key_to_ignore, None)

                actual_attributes['Policy'] = json.loads(actual_attributes['Policy'])
                dd = deepdiff.DeepDiff(
                    expected_sqs_attributes,
                    actual_attributes,
                    ignore_order=True
                )
                if len(dd) == 0:
                    self.log_pass("Attributes match.")
                else:
                    self._create_comparison_json_files(
                        "sqs-queue--%s--attributes" % q_name,
                        expected_sqs_attributes,
                        actual_attributes,
                        failure_message="Attributes differ"
                    )
                return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")

    def _verify_step_route53(self, step_config):
        client = self._get_boto_client('route53')
        function_name = step_config['function']

        # VERIFY::route53::create_hosted_zone
        if function_name == "create_hosted_zone":
            arguments = self.replacer(copy.copy(step_config['arguments']))

            hosted_zone_ids = get_hosted_zones_for_domain_name(client, arguments['Name'])

            if len(hosted_zone_ids) > 0:
                self.log_pass("hosted zone with the name `%s` exists" % arguments['Name'])
            else:
                self.log_fail("hosted zone with the name `%s` does NOT exist" % arguments['Name'])
                return

            if 'addParentHostedZoneRecord' in step_config and step_config['addParentHostedZoneRecord']:
                self.log_fn("  -  getting desired_nameservers_wanted in_parent_route53_record.")
                r2 = client.get_hosted_zone(Id=hosted_zone_ids[0])
                desired_nameservers_in_parent_route53_record = r2['DelegationSet']['NameServers']

                parent_route53_client = client
                if 'functionToGetRoute53ClientForParentHostedZone' in step_config:
                    parent_route53_client = step_config['functionToGetRoute53ClientForParentHostedZone']()

                # Update the parent zone with new data.
                parent_zone_dn = re.sub('^[^.]+\.', '', arguments['Name'])
                self.log_fn("  -  getting zone id of parent")
                parent_hosted_zone_id = get_best_parent_hosted_zone_for_dn(parent_route53_client, parent_zone_dn)
                expected_name = re.sub("\.+$", ".", arguments['Name'])

                r = parent_route53_client.list_resource_record_sets(
                    HostedZoneId=parent_hosted_zone_id,
                    StartRecordName=expected_name,
                    StartRecordType="NS",
                    MaxItems='1'
                )

                if len(r['ResourceRecordSets']) == 0 \
                   or r['ResourceRecordSets'][0]['Name'] != expected_name \
                   or r['ResourceRecordSets'][0]['Type'] != 'NS':
                    self.log_fail("Parent hosted zone does not the desired NS record.")

                current_nameservers_in_parent_route53_record = [ x["Value"] for x in r['ResourceRecordSets'][0]["ResourceRecords"] ]

                dd = deepdiff.DeepDiff(
                    desired_nameservers_in_parent_route53_record,
                    current_nameservers_in_parent_route53_record,
                    ignore_order=True
                )
                if len(dd) == 0:
                    self.log_pass("Parent hosted zone has relevant NS record with correct value.")
                else:
                    self.log_fail("Parent hosted zone does not has the correct value in the NS record.")

            return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")


    # ==================================== PLAY STEPS ====================================
    def _play_step(self, step_config):
        """
        The official function for playing a step.

        :param step_config:
        :return:
        """
        if step_config['client'] == 'apigateway':
            return self._builders['apigateway']._play_step(step_config)

        if step_config['client'] == 'ecs':
            return self._play_step_ecs(step_config)

        if step_config['client'] == 'elbv2':
            return self._play_step_elbv2(step_config)

        if step_config['client'] == 'events':
            return self._play_step_cloudwatch_events(step_config)

        if step_config['client'] == 'iam':
            return self._play_step_iam(step_config)

        elif step_config['client'] == 'lambda':
            return self._play_step_lambda(step_config)

        elif step_config['client'] == 'logs':
            return self._play_step_cloud_watch_logs(step_config)

        if step_config['client'] == 'organizations':
            return self._play_step_organizations(step_config)

        if step_config['client'] == 's3':
            return self._play_step_s3(step_config)

        if step_config['client'] == 'sdb':
            return self._play_step_sdb(step_config)

        if step_config['client'] == 'ses':
            return self._play_step_ses(step_config)

        if step_config['client'] == 'sns':
            return self._play_step_sns(step_config)

        if step_config['client'] == 'sqs':
            return self._play_step_sqs(step_config)

        if step_config['client'] == 'route53':
            return self._play_step_route53(step_config)

        else:
            raise ValueError("Unknown client, `" + step_config['client'] + "`")

    def play_sub_step(self, step_config):
        """
        Play a single sub-step, (only difference from step is the formatting of the log.

        :param step_config: The parameters for a single step.
        :return:
        """
        self.log_fn(colored(" => Sub-step : - " + step_config['description'], "green"))
        self._play_step(step_config)



    def _play_step_ecs(self, step_config):
        """
        Handle playing of ECS steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('ecs')

        function_name = step_config['function']

        # PLAY::ecs::custom_restart_service_if_running
        if function_name == 'custom_restart_service_if_running':
            self.log_fn("WARNING - Deprecated, custom_restart_service_if_running is deprecated, please used please custom:restart_service_if_running instead")

        # PLAY::ecs::custom:restart_service_if_running
        if function_name == 'custom_restart_service_if_running' or function_name == 'custom:restart_service_if_running':
            print(" - seeing if service is running ...")
            service_info = self._get_boto_client('ecs').describe_services(
                cluster=arguments['cluster'], services=[arguments['serviceName']]
            )['services'][0]

            if service_info['desiredCount'] == 0 or service_info['pendingCount'] == 0 and service_info['runningCount'] == 0:
                self.log_fn(" not running, restart not needed.")
                return

            if service_info['desiredCount'] == 0:
                self.log_fn(" currently scaling down, no action needed.")
                return

            scale_back_to = service_info['desiredCount']
            self.log_fn(" running with desiredCount of %d" % scale_back_to)
            print(" - scaling down")
            self._get_boto_client('ecs').update_service(
                cluster=arguments['cluster'], service=arguments['serviceName'], desiredCount=0
            )
            running_count = 5
            while running_count > 0:
                service_info = self._get_boto_client('ecs').describe_services(
                    cluster=arguments['cluster'], services=[arguments['serviceName']]
                )['services'][0]
                running_count = service_info['runningCount']
                time.sleep(1)
                sys.stdout.write(".")
                sys.stdout.flush()
            self.log_fn("")
            self.log_fn(" - scaling back to %d" % scale_back_to)

            self._get_boto_client('ecs').update_service(
                cluster=arguments['cluster'], service=arguments['serviceName'], desiredCount=scale_back_to
            )
            return

        # PLAY-PRE::ecs::custom:register_task_definition
        # - handle 'sourceOfDockerImageVersions'
        if function_name == 'register_task_definition':
            if 'sourceOfDockerImageVersions' in step_config:
                args = step_config['sourceOfDockerImageVersions']['arguments']
                docker_image_version_map = step_config['sourceOfDockerImageVersions']['function'](**args)
                arguments['containerDefinitions'] = self.update_images_in_container_definitions(
                    arguments['containerDefinitions'], docker_image_version_map
                )

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        r = client_method(**arguments)

        # PLAY-POST::ecs::custom:register_task_definition
        # - describe the task that got created.
        if function_name == 'register_task_definition':
            task_definition_arn = r['taskDefinition']['taskDefinitionArn']
            self.log_fn(" - created task, %s" % task_definition_arn)

    def _play_step_elbv2(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('elbv2')
        function_name = step_config['function']
        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        try:
            client_method_response = client_method(**arguments)
        except botocore.exceptions.ParamValidationError as e:
            pprint(arguments)
            raise e

        # PLAY-POST::elbv2::create_load_balancer
        # - describe the task that got created.
        if function_name == 'create_load_balancer':
            new_load_balancer_detail = client_method_response['LoadBalancers'][0]
            load_balancer_arn = new_load_balancer_detail['LoadBalancerArn']

            if 'listeners' in step_config:
                for listener_config in step_config['listeners']:
                    listener_create_arguments = self.replacer(copy.copy(listener_config['arguments']))

                    listener_message_prefix = " - Adding listener {Protocol}:{Port}".format(**listener_create_arguments)
                    print(listener_message_prefix)
                    listener_create_arguments['LoadBalancerArn'] = load_balancer_arn

                    r = client.create_listener(
                        **listener_create_arguments
                    )
                    listener_arn = r['Listeners'][0]['ListenerArn']
                    if 'nonDefaultRules' in listener_config:
                        self._elbv2_synchronise_listener_rules(
                            client, listener_arn, self.replacer(copy.copy(listener_config['nonDefaultRules'])),
                            message_prefix=listener_message_prefix
                        )

                    if 'nonDefaultCertificates' in listener_config:
                        self._elbv2_synchronise_listener_certificates(
                            client, listener_arn, self.replacer(copy.copy(listener_config['nonDefaultCertificates'])),
                            message_prefix=listener_message_prefix
                        )

            if 'attributes' in step_config:
                attributes_copied = self.replacer(copy.copy(step_config['attributes']))
                self.log_fn(" - setting some attributes")
                attributes_as_list = [
                    {"Key": x, "Value": attributes_copied[x]} for x in attributes_copied
                ]

                client.modify_load_balancer_attributes(
                    LoadBalancerArn=load_balancer_arn,
                    Attributes=attributes_as_list
                )

    def _play_step_cloudwatch_events(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('events')
        function_name = step_config['function']
        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        client_method(**arguments)

        # POST-PLAY::events::put_rule
        if function_name == "put_rule":
            if "targets" in step_config:
                self.log_fn(" - put the targets")
                targets = self.replacer(copy.copy(step_config['targets']))
                client.put_targets(
                    Rule=arguments['Name'],
                    Targets=targets
                )

    def _play_step_iam(self, step_config):
        """
        Handle playing of EC2 steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('iam')
        function_name = step_config['function']
        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        # PLAY-PRE::iam::create_role
        # - convert AssumeRolePolicyDocument to nice json.
        if function_name == "create_role" and 'AssumeRolePolicyDocument' in arguments:
            arguments['AssumeRolePolicyDocument'] = as_nice_json(arguments['AssumeRolePolicyDocument'])

        # PLAY-PRE::iam::put_role_policy
        # - convert PolicyDocument to nice json.
        if function_name == "put_role_policy" and 'PolicyDocument' in arguments:
            arguments['PolicyDocument'] = as_nice_json(arguments['PolicyDocument'])

        # PLAY-PRE::iam::create_group
        if (function_name == "create_policy" or function_name == "create_group") and 'PolicyDocument' in arguments:
            arguments['PolicyDocument'] = as_nice_json(arguments['PolicyDocument'])

        # PLAY-PRE::iam::create_account_alias
        if function_name == "create_account_alias":
            self.log_fn(" - creating account alias, `%s`" % arguments['AccountAlias'])

        client_method(**arguments)

        # PLAY-POST::iam::create_user
        if function_name == "create_user":
            current_iam_user_name = arguments['UserName']
            self.log_fn(" - created the user, `%s`" % current_iam_user_name)
            if 'linkedPolicyArns' in step_config:
                for linked_policy_arn in self.replacer(copy.copy(step_config['linkedPolicyArns'])):
                    self._get_boto_client('iam').attach_user_policy(
                        UserName=current_iam_user_name,
                        PolicyArn=linked_policy_arn
                    )

            if 'inlinePolicies' in step_config:
                for inline_policy in step_config['inlinePolicies']:
                    self.log_fn("Adding inline policy, %s" % inline_policy['PolicyName'])
                    self._get_boto_client('iam').put_user_policy(**{
                        'UserName': current_iam_user_name,
                        'PolicyName': inline_policy['PolicyName'],
                        'PolicyDocument': as_nice_json(self.replacer(inline_policy['PolicyDocument']))
                    })

            if 'groupNames' in step_config:
                for group in step_config['groupNames']:
                    self.log_fn("Adding user to group, %s" % group)
                    self._get_boto_client('iam').add_user_to_group(**{
                        'UserName': current_iam_user_name,
                        'GroupName': group
                    })

        # PLAY-POST::iam::create_group
        if function_name == "create_group":
            current_iam_group_name = arguments['GroupName']
            self.log_fn(" - created the group, `%s`" % current_iam_group_name)
            if 'linkedPolicyArns' in step_config:
                for linked_policy_arn in self.replacer(copy.copy(step_config['linkedPolicyArns'])):
                    self._get_boto_client('iam').attach_group_policy(
                        GroupName=arguments['GroupName'],
                        PolicyArn=linked_policy_arn
                    )

            if 'inlinePolicies' in step_config:
                for inline_policy in step_config['inlinePolicies']:
                    self.log_fn("Adding inline policy, %s" % inline_policy['PolicyName'])
                    self._get_boto_client('iam').put_group_policy(**{
                        'GroupName': current_iam_group_name,
                        'PolicyName': inline_policy['PolicyName'],
                        'PolicyDocument': as_nice_json(self.replacer(inline_policy['PolicyDocument']))
                    })

        # PLAY-POST::iam::create_role
        if function_name == "create_role":
            current_role_name = arguments['RoleName']
            self.log_fn(" - created the role, `%s`" % current_role_name)
            if 'linkedPolicyArns' in step_config:
                for linked_policy_arn in self.replacer(copy.copy(step_config['linkedPolicyArns'])):
                    self.log_fn(" - attaching the linked policy, %s" % linked_policy_arn)
                    self._get_boto_client('iam').attach_role_policy(
                        RoleName=arguments['RoleName'],
                        PolicyArn=linked_policy_arn
                    )

            if 'inlinePolicies' in step_config:
                self._current_iam_role_name = arguments['RoleName']
                for inline_policy in step_config['inlinePolicies']:
                    self.log_fn(" - adding inline policy, %s" % inline_policy['PolicyName'])
                    self._get_boto_client('iam').put_role_policy(**{
                        'RoleName': current_role_name,
                        'PolicyName': inline_policy['PolicyName'],
                        'PolicyDocument': as_nice_json(self.replacer(inline_policy['PolicyDocument']))
                    })

            if 'roleDescription' in step_config:
                if 'Description' in step_config['arguments']:
                    raise Exception("Both `['roleDescription']` and exist `['arguments']['Description']`. Only one of these should exist.")

                self.log_fn(" - setting the description (Deprecation Note: Please replace `roleDescription` with `arguments.Description`)")
                self._get_boto_client('iam').update_role(
                    RoleName=current_role_name,
                    Description=self.replacer(copy.copy(step_config['roleDescription']))
                )

            if 'subSteps' in step_config:
                self._current_iam_role_name = arguments['RoleName']
                for sub_step in step_config['subSteps']:
                    if sub_step['function'] == 'put_role_policy':
                        raise Exception(
                            "Please move substeps with function, `put_role_policy` up to `inlinePolicies` to enable 'update' step to work correctly"
                        )

                    self.update_step(sub_step, True)
                self._current_iam_role_name = None

        # PLAY-POST::iam::create_instance_profile
        if function_name == "create_instance_profile" and 'subSteps' in step_config:
            for sub_step in step_config['subSteps']:
                self.play_sub_step(sub_step)

    def _play_step_lambda(self, step_config):
        """
        Handle playing of lambda steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client_for_step('lambda', step_config)
        function_name = step_config['function']

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        try:
            client_method(**arguments)
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "InvalidParameterValueException":
                print("================= Parameters to call that raised an exceptions =========")
                pprint(arguments)
                print("========================================================================")
            raise e


        if function_name == 'create_function':
            function_name = arguments['FunctionName']
            if 'permissionsByStatementId' not in step_config:
                self.log_fn("WARNING - Will not be able to track `permissions` of the lambda, because the key, `permissionsByStatementId` is missing in the step")
            else:
                permissions_by_statement_id = self.replacer(step_config['permissionsByStatementId'])
                for statement_id in permissions_by_statement_id:
                    self.log_fn(" - Adding permission for statement id, `%s`" % statement_id)
                    permission_arguments = permissions_by_statement_id[statement_id]
                    permission_arguments['StatementId'] = statement_id
                    permission_arguments['FunctionName'] = function_name
                    client.add_permission(**permission_arguments)

    def _play_step_cloud_watch_logs(self, step_config):
        """
        Handle playing of Cloud watch logs steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client("logs")
        function_name = step_config['function']

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        client_method(**arguments)

    def _play_step_organizations(self, step_config):
        """
        Handle playing of Organizations steps.

        :param step_config:
        :return:
        """
        function_name = step_config['function']

        # PLAY::organizations::custom:accept_handshake
        if function_name == 'custom:accept_handshake':
            client = self._get_boto_client('organizations')
            self.log_fn("================ list_handshakes_for_account ==========")
            r = client.list_handshakes_for_account()
            if len(r["Handshakes"]) == 0:
                raise Exception("There are no handshakes.. Nothing to accept.")
            fromOrganizationId = step_config["fromOrganizationId"]
            if callable(fromOrganizationId):
                fromOrganizationId = fromOrganizationId()

            desired_parties = [
                {"Id": fromOrganizationId, "Type": "ORGANIZATION"},
                {"Id": self._get_account_id(), "Type": "ACCOUNT"}
            ]

            relevant_handshakes = list(filter(lambda r: deepdiff.DeepDiff(r['Parties'], desired_parties, ignore_order=True) == {}, r['Handshakes']))
            relevant_handshakes_by_states = reduce(_state_grouper, relevant_handshakes, {})

            if "OPEN" in relevant_handshakes_by_states:
                handshake_to_accept_arn = relevant_handshakes_by_states["OPEN"][0]["Arn"]
                self.log_fn(" - Attempting to accept the handshake, with ARN, %s" % handshake_to_accept_arn)
                client.accept_handshake(HandshakeId=relevant_handshakes_by_states["OPEN"][0]["Id"])
                return

            if "ACCEPTED" in relevant_handshakes_by_states:
                in_an_organization = True
                try:
                    r = client.describe_organization()
                except botocore.exceptions.ClientError as e:
                    if e.response['Error']['Code'] == "AWSOrganizationsNotInUseException":
                        in_an_organization = False
                else:
                    raise e

                if not in_an_organization:
                    self.log_fn(" - Relevant handshake has been accepted, but the account is no longer in an organisation")
                else:
                    if r["Organization"]["Id"].replace("o-", "") == fromOrganizationId:
                        self.log_fn(" - have already accepted the handshake, and become part of the (correct) organization.")
                        return
                    else:
                        raise Exception(
                            "Handshake previously accepted, and reject, and the account is now part of a different organization (with id, %s)!" % r["Organization"]["Id"]
                        )

                del relevant_handshakes_by_states["ACCEPTED"]

            for state_to_ignore in ["CANCELED", "DECLINED", "EXPIRED"]:
                if state_to_ignore in relevant_handshakes_by_states:
                    del relevant_handshakes_by_states[state_to_ignore]

            if len(relevant_handshakes_by_states) > 0:
                pprint(relevant_handshakes_by_states.keys())
                raise Exception("Not sure how to handle the above keys")

            raise Exception("There are no OPEN handshakes to accept")
            return

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('organizations')
        client_method = getattr(client, function_name)

        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        client_method(**arguments)


    def _play_step_s3(self, step_config):
        """
        Handle playing of S3 steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('s3')
        function_name = step_config['function']
        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        # PRE-PLAY::s3::put_bucket_policy
        # - turn Policy argument into a string.
        if function_name == 'put_bucket_policy':
            arguments['Policy'] = as_nice_json(arguments['Policy'])

        # PRE-PLAY::s3::create_bucket
        # - show the name of the bucket being created.
        if function_name == 'create_bucket':
            self.log_fn(" - creating the bucket, `%s`" % arguments['Bucket'])

        the_exception = None
        try:
            client_method(**arguments)
        except botocore.exceptions.ClientError as e:
            # We'll delay raising the exception to possibly print some stuff out.
            the_exception = e

        if the_exception is not None:
            # PLAY-ERR_POST::s3::put_bucket_notification_configuration
            if function_name == 'put_bucket_notification_configuration' and the_exception.response['Error']['Code'] == "InvalidArgument":
                print("=========================")
                pprint(arguments)
                print("=========================")

            # PLAY-ERR_POST::s3::put_bucket_policy
            # - display the erraneous error.
            if function_name == 'put_bucket_policy' and the_exception.response['Error']['Code'] == "MalformedPolicy":
                print("=========================")
                print(arguments['Policy'])
                print("=========================")

        if the_exception is not None:
            raise the_exception

    def _play_step_sdb(self, step_config):
        """
        Handle playing of SWF steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('sdb')
        function_name = step_config['function']

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        # PLAY-PRE::sdb::create_domain
        # - show the name of the domain
        if function_name == "create_domain":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            self.log_fn(" - creating the SimpleDB domain, `%s`" % arguments['DomainName'])

        client_method(**arguments)

    def _play_step_ses(self, step_config):
        """
        Handle playing of S3 steps.

        :param step_config:
        :return:
        """
        arguments = self.replacer(copy.copy(step_config['arguments']))

        ses_aws_region_name = step_config.get('aws_region_name', self.region_name)
        client = self._get_boto_client_for_step('ses', step_config)

        function_name = step_config['function']

        # PLAY::ses::custom:verify_domain_identity_and_add_txt_record_to_route53
        if function_name == 'custom:verify_domain_identity_and_add_txt_record_to_route53':

            # drop all trailing dots.
            email_domain = re.sub("\.+$", "", arguments['Domain'])

            self.log_fn(" - Calling verify_domain_identity for the email domain, `%s`" % email_domain)
            r = client.verify_domain_identity(Domain=email_domain)
            verification_token = r['VerificationToken']

            self.log_fn(" - Adding the TXT record to route53 with the received verification token, `%s`" % verification_token)
            route53client = self._get_boto_client("route53")
            self.log_fn(" - getting hosted zone id")
            hosted_zone_ids = get_hosted_zones_for_domain_name(route53client, email_domain + ".")
            if len(hosted_zone_ids) != 1:
                raise Exception("Unable to find a hosted zone to place the TXT record into.")
            hosted_zone_id = hosted_zone_ids[0]

            change_batch_changes = [
                {
                    'Action': 'UPSERT',
                    'ResourceRecordSet': {
                        'Name': email_domain + ".",
                        'Type': 'TXT',
                        'TTL': 60,
                        'ResourceRecords': [{'Value': '"' + verification_token + '"'}]
                    }
                }
            ]

            if 'addMXRecordToSES' in step_config and step_config['addMXRecordToSES']:
                self.log_fn(" - Adding TXT and MX record in route53")
                change_batch_changes.append({
                    'Action': 'UPSERT',
                    'ResourceRecordSet': {
                        'Name': email_domain + ".",
                        'Type': 'MX',
                        'TTL': 300,
                        'ResourceRecords': [{'Value': '10 inbound-smtp.%s.amazonaws.com' % ses_aws_region_name}]
                    }
                })
            else:
                self.log_fn(" - adding TXT record in route53")

            route53client.change_resource_record_sets(
                HostedZoneId=hosted_zone_id,
                ChangeBatch={
                    'Comment': 'Automated update: Upsert TXTrecord for %s' % email_domain + ".",
                    'Changes': change_batch_changes
                }
            )
            return

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        client_method(**arguments)

    def _play_step_sns(self, step_config):
        """
        Handle playing of SNS steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('sns')

        function_name = step_config['function']

        # PLAY-PRE::sns::custom-multi-subscribe
        if function_name == 'custom-multi-subscribe':
            self.log_fn("DEPRECATED - the function name, `custom-multi-subscribe` is deprecated, please use `custom:multi-subscribe` instead")

        # PLAY-PRE::sns::custom:multi-subscribe
        if function_name == 'custom-multi-subscribe' or function_name == 'custom:multi-subscribe':
            for endpoint in step_config['endpoints']:
                step_config2 = copy.copy(step_config)
                step_config2['function'] = 'subscribe'
                step_config2['arguments']['Protocol'] = endpoint['Protocol']
                step_config2['arguments']['Endpoint'] = endpoint['Endpoint']
                self.log_fn(' - subscribing %s::%s' % (endpoint['Protocol'], endpoint['Endpoint']))
                del step_config2['endpoints']
                self._play_step_sns(step_config2)
            return

        client_method = getattr(client, function_name)

        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        # PLAY-PRE::sns::create_topic
        # - show info - display the topic name being created.x
        if function_name == "create_topic":
            self.log_fn(" - creating SNS topic, `%s`" % arguments['Name'])

        # PLAY-PRE::sns::set_topic_attributes
        # - Convert 'AttributeValue' to json
        if function_name == "set_topic_attributes" and arguments['AttributeName'] == 'Policy':
            arguments['AttributeValue'] = as_nice_json(arguments['AttributeValue'])

        resp = client_method(**arguments)

        # PLAY-POST::sns::create_topic
        # - handle 'topicPolicy'
        # - play subSteps
        if function_name == "create_topic":
            self._current_sns_topic_arn = resp['TopicArn']
            self._current_sns_client = client

            if 'topicPolicy' in step_config:
                policy_document = self.replacer(copy.copy(step_config['topicPolicy']))
                try:
                    client.set_topic_attributes(
                        TopicArn=self._current_sns_topic_arn,
                        AttributeName='Policy',
                        AttributeValue=as_nice_json(policy_document)
                    )
                except botocore.exceptions.ClientError as e:
                    print("======== Policy Document =========")
                    pprint(policy_document)
                    print("==================================")
                    raise e

            if 'subSteps' in step_config:
                for sub_step in step_config['subSteps']:
                    self.play_sub_step(sub_step)

            self._current_sns_topic_arn = None
            self._current_sns_client = None

    def _play_step_sqs(self, step_config):
        """
        Handle playing of SQS steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('sqs')

        function_name = step_config['function']
        client_method = getattr(client, function_name)

        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        # PLAY-PRE::sqs::create_queue
        # - show info - display name of sqs queue being created.
        if function_name == "create_queue":
            self.log_fn(" - creating SQS queue, `%s`" % arguments['QueueName'])

        # PLAY-PRE::sqs::set_queue_attributes
        # - conviert 'Policy' attribute to string.
        if function_name == "set_queue_attributes" and 'Policy' in arguments['Attributes']:
            arguments['Attributes']['Policy'] = json.dumps(arguments['Attributes']['Policy'])

        resp = client_method(**arguments)

        # PLAY-POST::sqs::create_queue
        # - play the sub steps.
        if function_name == "create_queue":
            q_region, q_account, q_name = re.match("https://([^.]+).queue.amazonaws.com/([^/]+)/(.*)$", resp['QueueUrl']).groups()
            queue_arn = "arn:aws:sqs:%s:%s:%s" % (q_region, q_account, q_name)

            self._current_sqs_queue_url = resp['QueueUrl']
            self._current_sqs_queue_arn = queue_arn

            if 'queueAttributes' not in step_config:
                self.log_fn("- WARNING, 'queueAttributes', this won't be verifiable later!")
            else:
                queue_attributes = self.replacer(copy.copy(step_config['queueAttributes']))
                if 'Policy' in queue_attributes:
                    queue_attributes['Policy'] = as_nice_json(queue_attributes['Policy'])

                client.set_queue_attributes(
                    QueueUrl=self._current_sqs_queue_url,
                    Attributes=queue_attributes
                )

            if 'subSteps' in step_config:
                # ARN is not in the initial response!
                self.log_fn(" - getting SQS ARN")

                for sub_step in step_config['subSteps']:
                    self.update_step(sub_step, True)

            self._current_sqs_queue_url = None
            self._current_sqs_queue_arn = None

    def _play_step_route53(self, step_config):
        """
        Handle playing of Route53 steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client("route53")

        function_name = step_config['function']
        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        # PRE-PLAY::route53::create_hosted_zone
        # - Ensure hosted zone does not already exist (otherwise it'll make another with the same name)
        if function_name == "create_hosted_zone":
            self.log_fn(" - looking up info about the hosted zone, with name, `%s`" % arguments['Name'])
            hosted_zone_ids = get_hosted_zones_for_domain_name(client, arguments['Name'])

            if len(hosted_zone_ids) > 0:
                raise ValueError("A hosted zone already exists with the name `%s`, refusing to make another one." % arguments['Name'])

        r = client_method(**arguments)

        # POST-PLAY::route53::create_hosted_zone
        # - handle adding addParentHostedZoneRecord.
        if function_name == "create_hosted_zone":
            # Force the next request for hosted zones to refresh itself.
            self.hosted_zones = None

            if 'addParentHostedZoneRecord' in step_config and step_config['addParentHostedZoneRecord']:

                parent_route53_client = client
                if 'functionToGetRoute53ClientForParentHostedZone' in step_config:
                    parent_route53_client = step_config['functionToGetRoute53ClientForParentHostedZone']()

                # Update the parent zone with new data.
                parent_zone_dn = re.sub('^[^.]+\.', '', arguments['Name'])
                self.log_fn(" - updating record in parent hosted zone - 1. getting zone id of parent")
                parent_hosted_zone_id = get_best_parent_hosted_zone_for_dn(parent_route53_client, parent_zone_dn)

                self.log_fn(" - zone id of parent: %s" % parent_hosted_zone_id)

                self.log_fn(" - adding NS record to parent zone for %s " % arguments['Name'])
                resource_records = []
                for ns in r['DelegationSet']['NameServers']:
                    resource_records.append({'Value': ns})

                change_batch_changes = [
                    {
                        'Action': 'UPSERT',
                        'ResourceRecordSet': {
                            'Name': arguments['Name'],
                            'Type': 'NS',
                            'TTL': 60,
                            'ResourceRecords': resource_records
                        }
                    }
                ]
                parent_route53_client.change_resource_record_sets(
                    HostedZoneId=parent_hosted_zone_id,
                    ChangeBatch={
                        'Comment': 'Automated update: Upsert NS record for %s' % arguments['Name'],
                        'Changes': change_batch_changes
                    }
                )

    # ==================================== UPDATE STEPS ====================================
    def _update_step(self, step_config):
        self.clear_logs()
        if step_config['client'] == 'apigateway':
            return self._builders['apigateway']._update_step(step_config)

        if step_config['client'] == 'ecs':
            return self._update_step_ecs(step_config)

        if step_config['client'] == 'elbv2':
            return self._update_step_elbv2(step_config)

        if step_config['client'] == 'events':
            return self._update_step_cloudwatch_events(step_config)

        if step_config['client'] == 'iam':
            return self._update_step_iam(step_config)

        if step_config['client'] == 'lambda':
            return self._update_step_lambda(step_config)

        if step_config['client'] == 's3':
            return self._update_step_s3(step_config)

        if step_config['client'] == 'ses':
            return self._update_step_ses(step_config)

        if step_config['client'] == 'sns':
            return self._update_step_sns(step_config)

        if step_config['client'] == 'sqs':
            return self._update_step_sqs(step_config)

        else:
            raise ValueError("Unknown client, `" + step_config['client'] + "`")

        raise ValueError("Update step not implemented yet")

    def _update_step_ecs(self, step_config):
        client = self._get_boto_client('ecs')
        function_name = step_config['function']

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        # UPDATE::ecs::register_task_definition
        if function_name == 'register_task_definition':
            self.log_fn(" - updating task definition using the 'play' since both 'play' and 'update' involve creating a new version:")
            self.log_fn(" - ")
            self.play_step(step_config)
            return

        # UPDATE::ecs::create_service
        if function_name == 'create_service':
            self.log_fn(" - updating the service task definition, (other changes not implemented yet)")
            update_arguments = {
                'cluster': arguments['cluster'],
                'service': arguments['serviceName'],
                'taskDefinition': arguments['taskDefinition']
            }
            client.update_service(**update_arguments)
            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def _update_step_elbv2(self, step_config):
        client = self._get_boto_client('elbv2')
        function_name = step_config['function']

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        # UPDATE::elbv2::create_load_balancer
        if function_name == 'create_load_balancer':
            r = client.describe_load_balancers(Names=[arguments['Name']])
            current_attributes = copy.copy(r['LoadBalancers'][0])
            load_balancer_arn = current_attributes['LoadBalancerArn']

            if current_attributes['Type'] == 'application':
                self.log_fn(" - Updating the security groups")
                client.set_security_groups(
                    LoadBalancerArn=load_balancer_arn,
                    SecurityGroups=arguments['SecurityGroups']
                )

                self.log_fn(" - Updating the subnets")
                client.set_subnets(
                    LoadBalancerArn=load_balancer_arn,
                    Subnets=arguments['Subnets']
                )

            self.log_fn(" - Looking up existing listeners")
            current_listeners = client.describe_listeners(LoadBalancerArn=load_balancer_arn)['Listeners']
            desired_listeners = self.replacer(copy.copy(step_config['listeners']))

            current_listeners_by_proto_and_port = {"{Protocol}:{Port}".format(**l) : l for l in current_listeners}
            desired_listeners_by_proto_and_port = {"{Protocol}:{Port}".format(**l['arguments']) : l for l in desired_listeners}

            desired_keys = set(desired_listeners_by_proto_and_port.keys())
            current_keys = set(current_listeners_by_proto_and_port.keys())

            keys_to_remove = current_keys - desired_keys
            keys_to_add = desired_keys - current_keys
            keys_to_update = desired_keys - keys_to_add

            for key_to_remove in keys_to_remove:
                print(" - Removing listener %s" % key_to_remove)
                client.delete_listener(ListenerArn=current_listeners_by_proto_and_port[key_to_remove]['ListenerArn'])

            for key_to_add in keys_to_add:
                print(" - Adding listener %s" % key_to_add)
                listener_create_arguments = desired_listeners_by_proto_and_port[key_to_add]['arguments']
                listener_create_arguments['LoadBalancerArn'] = load_balancer_arn
                r = client.create_listener(**listener_create_arguments)
                listener_arn = r['Listeners'][0]['ListenerArn']

                if 'nonDefaultRules' in desired_listeners_by_proto_and_port[key_to_add]:
                    print(" - Adding listener %s - rules" % key_to_add)
                    self._elbv2_synchronise_listener_rules(
                        client, listener_arn, desired_listeners_by_proto_and_port[key_to_add]['nonDefaultRules'],
                        message_prefix=" - Adding listener %s - rules" % key_to_add
                    )

                if 'nonDefaultCertificates' in desired_listeners_by_proto_and_port[key_to_add]:
                    print(" - Adding listener %s - non-default certificates" % key_to_add)
                    self._elbv2_synchronise_listener_certificates(
                        client, listener_arn, desired_listeners_by_proto_and_port[key_to_add]['nonDefaultCertificates'],
                        message_prefix=" - Adding listener %s - rules" % key_to_add
                    )

            for key_to_update in keys_to_update:
                print(" - Updating listener %s - settings" % key_to_update)
                listener_modify_arguments = desired_listeners_by_proto_and_port[key_to_update]['arguments']
                listener_arn = current_listeners_by_proto_and_port[key_to_update]['ListenerArn']
                listener_modify_arguments['ListenerArn'] = listener_arn
                r = client.modify_listener(**listener_modify_arguments)

                if 'nonDefaultCertificates' in desired_listeners_by_proto_and_port[key_to_update]:
                    print(" - Updating listener %s - non-default certificates" % key_to_update)
                    self._elbv2_synchronise_listener_certificates(
                        client,
                        listener_arn,
                        desired_listeners_by_proto_and_port[key_to_update]['nonDefaultCertificates'],
                        message_prefix=" - Updating listener %s - certificates" % key_to_update
                    )

                print(" - Updating listener %s - rules" % key_to_update)
                self._elbv2_synchronise_listener_rules(client, listener_arn, desired_listeners_by_proto_and_port[key_to_update]['nonDefaultRules'], message_prefix=" - Updating listener %s - rules" % key_to_update)

            self.log_info("There may be more steps to be updated still")
            return

        # UPDATE::elbv2::create_target_group
        if function_name == 'create_target_group':
            arguments = self.replacer(copy.deepcopy(step_config['arguments']))

            client = self._get_boto_client('elbv2')
            r = client.describe_target_groups(Names=[arguments['Name']])
            target_groups = r.get('TargetGroups', [])
            if len(target_groups) != 1:
                raise Exception("Load-balancer not found or too many targets found with that name")

            target_group_arn = target_groups[0]['TargetGroupArn']
            # Health check attributes - https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/elbv2.html#ElasticLoadBalancingv2.Client.modify_target_group
            health_check_attributes = [
                'HealthCheckProtocol',
                'HealthCheckPort',
                'HealthCheckPath',
                'HealthCheckEnabled',
                'HealthCheckIntervalSeconds',
                'HealthCheckTimeoutSeconds',
                'HealthyThresholdCount',
                'UnhealthyThresholdCount',
                'Matcher'
            ]
            args = {}
            for health_check_attribute in health_check_attributes:
                if health_check_attribute in arguments:
                    args[health_check_attribute] = arguments.pop(health_check_attribute)

            if len(args.keys()) > 0:
                print(" - Modifying the health check attributes")
                args['TargetGroupArn'] = target_group_arn
                client.modify_target_group(**args)

            # The following cannot be updated (I think!)
            arguments_to_ignore = ['Name', 'Port', 'Protocol', 'VpcId']
            for argument_to_ignore in arguments_to_ignore:
                arguments.pop(argument_to_ignore)

            if len(arguments.keys()) == 0:
                return

            pprint(arguments)
            raise Exception("Some arguments could not be updated - see pprint output above")

        raise ValueError("Unhandled update for function, {client}::{function}`".format(**step_config))

    def _update_step_cloudwatch_events(self, step_config):
        function_name = step_config['function']

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        # UPDATE::events::put_rule
        if function_name == 'put_rule':
            client = self._get_boto_client('events')
            self.log_fn(" - put the rule")
            client.put_rule(**arguments)
            if "targets" in step_config:
                self.log_fn(" - put the targets")
                targets = self.replacer(copy.copy(step_config['targets']))
                client.put_targets(
                    Rule=arguments['Name'],
                    Targets=targets
                )
                self.log_fn("- WARNING / TODO - need to remove the additional targets not needed!")

            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def _update_step_iam(self, step_config):
        function_name = step_config['function']

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        # UPDATE::iam::update_account_password_policy
        if function_name == 'update_account_password_policy':
            self._get_boto_client('iam').update_account_password_policy(**arguments)
            return

        # UPDATE::iam::put_role_policy
        if function_name == 'put_role_policy':
            self.log_fn(" - putting iam role policy, `%s`" % arguments['PolicyName'])
            self._play_step(step_config)
            return

        # UPDATE::iam::create_policy
        if function_name == 'create_policy':
            self.log_fn(" - getting count of policy versions")
            policy_arn = self.replacer("arn:aws:iam::__ACCOUNT_ID__:policy/")+arguments['PolicyName']
            r = self._get_boto_client('iam').list_policy_versions(PolicyArn=policy_arn)
            self.log_fn(".. %d" % len(r))
            if len(r['Versions']) > 4:
                oldest_version_id = sorted(r['Versions'], key=(lambda x: x['CreateDate']))[0]['VersionId']
                self.log_fn(" - Exceeded 4 policies, deleting oldest policy version (%s) to make way for new fifth." % oldest_version_id)
                self._get_boto_client('iam').delete_policy_version(PolicyArn=policy_arn, VersionId=oldest_version_id)

            self.log_fn(" - Creating a policy version")
            self._get_boto_client('iam').create_policy_version(
                PolicyArn=policy_arn,
                PolicyDocument=as_nice_json(arguments['PolicyDocument']),
                SetAsDefault=True
            )
            return

        # UPDATE::iam::create_user
        if function_name == 'create_user':
            current_user_name = arguments['UserName']
            client = self._get_boto_client('iam')

            if 'linkedPolicyArns' not in step_config:
                self.log_fn(
                    " %s Skipping verification of linked policies (as `linkedPolicyArns` is not provided)." % self.s_info
                )
            else:
                r = client.list_attached_user_policies(UserName=arguments['UserName'])

                current_linked_policy_arns = set(map(lambda rr: rr['PolicyArn'], r['AttachedPolicies']))
                desired_linked_policy_arns = set(self.replacer(copy.copy(step_config['linkedPolicyArns'])))
                additional_policies = current_linked_policy_arns - desired_linked_policy_arns
                missing_policies = desired_linked_policy_arns - current_linked_policy_arns

                if len(missing_policies) > 0:
                    for policy_arn in missing_policies:
                        self.log_fn(" - attaching the missing linked policy, %s ..." % policy_arn)
                        client.attach_user_policy(
                            UserName=arguments['UserName'],
                            PolicyArn=policy_arn
                        )
                if len(additional_policies) > 0:
                    for policy_arn in additional_policies:
                        self.log_fn(" - detaching the additional linked policy, %s ..." % policy_arn)
                        client.detach_user_policy(
                            UserName=arguments['UserName'],
                            PolicyArn=policy_arn
                        )
                if len(missing_policies) == 0 and len(additional_policies) == 0:
                    self.log_fn(" - all the linked policies are in sync.")

            if 'inlinePolicies' not in step_config:
                self.log_fn(" - skipping update of inline policies")
            else:
                self.log_fn(" - getting existing inline policies (to remove stale ones)")
                attached_policies = self._get_boto_client('iam').list_user_policies(UserName=arguments['UserName'])
                new_inline_policy_names = []
                new_inline_policy_names = list(map(lambda r: r['PolicyName'], step_config['inlinePolicies']))

                for policy_name in attached_policies['PolicyNames']:
                    if policy_name in new_inline_policy_names:
                        continue
                    self.log_fn(" - deleting inline policy `%s`" % policy_name)
                    self._get_boto_client('iam').delete_user_policy(UserName=arguments['UserName'], PolicyName=policy_name)

                for inline_policy in step_config['inlinePolicies']:
                    inline_policy_replaced = self.replacer(inline_policy)
                    self.log_fn(" - putting the inline policy, `%s`" % inline_policy_replaced['PolicyName'])

                    try:
                        inline_policy_document = as_nice_json(inline_policy_replaced['PolicyDocument'])
                        self._get_boto_client('iam').put_user_policy(
                            UserName=arguments['UserName'],
                            PolicyName=inline_policy_replaced['PolicyName'],
                            PolicyDocument=inline_policy_document
                        )
                    except botocore.exceptions.ClientError as the_exception:
                        if the_exception.response['Error']['Code'] == "MalformedPolicyDocument":
                            self.log_fn("=========================")
                            self.log_fn(inline_policy_document)
                            self.log_fn("=========================")
                        raise the_exception

            if 'groupNames' not in step_config:
                self.log_fn("- there are no groups requested - skipping update.")
            else:
                r = client.list_groups_for_user(UserName=arguments['UserName'])

                current_groups = set(map(lambda rr: rr['GroupName'], r['Groups']))
                desired_groups = set(self.replacer(copy.copy(step_config['groupNames'])))
                additional_groups = current_groups - desired_groups
                missing_groups = desired_groups - current_groups

                if len(missing_groups) > 0:
                    for group in missing_groups:
                        self.log_fn(" - adding user to the missing group, %s ..." % group)
                        client.add_user_to_group(
                            UserName=arguments['UserName'],
                            GroupName=group
                        )
                if len(additional_groups) > 0:
                    for group in additional_groups:
                        self.log_fn(" - removing user from the additional group, %s ..." % group)
                        client.remove_user_from_group(
                            UserName=arguments['UserName'],
                            GroupName=group
                        )
                if len(missing_groups) == 0 and len(additional_groups) == 0:
                    self.log_fn(" - all the groups are in sync.")

            if 'userDescription' not in step_config:
                self.log_fn(" - skipping update of the description.")
            else:
                self.log_fn(" - setting the description (even if it doesn't need changing).")
                self._get_boto_client('iam').update_user_description(
                    UserName=current_user_name,
                    Description=self.replacer(copy.copy(step_config['userDescription']))
                )

            return

        # UPDATE::iam::create_role
        if function_name == 'create_role':
            current_role_name = arguments['RoleName']
            client = self._get_boto_client('iam')

            if 'AssumeRolePolicyDocument' in arguments:
                self.log_fn(" - updating assume role policy")
                self._get_boto_client('iam').update_assume_role_policy(
                    RoleName=arguments['RoleName'],
                    PolicyDocument=as_nice_json(arguments['AssumeRolePolicyDocument'])
                )

            if 'linkedPolicyArns' not in step_config:
                self.log_fn(" %s WARNING - there are no linked policies requested - skipping update (if linked policies exist they will NOT be removed)." % self.s_info)
            else:
                r = client.list_attached_role_policies(RoleName=arguments['RoleName'])

                current_linked_policy_arns = set(map(lambda rr: rr['PolicyArn'], r['AttachedPolicies']))
                desired_linked_policy_arns = set(self.replacer(copy.copy(step_config['linkedPolicyArns'])))
                additional_policies = current_linked_policy_arns - desired_linked_policy_arns
                missing_policies = desired_linked_policy_arns - current_linked_policy_arns

                if len(missing_policies) > 0:
                    for policy_arn in missing_policies:
                        self.log_fn(" - attaching the missing linked policy, %s ..." % policy_arn)
                        client.attach_role_policy(
                            RoleName=arguments['RoleName'],
                            PolicyArn=policy_arn
                        )
                if len(additional_policies) > 0:
                    for policy_arn in additional_policies:
                        self.log_fn(" - detaching the additional linked policy, %s ..." % policy_arn)
                        client.detach_role_policy(
                            RoleName=arguments['RoleName'],
                            PolicyArn=policy_arn
                        )
                if len(missing_policies) == 0 and len(additional_policies) == 0:
                    self.log_fn(" - all the linked policies are in sync.")

            if 'inlinePolicies' not in step_config:
                self.log_fn(" - skipping update of inline policies")
            else:
                self.log_fn(" - getting existing inline policies (to remove stale ones)")
                attached_policies = self._get_boto_client('iam').list_role_policies(RoleName=arguments['RoleName'])
                new_inline_policy_names = []
                new_inline_policy_names = list(map(lambda r: r['PolicyName'], step_config['inlinePolicies']))

                for policy_name in attached_policies['PolicyNames']:
                    if policy_name in new_inline_policy_names:
                        continue
                    self.log_fn(" - deleting inline policy `%s`" % policy_name)
                    self._get_boto_client('iam').delete_role_policy(RoleName=arguments['RoleName'], PolicyName=policy_name)

                for inline_policy in step_config['inlinePolicies']:
                    inline_policy_replaced = self.replacer(inline_policy)
                    self.log_fn(" - putting the inline policy, `%s`" % inline_policy_replaced['PolicyName'])

                    try:
                        inline_policy_document = as_nice_json(inline_policy_replaced['PolicyDocument'])
                        self._get_boto_client('iam').put_role_policy(
                            RoleName=arguments['RoleName'],
                            PolicyName=inline_policy_replaced['PolicyName'],
                            PolicyDocument=inline_policy_document
                        )
                    except botocore.exceptions.ClientError as the_exception:
                        if the_exception.response['Error']['Code'] == "MalformedPolicyDocument":
                            self.log_fn("=========================")
                            self.log_fn(inline_policy_document)
                            self.log_fn("=========================")
                        raise the_exception

            args = {
                'RoleName': current_role_name,
            }

            if 'roleDescription' in step_config:
                self.log_fn(" - preparing to set the description (even it doesn't need changing).")
                args['Description'] = self.replacer(copy.copy(step_config['roleDescription']))

            if 'MaxSessionDuration' in arguments:
                self.log_fn(" - preparing to set the MaxSessionDuration (even if it doesn't need changing).")
                args['MaxSessionDuration'] = arguments['MaxSessionDuration']

            # If Description or MaxSessionDuration have been set, update the role
            if len(args) > 1:
                self.log_fn(" - updating role")
                self._get_boto_client('iam').update_role(**args)

            return

        # UPDATE::iam::create_group
        if function_name == 'create_group':
            client = self._get_boto_client('iam')

            arguments = self.replacer(copy.copy(step_config['arguments']))
            self.log_fn(" - Updating the IAM group, `%s`." % arguments['GroupName'])

            if 'linkedPolicyArns' not in step_config:
                self.log_fn("- there are no linked policies requested - skipping update.")
            else:
                r = client.list_attached_group_policies(
                    GroupName=arguments['GroupName']
                )
                current_linked_policy_arns = set(map(lambda rr: rr['PolicyArn'], r['AttachedPolicies']))
                desired_linked_policy_arns = set(self.replacer(copy.copy(step_config['linkedPolicyArns'])))
                additional_policies = current_linked_policy_arns - desired_linked_policy_arns
                missing_policies = desired_linked_policy_arns - current_linked_policy_arns

                if len(missing_policies) > 0:
                    for policy_arn in missing_policies:
                        self.log_fn("- Attaching the missing linked policy, %s ..." % policy_arn)
                        client.attach_group_policy(
                            GroupName=arguments['GroupName'],
                            PolicyArn=policy_arn
                        )
                if len(additional_policies) > 0:
                    for policy_arn in additional_policies:
                        self.log_fn("- Detaching the additional linked policy, %s ..." % policy_arn)
                        client.detach_group_policy(
                            GroupName=arguments['GroupName'],
                            PolicyArn=policy_arn
                        )
                if len(missing_policies) == 0 and len(additional_policies) == 0:
                    self.log_fn(" - All the linked policies are in sync.")

            if 'inlinePolicies' not in step_config:
                self.log_fn(" - skipping update of inline policies")
            else:
                self.log_fn(" - getting existing inline policies (to remove stale ones)")
                attached_policies = self._get_boto_client('iam').list_group_policies(GroupName=arguments['GroupName'])
                new_inline_policy_names = []
                new_inline_policy_names = list(map(lambda r: r['PolicyName'], step_config['inlinePolicies']))

                for policy_name in attached_policies['PolicyNames']:
                    if policy_name in new_inline_policy_names:
                        continue
                    self.log_fn(" - deleting inline policy `%s`" % policy_name)
                    self._get_boto_client('iam').delete_group_policy(GroupName=arguments['GroupName'], PolicyName=policy_name)

                for inline_policy in step_config['inlinePolicies']:
                    inline_policy_replaced = self.replacer(inline_policy)
                    self.log_fn(" - putting the inline policy, `%s`" % inline_policy_replaced['PolicyName'])

                    self._get_boto_client('iam').put_group_policy(
                        GroupName=arguments['GroupName'],
                        PolicyName=inline_policy_replaced['PolicyName'],
                        PolicyDocument=as_nice_json(inline_policy_replaced['PolicyDocument'])
                    )

            return

        # UPDATE::iam::create_account_alias
        if function_name == "create_account_alias":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            self.log_fn(" - creating (/updating) account alias to `%s`" % arguments['AccountAlias'])
            client = self._get_boto_client('iam')
            r = client.create_account_alias(**arguments)
            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def _update_step_lambda(self, step_config):
        function_name = step_config['function']

        # UPDATE::lambda::publish_layer_version
        if function_name == 'publish_layer_version':
            self.log_fn(" - updating task definition using the 'play' since both 'play' and 'update' involve creating a new version:")
            self.log_fn(" - ")
            self.play_step(step_config)
            return

        # UPDATE::lambda::create_function
        if function_name == "create_function":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            client = self._get_boto_client_for_step('lambda', step_config)
            args = {
                'FunctionName': arguments['FunctionName']
            }
            valid_arg_keys = [
                'Role', 'Handler', 'Description', 'Timeout', 'MemorySize', 'VpcConfig',
                'Environment', 'Runtime', 'DeadLetterConfig', 'KMSKeyArn', 'TracingConfig'
            ]
            for valid_arg_key in valid_arg_keys:
                if valid_arg_key in arguments:
                    args[valid_arg_key] = arguments[valid_arg_key]

            self.log_fn(" - updating function configuration")
            r = client.update_function_configuration(**args)
            current_code_sha256, current_code_size = r['CodeSha256'], r['CodeSize']

            arguments_code = arguments.get('Code',{})
            if 'S3Bucket' in arguments_code and 'S3Key' in arguments_code:
                desired_code_sha256, desired_code_size = praws.utils.cached_get_s3_zip_lambda_signature(
                    arguments_code['S3Bucket'],
                    arguments_code['S3Key'],
                    self._get_lambda_cache_filename(),
                    use_cache=True,
                    s3_client=self._get_boto_client("s3")
                )
                if desired_code_sha256 == current_code_sha256 and desired_code_size == current_code_size:
                    self.log_fn(" - the source code is has matching signature - avoiding any update")
                else:
                    self.log_fn(" - updating function source code")
                    args = {
                        'FunctionName': arguments['FunctionName']
                    }
                    args.update(arguments['Code'])
                    client.update_function_code(**args)
            else:
                self.log_fn(" %s WARNING - not sure how to update / verify source code" % self.s_info)

            if 'permissionsByStatementId' not in step_config:
                self.log_fn(" - Warning - the key `permissionsByStatementId` is not in the step_config so it cannot be updated")
            else:
                desired_permissions_by_statement_id = self.replacer(step_config['permissionsByStatementId'])
                try:
                    current_policy = json.loads(
                        client.get_policy(FunctionName=arguments['FunctionName']).get('Policy', '{"Statement":[]}')
                    )
                except botocore.exceptions.ClientError as e:
                    if e.response['Error']['Code'] == "ResourceNotFoundException":
                        current_policy = {"Statement":[]}
                    else:
                        raise

                current_statement_ids = [x['Sid'] for x in current_policy["Statement"]]

                self.log_fn(" - Removing permissions (its not possible to update permissions)")
                # ToDo - Use verification to compare
                for current_statement_id in current_statement_ids:
                    self.log_fn(" - Removing permission for statement id, `%s`" % current_statement_id)
                    client.remove_permission(FunctionName=arguments['FunctionName'], StatementId=current_statement_id)

                self.log_fn(" - Adding permissions:")
                for statement_id in desired_permissions_by_statement_id:
                    self.log_fn(" - Adding / (Updating) permission for statement id, `%s`" % statement_id)
                    permission_arguments = desired_permissions_by_statement_id[statement_id]
                    permission_arguments['StatementId'] = statement_id
                    permission_arguments['FunctionName'] = arguments['FunctionName']
                    client.add_permission(**permission_arguments)

            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def _update_step_s3(self, step_config):
        function_name = step_config['function']

        # UPDATE::s3::put_bucket_notification_configuration
        if function_name == "put_bucket_notification_configuration":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            client = self._get_boto_client('s3')
            self.log_fn(" - putting bucket notification configuration")
            client.put_bucket_notification_configuration(**arguments)
            return

        # UPDATE::s3::put_bucket_policy
        # - turn Policy argument into a string.
        if function_name == 'put_bucket_policy':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            arguments['Policy'] = as_nice_json(arguments['Policy'])

            client = self._get_boto_client('s3')
            r = client.put_bucket_policy(**arguments)
            return

        # UPDATE::s3::put_bucket_versioning
        if function_name == "put_bucket_versioning":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            client = self._get_boto_client('s3')
            self.log_fn(" - putting bucket versioning")
            client.put_bucket_versioning(Bucket=arguments['Bucket'], VersioningConfiguration=arguments['VersioningConfiguration'])
            return

        # UPDATE::s3::put_bucket_lifecycle_configuration
        if function_name == "put_bucket_lifecycle_configuration":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            client = self._get_boto_client('s3')
            self.log_fn("- putting the lifecycle_configuration")
            client.put_bucket_lifecycle_configuration(**arguments)
            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def _update_step_ses(self, step_config):
        function_name = step_config['function']

        arguments = self.replacer(copy.copy(step_config['arguments']))

        # UPDATE::ses::custom:set_active_receipt_rule_set
        if function_name == "custom:set_active_receipt_rule_set":
            ses_client = self._get_boto_client_for_step('ses', step_config)
            r = ses_client.describe_active_receipt_rule_set()
            current_rule_name = r['Metadata']['Name']
            rule_name_prefix = arguments['ruleNamePrefix']
            desired_rules = arguments['Rules']
            active_rules = r['Rules']
            def _add_defaults(r):
                r['Enabled'] = r.get('Enabled', True)
                r['ScanEnabled'] = r.get('ScanEnabled', True)
                r['TlsPolicy'] = r.get('TlsPolicy', 'Optional')
                return r
            desired_rules = list(map(_add_defaults, desired_rules))

            dd = deepdiff.DeepDiff(desired_rules, active_rules, ignore_order=True)
            if len(dd) == 0:
                self.log_fn("- The current ruleset matches - no changes needed")
                return

            ruleset_name = rule_name_prefix + "-" + datetime.datetime.utcnow().strftime("%Y-%m-%d-%H%M%S")

            self.log_fn("- creating a recipient rule set with the name `%s`" % ruleset_name)
            ses_client.create_receipt_rule_set(RuleSetName=ruleset_name)
            for rule in desired_rules:
                self.log_fn("- Adding the rule, `%s`" % rule['Name'])
                ses_client.create_receipt_rule(RuleSetName=ruleset_name, Rule=rule)

            self.log_fn("- setting active receipt rule set")
            ses_client.set_active_receipt_rule_set(RuleSetName=ruleset_name)
            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def _update_step_sns(self, step_config):
        function_name = step_config['function']

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        # UPDATE::sns::set_topic_attributes
        if function_name == 'set_topic_attributes':
            self.log_fn(" - updating topic attributes")
            self._play_step(step_config)
            return

        # UPDATE::sns::create_topic
        if function_name == 'create_topic':
            client = self._get_boto_client('sns')
            r = client.list_topics()
            topic_names_by_arn = {x['TopicArn'].split(":")[-1]:x['TopicArn'] for x in r['Topics']}
            if arguments['Name'] not in topic_names_by_arn:
                raise Exception(
                    "- SNS Topic with the name, `%s` does NOT exist" % arguments['Name']
                )

            topic_arn = topic_names_by_arn[arguments['Name']]

            if 'topicPolicy' not in step_config:
                self.log_fn("%s topicPolicy not specified - not updating!" % (self.s_info))
            else:
                self.log_fn("- updating topicPolicy")
                self._current_sns_topic_arn = topic_arn
                desired_topic_policy = self.replacer(copy.copy(step_config['topicPolicy']))
                self._current_sns_topic_arn = None
                client.set_topic_attributes(
                    TopicArn=topic_arn,
                    AttributeName='Policy',
                    AttributeValue=as_nice_json(desired_topic_policy)
                )
            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def _update_step_sqs(self, step_config):
        function_name = step_config['function']
        arguments = self.replacer(copy.copy(step_config['arguments']))

        # UPDATE::sqs::create_queue
        if function_name == 'create_queue':
            queue_name = arguments['QueueName']
            sqs_queue_url = self.replacer("https://__AWS_REGION__.queue.amazonaws.com/__ACCOUNT_ID__/%s" % queue_name)
            queue_arn = self.replacer("arn:aws:sqs:__AWS_REGION__:__ACCOUNT_ID__:%s" % queue_name)

            attributes_to_push = {}
            if 'Attributes' in arguments:
                attributes_to_push.update(arguments['Attributes'])
            if 'queueAttributes' in step_config:
                self._current_sqs_queue_url = sqs_queue_url
                self._current_sqs_queue_arn = queue_arn
                extra_attributes = self.replacer(copy.copy(step_config['queueAttributes']))
                attributes_to_push.update(extra_attributes)
                self._current_sqs_queue_url = None
                self._current_sqs_queue_arn = None

            if 'Policy' in extra_attributes:
                extra_attributes['Policy'] = as_nice_json(extra_attributes['Policy'])
            self._get_boto_client('sqs').set_queue_attributes(
                QueueUrl=sqs_queue_url,
                Attributes=extra_attributes
            )
            return

        # UPDATE::sqs::set_queue_attributes
        if function_name == 'set_queue_attributes':
            self.log_fn(" - updating SQS topic attributes")
            self._play_step(step_config)
            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    # ==================================== REVERT STEPS ====================================
    def _revert_step(self, step_config):
        if step_config['client'] == 'apigateway':
            return self._builders['apigateway']._revert_step(step_config)

        if step_config['client'] == 'ecs':
            return self._revert_step_ecs(step_config)

        if step_config['client'] == 'elbv2':
            return self._revert_step_elbv2(step_config)

        if step_config['client'] == 'events':
            return self._revert_step_cloudwatch_events(step_config)

        if step_config['client'] == 'iam':
            return self._revert_step_iam(step_config)

        if step_config['client'] == 'lambda':
            return self._revert_step_lambda(step_config)

        if step_config['client'] == 'logs':
            return self._revert_step_cloud_watch_logs(step_config)

        if step_config['client'] == 'organizations':
            return self._revert_step_organizations(step_config)

        if step_config['client'] == 's3':
            return self._revert_step_s3(step_config)

        if step_config['client'] == 'sdb':
            return self._revert_step_sdb(step_config)

        if step_config['client'] == 'ses':
            return self._revert_step_ses(step_config)

        if step_config['client'] == 'sqs':
            return self._revert_step_sqs(step_config)

        if step_config['client'] == 'sns':
            return self._revert_step_sns(step_config)

        if step_config['client'] == 'route53':
            return self._revert_step_route53(step_config)

        raise ValueError("Unknown client, `" + step_config['client'] + "`")



    def _revert_step_ecs(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('ecs')
        function_name = step_config['function']

        # REVERT::ecs::create_cluster
        if function_name == 'create_cluster':
            cluster_name = arguments['clusterName']
            try:
                cluster_instance_arns = client.list_container_instances(cluster=cluster_name)['containerInstanceArns']
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ClusterNotFoundException":
                    self.log_fn(" - Cluster does not exist, (assume already deleted)")
                    return
                else:
                    raise e

            self.log_fn(" - Deregistering %d container instance(s)" % len(cluster_instance_arns))

            for cluster_instance_arn in cluster_instance_arns:
                client.deregister_container_instance(cluster=cluster_name, containerInstance=cluster_instance_arn)

            start_time = time.time()
            this_time = start_time
            instance_count = 99
            max_wait_time = 60
            while (instance_count > 0) and ((this_time - start_time) < max_wait_time):
                cluster_instance_arns = client.list_container_instances(cluster=cluster_name)['containerInstanceArns']
                instance_count = len(cluster_instance_arns)
                this_time = time.time()

            if instance_count > 0:
                self.log_fn("WARNING: Instance count is still greater than 0 after %d seconds" % max_wait_time)

            client.delete_cluster(cluster=cluster_name)
            return

        # REVERT::ecs::register_task_definition
        if function_name == 'register_task_definition':
            task_definition_arns = client.list_task_definitions(familyPrefix=arguments['family'])['taskDefinitionArns']
            self.log_fn(" - Deregistering %d task definitions" % len(task_definition_arns))
            for task_definition_arn in task_definition_arns:
                client.deregister_task_definition(taskDefinition=task_definition_arn)
                self.log_fn(".")
            self.log_fn(
                "    Note that task definitions are made INACTIVE, but not deleted, (AWS does not delete definitions)"
            )
            return

        # REVERT::ecs::create_service
        if function_name == 'create_service':
            try:
                services_info = client.describe_services(
                    cluster=arguments['cluster'], services=[arguments['serviceName']]
                ).get('services')
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ClusterNotFoundException":
                    self.log_fn(" - Cluster does not exist, (assume already deleted)")
                    return
                else:
                    raise e

            if len(services_info) == 0:
                self.log_fn(" - Not present, nothing to do")
                return

            if services_info[0]['status'] == 'INACTIVE':
                self.log_fn(" - Already inactive, nothing to do.")
                return

            self.log_fn(" - Updating service to have 0 desired..")
            client.update_service(cluster=arguments['cluster'], service=arguments['serviceName'], desiredCount=0)
            start_time = time.time()
            this_time = start_time

            instance_count = 99
            max_wait_time = 60
            while (instance_count > 0) and ((this_time - start_time) < max_wait_time):
                time.sleep(0.5)
                this_time = time.time()
                services_info = client.describe_services(
                    cluster=arguments['cluster'],
                    services=[arguments['serviceName']]
                ).get('services')
                instance_count = services_info[0]['runningCount']
                sys.stdout.write(".%d" % instance_count),
                sys.stdout.flush()
            print("")

            if instance_count > 0:
                self.log_fn("WARNING: Running count is still greater than 0 after %d seconds" % max_wait_time)

            self.log_fn(" - deleting service")
            client.delete_service(cluster=arguments['cluster'], service=arguments['serviceName'])
            self.log_fn(".")
            return

        raise ValueError("Unhandled revert for function ecs.`" + function_name + "`")

    def _revert_step_organizations(self, step_config):
        function_name = step_config['function']

        # REVERT::organizations::custom:accept_handshake
        if function_name == 'custom:accept_handshake':
            fromOrganizationId = step_config["fromOrganizationId"]
            if callable(fromOrganizationId):
                fromOrganizationId = fromOrganizationId()

            client = self._get_boto_client('organizations')

            try:
                r = client.describe_organization()
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "AWSOrganizationsNotInUseException":
                    self.log_fn(" - Account is not part of an organization (nothing to leave).")
                    return
                else:
                    raise e

            if r["Organization"]["Id"].replace("o-", "") != fromOrganizationId:
                raise Exception("The account is part of a different organization (%s) than that which you are requesting it to leave!" % r["Organization"]["Id"])
            self.log_fn("About to leave organization - This operation can be called only from a member account in the organization.")
            # We could create a user that is able to leave the org, and use their credentials, but for now we'll leave this as is.
            client.leave_organization()
            return

        # REVERT::organizations::invite_account_to_organization
        if function_name == "invite_account_to_organization":
            arguments = copy.copy(step_config['arguments'])
            arguments = self.replacer(arguments)

            target_account_id = arguments['Target']["Id"]

            target_type = arguments['Target']["Type"]
            account_handshakes = self.get_organization_handshakes_for_account(target_account_id, target_type)

            if len(account_handshakes) == 0:
                self.log_fn(" - No handshakes exist for the account, %d" % target_account_id)
                return

            handshakes_by_states = reduce(_state_grouper, account_handshakes, {})
            client = self._get_boto_client('organizations')

            #possible states are: REQUESTED, OPEN, CANCELED, ACCEPTED, DECLINED, EXPIRED

            if "OPEN" in handshakes_by_states:
                for open_handshake in handshakes_by_states["OPEN"]:
                    self.log_fn(" - cancelling handshape, %s" % open_handshake['Arn'])
                    client.cancel_handshake(HandshakeId=open_handshake['Id'])
                del handshakes_by_states["OPEN"]
            else:
                self.log_fn(" - No OPEN handshakes. ")

            if "ACCEPTED" in handshakes_by_states:
                self.log_fn(" - handshakes exist in ACCEPTED state, seeing if the account is still in the organization.")

                accounts = self._get_all_accounts_in_organization()
                linked_accounts_info = list(filter(lambda r: r['Id'] == target_account_id, accounts))
                if len(linked_accounts_info) > 1:
                    raise Exception("Not sure how to handle more than 1 record for an account");

                if len(linked_accounts_info) == 1:
                    raise Exception("The handshake has been accepted, need to determine how to revert in this instance.")

                if len(linked_accounts_info) == 0:
                    self.log_fn(" - the account has had an accepted handshake, but is no longer in the organisation.")
                del handshakes_by_states["ACCEPTED"]

            for state_to_ignore in ["CANCELED", "DECLINED", "EXPIRED"]:
                if state_to_ignore in handshakes_by_states:
                    del handshakes_by_states[state_to_ignore]

            if len(handshakes_by_states.keys()) == 0:
                return

            pprint(handshakes_by_states.keys())
            raise Exception("Handshakes exist having un-supported states.")

        # REVERT::organizations::move_account
        if function_name == 'move_account':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            if 'RevertDestinationParentId' not in step_config:
                raise Exception("Unable to revert the organization:move_account unless the property, 'RevertDestinationParentId' is set")
            arguments['DestinationParentId'] = self.replacer(copy.copy(step_config['RevertDestinationParentId']))
            self.log_fn(" - Moving account to parent, %s" % arguments['DestinationParentId'])
            self._get_boto_client('organizations').move_account(**arguments)
            return

        raise ValueError("Unhandled revert for organization function, `" + function_name + "`")

    def _revert_step_s3(self, step_config):
        step_config = copy.copy(step_config)
        step_config = self.replacer(step_config, ignore_not_found=True)
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        function_name = step_config['function']

        # REVERT::s3::create_bucket
        if function_name == 'create_bucket':
            if 'envVariableRequiredForDeletion' in step_config:
                ename = step_config['envVariableRequiredForDeletion']
                if ename not in os.environ or os.environ[ename] != "true":
                    raise ValueError(
                        "You must set the environment variable, '%s' to 'true' to delete the bucket" % ename
                    )
            self.log_fn(" - deleting the bucket `%s`" % arguments['Bucket'])
            self._get_boto_client('s3').delete_bucket(Bucket=arguments['Bucket'])
            return

        # REVERT::s3::put_bucket_notification_configuration
        if function_name == 'put_bucket_notification_configuration':
            self.log_fn(" - wiping bucket_notification_configuration for `%s`" % arguments['Bucket'])

            self._get_boto_client('s3').put_bucket_notification_configuration(
                Bucket=arguments['Bucket'], NotificationConfiguration={}
            )
            return

        # REVERT::s3::put_bucket_policy
        if function_name == "put_bucket_policy":
            arguments = copy.copy(step_config['arguments'])
            client = self._get_boto_client('s3')
            client.delete_bucket_policy(Bucket=arguments["Bucket"])
            return

        # REVERT::s3::put_bucket_versioning
        if function_name == "put_bucket_versioning":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            versioning_configuration_on_revert = self.replacer(copy.copy(step_config['VersioningConfigurationOnRevert']))
            client = self._get_boto_client('s3')
            self.log_fn(" - Pushing the versioning state as specified in the key, `VersioningConfigurationOnRevert`")
            client.put_bucket_versioning(Bucket=arguments["Bucket"], VersioningConfiguration=versioning_configuration_on_revert)
            return

        # REVERT::s3::put_bucket_lifecycle_configuration
        # - when the optiona step argument, disableRulesOnRevert is set to True, then the rules are put, but with all of them disabled.
        #   otherwise the lifecycle config is.
        if function_name == "put_bucket_lifecycle_configuration":
            arguments = self.replacer(copy.copy(step_config['arguments']))
            client = self._get_boto_client('s3')
            if step_config.get('disableRulesOnRevert', False):
                self.log_fn(" - putting the lifecycle configuration rules, with the rules disabled")
                def _disable_rules(r):
                    r['Status'] = 'Disabled'
                    return r
                arguments['LifecycleConfiguration']['Rules'] = list(map(_disable_rules, arguments['LifecycleConfiguration']['Rules']))
                client.put_bucket_lifecycle_configuration(**arguments)
            else:
                self.log_fn(" - deleting the lifecycle rules")
                client.delete_bucket_lifecycle(Bucket=arguments["Bucket"])
            return

        raise ValueError("Unhandled revert for function s3.`" + function_name + "`")

    def _revert_step_sdb(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        client = self._get_boto_client('sdb')
        function_name = step_config['function']

        # REVERT::sdb::create_domain
        # - check for 'envVariableRequiredForDeletion'
        if function_name == 'create_domain':
            if 'envVariableRequiredForDeletion' in step_config:
                ename = step_config['envVariableRequiredForDeletion']
                if ename not in os.environ or os.environ[ename] != "true":
                    raise ValueError(
                        "You must set the environment variable, '%s' to 'true' to delete the bucket" % ename
                    )
            self.log_fn(" - Deleting SDB domain, `%s`" % arguments['DomainName'])
            self._get_boto_client('sdb').delete_domain(DomainName=arguments['DomainName'])
            return

        raise ValueError("Unhandled revert for function sdb.`" + function_name + "`")

    def _revert_step_ses(self, step_config):
        step_config = self.replacer(copy.copy(step_config), ignore_not_found=True)

        function_name = step_config['function']
        client = self._get_boto_client_for_step('ses', step_config)

        # REVERT::ses::custom:verify_domain_identity_and_add_txt_record_to_route53
        if function_name == 'custom:verify_domain_identity_and_add_txt_record_to_route53':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            email_domain = re.sub("\.+$", "", arguments['Domain'])
            self.log_fn(" - deleting identity for email domain, `%s`" % email_domain)
            client.delete_identity(Identity=email_domain)
            return

        raise ValueError("Unhandled revert for function s3.`" + function_name + "`")

    def _revert_step_sqs(self, step_config):
        step_config = copy.copy(step_config)
        step_config = self.replacer(step_config, ignore_not_found=True)
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        function_name = step_config['function']

        client = self._get_boto_client('sqs')

        if function_name == 'create_queue':
            queue_name = arguments['QueueName']

            self.log_fn(" - looking up arn...")
            resp = client.list_queues()
            if 'QueueUrls' not in resp:
                self.log_fn("- It appears there aren't any queues.. assume it's already deleted.")
                return

            matching_queue_urls = list(filter(lambda x: x.split("/")[-1] == queue_name, resp['QueueUrls']))

            if len(matching_queue_urls) == 0:
                self.log_fn(" - the queue could not be found, is gone.")
                return
            if len(matching_queue_urls) > 1:
                raise Exception("Too many SQS queues")

            queue_url = matching_queue_urls[0]

            self.log_fn(" - deleting the queue `%s`" % arguments['QueueName'])
            client.delete_queue(QueueUrl=queue_url)
            return

        raise ValueError("Unhandled revert for function sdb.`" + function_name + "`")

    def _revert_step_sns(self, step_config):
        step_config = copy.copy(step_config)
        step_config = self.replacer(step_config, ignore_not_found=True)
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        function_name = step_config['function']

        if 'region' in step_config:
            client = boto3.client('sns', region_name=self.replacer(step_config['region']))
            print("WARNING: please use `aws_region_name` instead of `region`")
        else:
            client = self._get_boto_client_for_step('sns', step_config)

        # REVERT::sns::create_topic
        if function_name == 'create_topic':
            self.log_fn(" - looking up arn for topic with name %s..." % arguments['Name'])
            r = client.list_topics()
            topic_names_by_arn = {x['TopicArn'].split(":")[-1]:x['TopicArn'] for x in r['Topics']}
            if arguments['Name'] not in topic_names_by_arn:
                self.log_fn(" - SNS topic ARN could not be found, assuming already deleted!")
                return

            topic_arn = topic_names_by_arn[arguments['Name']]

            self.log_fn(" - deleting the SNS topic `%s`" % arguments['Name'])
            client.delete_topic(TopicArn=topic_arn)
            return

        # REVERT::sns::subscribe
        if function_name == 'subscribe':
            self.log_fn(" - getting existing subscriptions...")
            r = client.list_subscriptions_by_topic(TopicArn=arguments['TopicArn'])
            for subscription in r['Subscriptions']:
                if subscription['Endpoint'] == arguments['Endpoint']:
                    pprint(subscription['SubscriptionArn'])
                    self.log_fn(" - unsubscribing...")
                    client.unsubscribe(SubscriptionArn=subscription['SubscriptionArn'])
                    return

            self.log_fn(" - no matching subscription found, assuming it's unsubscribed.")
            return

        raise ValueError("Unhandled revert for function sdb.`" + function_name + "`")

    def _revert_step_route53(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        client = self._get_boto_client("route53")
        function_name = step_config['function']

        # REVERT::route53::create_hosted_zone
        if function_name == "create_hosted_zone":
            self.log_fn(" - looking up info about the hosted zone, with name, `%s`" % arguments['Name'])
            hosted_zone_ids = get_hosted_zones_for_domain_name(client, arguments['Name'])

            if len(hosted_zone_ids) == 0:
                self.log_fn(" - Hosted zone with name, `%s` does not exists, assumed already deleted" % arguments['Name'])
                return

            hosted_zone_id = hosted_zone_ids[0]
            self.log_fn(" - deleting hosted zone, with name, `%s` and id, `%s`" % (arguments['Name'], hosted_zone_id))
            client.delete_hosted_zone(Id=hosted_zone_id)

            # Force the next request for hosted zones to refresh itself.
            self.hosted_zones = None
            return

        raise ValueError("Unhandled revert for function route53.`" + function_name + "`")

    def _revert_step_elbv2(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        client = self._get_boto_client('elbv2')
        function_name = step_config['function']

        # REVERT::elbv2::create_target_group
        if function_name == "create_target_group":
            try:
                r = client.describe_target_groups(Names=[arguments['Name']])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "TargetGroupNotFound":
                    self.log_fn(" - target-group not found (assume already deleted)")
                    return
                raise e
            target_groups = r.get('TargetGroups', [])
            if len(target_groups) == 0:
                self.log_fn(" - load-balancer not found (assume already deleted)")
                return
            if len(target_groups) != 1:
                raise Exception("Found too many target groups")

            target_group_arn = target_groups[0]['TargetGroupArn']
            r = client.delete_target_group(TargetGroupArn=target_group_arn)
            return

        # REVERT::elbv2::create_load_balancer
        if function_name == "create_load_balancer":
            try:
                r = client.describe_load_balancers(Names=[arguments['Name']])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "LoadBalancerNotFound":
                    self.log_fn(" - load-balancer not found (assume already deleted)")
                    return
                raise e

            load_balancers = r.get('LoadBalancers', [])
            if len(load_balancers) == 0:
                self.log_fn(" - load-balancer not found (assume already deleted)")
                return
            if len(load_balancers) != 1:
                raise Exception("Found too many ")

            load_balancer_arn = load_balancers[0]['LoadBalancerArn']
            r = client.delete_load_balancer(LoadBalancerArn=load_balancer_arn)
            return

        raise ValueError("Unhandled revert for function, {client}::{function}`".format(**step_config))

    def _revert_step_cloudwatch_events(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        client = self._get_boto_client('events')
        function_name = step_config['function']

        # REVERT::events::put_rule
        if function_name == "put_rule":
            r_targets = client.list_targets_by_rule(Rule=arguments['Name'])
            current_targets = r_targets.get('Targets', [])
            if len(current_targets) > 0:
                self.log_fn("- removing the targets")
                client.remove_targets(Rule=arguments['Name'], Ids=[r['Id'] for r in current_targets])

            self.log_fn("- deleting the rule")
            client.delete_rule(Name=arguments['Name'])
            return

        raise ValueError("Unhandled revert for function, `" + function_name + "`")

    def _revert_step_iam(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        client = self._get_boto_client('iam')
        function_name = step_config['function']

        # REVERT::iam::upload_server_certificate
        if function_name == "upload_server_certificate":
            client.delete_server_certificate(ServerCertificateName=arguments['ServerCertificateName'])
            return

        # REVERT::iam::update_account_password_policy
        if function_name == 'update_account_password_policy':
            try:
                self.log_fn(" - Deleting account password policy")
                client.delete_account_password_policy()
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fn(" - Account password policy doesn't even exist deleted")
                    return
                else:
                    raise e
            return

        # REVERT::iam::create_user
        if function_name == "create_user":
            try:
                self.log_fn(" - getting list of inline policies")
                r = client.list_user_policies(UserName=arguments['UserName'])
                inline_policy_names = r['PolicyNames']

                self.log_fn(" - getting list of attached policies")
                r = client.list_attached_user_policies(UserName=arguments['UserName'])
                attached_policy_arns = list(map(lambda rr: rr['PolicyArn'], r['AttachedPolicies']))

                self.log_fn(" - getting list of groups")
                r = client.list_groups_for_user(UserName=arguments['UserName'])
                group_memberships = list(map(lambda rr: rr['GroupName'], r['Groups']))

            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    inline_policy_names = []
                    attached_policy_arns = []
                else:
                    raise e

            for policy_name in inline_policy_names:
                self.log_fn(" - deleting inline policy `%s`" % policy_name)
                client.delete_user_policy(UserName=arguments['UserName'], PolicyName=policy_name)

            for policy_arn in attached_policy_arns:
                self.log_fn(" - detaching the additional linked policy, %s ..." % policy_arn)
                client.detach_user_policy(UserName=arguments['UserName'], PolicyArn=policy_arn)

            for group in group_memberships:
                self.log_fn(" - removing from group, %s ..." % group)
                client.remove_user_from_group(UserName=arguments['UserName'], GroupName=group)

            self.log_fn(" - deleting the user, `%s`" % arguments['UserName'])
            try:
                client.delete_user(UserName=arguments['UserName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fn(" - user not found (assume already deleted)")
                else:
                    raise e
            return

        # REVERT::iam::create_group
        if function_name == "create_group":
            try:
                self.log_fn(" - getting list of inline policies")
                r = client.list_group_policies(GroupName=arguments['GroupName'])
                inline_policy_names = r['PolicyNames']

                self.log_fn(" - getting list of attached policies")
                r = client.list_attached_group_policies(GroupName=arguments['GroupName'])
                attached_policy_arns = list(map(lambda rr: rr['PolicyArn'], r['AttachedPolicies']))

                self.log_fn(" - getting list of users")
                r = client.get_group(GroupName=arguments['GroupName'])
                attached_users = list(map(lambda rr: rr['UserName'], r['Users']))

            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    inline_policy_names = []
                    attached_policy_arns = []
                    attached_users = []
                else:
                    raise e

            if len(attached_users):
                to_remove = ""
                for user_name in attached_users:
                    to_remove += "\n   aws iam remove-user-from-group --group-name %s --user-name %s\n" % (arguments['GroupName'], user_name)
                pprint(attached_users)
                raise Exception("Cannot delete group as there are still users in it!\n\nTo remove the users run the following:\n" + to_remove)

            for policy_name in inline_policy_names:
                self.log_fn(" - deleting inline policy `%s`" % policy_name)
                client.delete_group_policy(GroupName=arguments['GroupName'], PolicyName=policy_name)

            for policy_arn in attached_policy_arns:
                self.log_fn(" - detaching the additional linked policy, %s ..." % policy_arn)
                client.detach_group_policy(
                    GroupName=arguments['GroupName'],
                    PolicyArn=policy_arn
                )

            self.log_fn(" - deleting the group, `%s`" % arguments['GroupName'])
            try:
                client.delete_group(GroupName=arguments['GroupName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fn(" - group not found (assume already deleted)")
                else:
                    raise e
            return

        # REVERT::iam::create_role
        if function_name == "create_role":
            try:
                self.log_fn(" - getting list of inline policies")
                r = client.list_role_policies(RoleName=arguments['RoleName'])
                inline_policy_names = r['PolicyNames']

                self.log_fn(" - getting list of attached policies")
                r = client.list_attached_role_policies(RoleName=arguments['RoleName'])
                attached_policy_arns = list(map(lambda rr: rr['PolicyArn'], r['AttachedPolicies']))

            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    inline_policy_names = []
                    attached_policy_arns = []
                else:
                    raise e

            for policy_name in inline_policy_names:
                self.log_fn(" - deleting inline policy `%s`" % policy_name)
                client.delete_role_policy(RoleName=arguments['RoleName'], PolicyName=policy_name)

            for policy_arn in attached_policy_arns:
                self.log_fn(" - detaching the additional linked policy, %s ..." % policy_arn)
                client.detach_role_policy(RoleName=arguments['RoleName'], PolicyArn=policy_arn)

            self.log_fn(" - deleting the role, `%s`" % arguments['RoleName'])
            try:
                client.delete_role(RoleName=arguments['RoleName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fn(" - role not found (assume already deleted)")
                else:
                    raise e
            return

        # REVERT::iam::create_policy
        if function_name == "create_policy":
            policy_arn = self.replacer("arn:aws:iam::__ACCOUNT_ID__:policy/")+arguments['PolicyName']
            self.log_fn("- getting list of policy versions for policy arn, `%s`" % policy_arn)
            try:
                r = self._get_boto_client('iam').list_policy_versions(PolicyArn=policy_arn)
                for policy_version in filter(lambda x: not x['IsDefaultVersion'], r['Versions']):
                    self.log_fn("- deleting version, `%s`" % policy_version['VersionId'])
                    client.delete_policy_version(
                        PolicyArn=policy_arn,
                        VersionId=policy_version['VersionId']
                    )
                self.log_fn("- deleting policy")
                client.delete_policy(
                    PolicyArn=policy_arn
                )
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fn(" - Policy non-existent, assuming already deleted")
                    return
                else:
                    raise e

            return

        # REVERT::iam::create_instance_profile
        if function_name == "create_instance_profile":
            try:
                r = client.get_instance_profile(
                    InstanceProfileName=arguments['InstanceProfileName']
                )
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NoSuchEntity":
                    self.log_fn(" - Instance profile non-existent, assuming already deleted")
                    return
                else:
                    raise e
            for role_data in r['InstanceProfile']['Roles']:
                self.log_fn(" - Removing role from instance profile `%s`" % arguments['InstanceProfileName'])
                client.remove_role_from_instance_profile(
                    InstanceProfileName=arguments['InstanceProfileName'],
                     RoleName=role_data['RoleName']
                )
            self.log_fn(" - Deleting instance profile `%s`" % arguments['InstanceProfileName'])
            client.delete_instance_profile(InstanceProfileName=arguments['InstanceProfileName'])
            return

        # REVERT::iam::create_account_alias
        if function_name == "create_account_alias":
            self.log_fn(" - getting list of account aliases")
            r = client.list_account_aliases()
            if len(r['AccountAliases']) == 0:
                self.log_fn(" - no account aliases exist, assume already removed.")
                return

            if r['AccountAliases'][0] != arguments['AccountAlias']:
                raise Exception('The account alias is different to expected.  Please run update step first.')

            self.log_fn(" - Removing account alias")
            client.delete_account_alias(AccountAlias=arguments['AccountAlias'])
            return

        raise ValueError("Unhandled revert for function iam.`" + function_name + "`")

    def _revert_step_lambda(self, step_config):
        step_config = copy.copy(step_config)
        step_config = self.replacer(step_config, ignore_not_found=True)
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        function_name = step_config['function']

        # REVERT::lambda::create_function
        if function_name == 'create_function':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            client = self._get_boto_client_for_step('lambda', step_config)
            self.log_fn("- deleting lambda function with the name, `%s`" % arguments['FunctionName'])
            client.delete_function(FunctionName=arguments['FunctionName'])

            return

        # REVERT::lambda::publish_layer_version
        if function_name == 'publish_layer_version':
            arguments = self.replacer(copy.copy(step_config['arguments']))
            client = self._get_boto_client_for_step('lambda', step_config)
            r = client.list_layer_versions(LayerName=arguments['LayerName'])
            if len(r['LayerVersions']) == 0:
                print(" - Layers already deleted")
                return
            layer_versions = r['LayerVersions']
            for layer_version in layer_versions:
                print("Deleting layer version %d" % layer_version['Version'])
                r2 = client.delete_layer_version(LayerName=arguments['LayerName'], VersionNumber=layer_version['Version'])
            return

        raise ValueError("Unhandled revert for function lambda.`" + function_name + "`")

    def _revert_step_cloud_watch_logs(self, step_config):
        step_config = copy.copy(step_config)
        step_config = self.replacer(step_config, ignore_not_found=True)
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        function_name = step_config['function']

        # REVERT::logs::create_log_group
        if function_name == 'create_log_group':
            client = self._get_boto_client("logs")
            try:
                log_group_name = arguments['logGroupName']
                self.log_fn(" - Deleting log group, `%s`" % log_group_name)
                client.delete_log_group(logGroupName=log_group_name)
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ResourceNotFoundException":
                    self.log_fn(" - Log group not found, assume already previously deleted.")
                    return
                else:
                    raise e
            return

        raise ValueError("Unhandled revert for function cloud watch logs.`" + function_name + "`")

    def get_organization_handshakes_for_account(self, target_account_id, target_type):
        client = self._get_boto_client('organizations')

        self.log_fn(" - Lookup up own organization details")
        organization_id = self._get_organization_details()['Id']

        r = client.list_handshakes_for_organization()
        if len(r['Handshakes']) == 0:
            return []

        desired_parties = [
            {
                "Id": target_account_id,
                'Type': target_type
            },
            {'Id': organization_id.replace("o-", ""), 'Type': 'ORGANIZATION'}
        ]

        return list(filter(lambda r: deepdiff.DeepDiff(r['Parties'], desired_parties, ignore_order=True) == {}, r['Handshakes']))

    def _assert_or_create_comparison_json_files(self, desired, actual, basename, passing_message, failure_message):
            dd = deepdiff.DeepDiff(as_nice_json(desired), as_nice_json(actual), ignore_order=True)
            if len(dd) == 0:
                self.log_fn(" {plus} {message}".format(plus=self.plus, message=passing_message))
            else:
                self._create_comparison_json_files(basename, desired, actual, failure_message=failure_message)

    def _create_comparison_json_files(self, basename, desired, actual, failure_message=None):
        delta_dir = self.delta_dir if self.delta_dir is not None else ""

        os.makedirs(f'{self.temp_dir}/desired/{delta_dir}',  exist_ok=True)
        os.makedirs(f'{self.temp_dir}/actual/{delta_dir}',  exist_ok=True)

        outfile_desired = f"{self.temp_dir}/desired/{delta_dir}/{basename}.json"
        outfile_actual = f"{self.temp_dir}/actual/{delta_dir}/{basename}.json"
        compare_str = f"{self.temp_dir}/{{desired,actual}}/{delta_dir}/{basename}.json"

        if isinstance(desired, string_types) and isinstance(actual, string_types):
            with open(outfile_desired, "w") as f:
                f.write(desired)
            with open(outfile_actual, "w") as f:
                f.write(actual)
        else:
            with open(outfile_desired, "w") as f:
                f.write(as_nice_json(desired))
            with open(outfile_actual, "w") as f:
                f.write(as_nice_json(actual))

        if failure_message is not None:
            self.log_fail("{failureMessage}\n  - For details, compare:\n     {compare_str}".format(
                failureMessage=failure_message, compare_str=compare_str,
            ))

        return outfile_desired, outfile_actual

    @staticmethod
    def get_tag_value(tags, key):
        for item in tags:
            if item['Key'] == key:
                return item['Value']

        return None

    @staticmethod
    def subnet_response_to_keyed_list(subnet_response):
        """
        This method takes the response from describe_subnets and creates a key / value dictionary, where the key is
        the value of the tag with the name "Name" (if it exists) or the subnetid.
        """
        subnet_by_praws_key = {}
        for item in subnet_response:
            if 'Tags' in item:
                key = Builder.get_tag_value(item['Tags'], 'Name')
            else:
                key = item['SubnetId']

            if key is None:
                key = item['SubnetId']

            subnet_by_praws_key[key] = item

        return subnet_by_praws_key

    def _elbv2_synchronise_listener_certificates(self, client, listener_arn, desired_non_default_certificates, message_prefix=""):
        existing_certs = client.describe_listener_certificates(ListenerArn=listener_arn).get('Certificates', [])
        existing_non_default_cert_arns = set([r['CertificateArn'] for r in existing_certs if not r['IsDefault']])
        desired_non_default_cert_arns = set([r['CertificateArn'] for r in desired_non_default_certificates])

        cert_arns_to_remove = existing_non_default_cert_arns - desired_non_default_cert_arns
        cert_arns_to_add = desired_non_default_cert_arns - existing_non_default_cert_arns

        if len(cert_arns_to_remove) > 0:
            self.log_fn(message_prefix + " - removing undesired certificates")
            client.remove_listener_certificates(ListenerArn=listener_arn, Certificates=[{'CertificateArn': x} for x in cert_arns_to_remove])

        self.log_fn(message_prefix + " - adding missing certificates")
        for cert_arn in cert_arns_to_add:
            client.add_listener_certificates(ListenerArn=listener_arn, Certificates=[{'CertificateArn': cert_arn}])


    def _elbv2_synchronise_listener_rules(self, client, listener_arn, desired_non_default_rules, message_prefix=""):
        self.log_fn(message_prefix + " - listing existing rules")
        existing_rules = client.describe_rules(ListenerArn=listener_arn)['Rules']
        existing_non_default_rules = [r for r in existing_rules if not r['IsDefault']]
        self.log_fn(message_prefix + " - There exists %d non-default rules" % len(existing_non_default_rules))

        if len(desired_non_default_rules) == 0:
            self.log_fn(message_prefix + " - deleting ALL existing rules")
            for existing_non_default_rule in existing_non_default_rules:
                client.delete_rule(RuleArn=existing_non_default_rule['RuleArn'])
            return

        desired_with_hashes = [
            {'h': hashlib.sha224(json.dumps([r['Actions'], r['Conditions']], sort_keys=True).encode('ascii')).hexdigest(), 'r': r}
            for r in desired_non_default_rules
        ]
        existing_with_hashes = [
            {'h': hashlib.sha224(json.dumps([r['Actions'], r['Conditions']], sort_keys=True).encode('ascii')).hexdigest(), 'r': r}
            for r in existing_non_default_rules
        ]

        # Remove any rules that are no-longer needed
        desired_hashes = [r['h'] for r in desired_with_hashes]
        rules_to_delete = [h_r['r'] for h_r in existing_with_hashes if h_r['h'] not in desired_hashes]
        if len(rules_to_delete) == 0:
            self.log_fn(message_prefix + " - there are no additional non-default rules that what is desired")
        else:
            self.log_fn(message_prefix + " - removing %d non-default rules" % len(rules_to_delete))
            for r in rules_to_delete:
                client.delete_rule(RuleArn=r['RuleArn'])

        # Append missing rules
        next_priority = 1
        if len(existing_with_hashes) > 0:
            next_priority = max([int(r['r']['Priority']) for r in existing_with_hashes]) + 1

        pprint(next_priority)
        existing_hashes = [r['h'] for r in existing_with_hashes]
        rules_to_add = [h_r['r'] for h_r in desired_with_hashes if h_r['h'] not in existing_hashes]
        rules_added = []
        if len(rules_to_add) == 0:
            self.log_fn(message_prefix + " - all desired non-default rules are present")
        else:
            self.log_fn(message_prefix + " - adding %d desired non-default rules" % len(rules_to_add))
            for r2a in rules_to_add:
                rules_added.append(client.create_rule(ListenerArn=listener_arn, Priority=next_priority, **r2a)['Rules'][0])
                next_priority += 1

        added_with_hashes = [
            {'h': hashlib.sha224(json.dumps([r['Actions'], r['Conditions']], sort_keys=True).encode('ascii')).hexdigest(), 'r': r}
            for r in rules_added
        ]

        new_existing_with_hashes = existing_with_hashes + added_with_hashes
        new_existing_by_hashes = {r['h']: r['r'] for r in new_existing_with_hashes}

        new_rule_priorities = []
        the_priority = 1
        for h_r in desired_with_hashes:
            new_rule_priorities.append({
                'RuleArn': new_existing_by_hashes[h_r['h']]['RuleArn'],
                'Priority': the_priority
            })
            the_priority += 1

        if len(new_rule_priorities) > 0:
            self.log_fn(message_prefix + " - Setting the rule priorities")
            client.set_rule_priorities(RulePriorities=new_rule_priorities)

        return

    def _get_acm_certificate_tags(self, arn):
        if arn not in self._acm_certificate_tags_by_arn:
            acm_client = self._get_boto_client('acm')
            r = acm_client.list_tags_for_certificate(CertificateArn=arn)
            self._acm_certificate_tags_by_arn[arn] = {
                r['Key']: r['Value'] for r in r['Tags']
            }

        return self._acm_certificate_tags_by_arn[arn]

    def get_acm_certificates_for_tag_name(self, certificate_name):
        if certificate_name in self._acm_certificates_by_tag_name:
            return self._acm_certificates_by_tag_name[certificate_name]

        acm_client = self._get_boto_client('acm')

        if self._acm_certificate_summary_list is None:
            self._acm_certificate_summary_list = acm_client.list_certificates()['CertificateSummaryList']

        for cert_info in self._acm_certificate_summary_list:
            r = acm_client.list_tags_for_certificate(CertificateArn=cert_info['CertificateArn'])
            name_tag_info = [ti for ti in r['Tags'] if ti['Key'] == 'Name']
            if len(name_tag_info) > 0:
                this_certificate_tag_name = name_tag_info[0]['Value']
                self._acm_certificates_by_tag_name[this_certificate_tag_name] = cert_info['CertificateArn']
                if certificate_name == this_certificate_tag_name:
                    return self._acm_certificates_by_tag_name[this_certificate_tag_name]

        return None

    def get_ssl_certificate_id_for_name(self, ssl_certificate_name):
        if ssl_certificate_name not in self._iam_server_certificate_arns_by_name:
            response = self._get_boto_client('iam').list_server_certificates()

            for item in response.get('ServerCertificateMetadataList'):
                if item['ServerCertificateName'] == ssl_certificate_name:
                    self._iam_server_certificate_arns_by_name[ssl_certificate_name] = item['Arn']
                    return self._iam_server_certificate_arns_by_name[ssl_certificate_name]
            return None

        return self._iam_server_certificate_arns_by_name[ssl_certificate_name]

    def _upsert_route53_record(self, route53_client, domain_name, type_, cname_target_dn):
        dn_one_dot_trail = re.sub(r'\.+$', '', domain_name) + "."
        hosted_zone_id = get_best_parent_hosted_zone_for_dn(route53_client, dn_one_dot_trail)

        change_batch_changes = [{
            'Action': 'UPSERT',
            'ResourceRecordSet': {
                'Name': dn_one_dot_trail,
                'Type': type_,
                'TTL': 60,
                'ResourceRecords': [{'Value': cname_target_dn}],
            }
        }]

        print(f"-  upserting a {type_} record: `{dn_one_dot_trail}` => `{cname_target_dn}`")
        route53_client.change_resource_record_sets(
            HostedZoneId=hosted_zone_id,
            ChangeBatch={
                'Comment': 'Automated update',
                'Changes': change_batch_changes
            }
        )

    def _verify_route53_record(self, route53_client, domain_name, type_, cname_target_dn):
        dn_one_dot_trail = re.sub(r'\.+$', '', domain_name) + "."
        hosted_zone_id = get_best_parent_hosted_zone_for_dn(route53_client, dn_one_dot_trail)

        r = route53_client.list_resource_record_sets(
            HostedZoneId=hosted_zone_id,
            StartRecordName=dn_one_dot_trail,
            StartRecordType=type_,
            MaxItems='1'
        )

        if len(r['ResourceRecordSets']) == 0 \
           or r['ResourceRecordSets'][0]['Name'] != dn_one_dot_trail \
               or r['ResourceRecordSets'][0]['Type'] != type_:
            self.log_fail(f"Parent hosted zone does not the desired {type_} record for the name, {dn_one_dot_trail}.")
            return

        first_value = r['ResourceRecordSets'][0]['ResourceRecords'][0]['Value']

        if first_value == cname_target_dn:
            self.log_pass(f"Hosted zone has the desired {type_} record for the name, {dn_one_dot_trail}.")
        else:
            self.log_fail(f"Hosted zone does not have the desired {type_} record for the name, {dn_one_dot_trail}.")
