#!/usr/bin/env python3
from __future__ import print_function

from . write_mime_multipart import convert_files_to_multipart

import zlib
import io
import gzip
import os
import sys
import boto3
from six import string_types
import re
from pprint import pprint
import time
from termcolor import colored
import datetime

import socket
import paramiko
import scp
import json

from . import launch_monitor_ubuntu
from . import launch_monitor_ubuntu_1804
from . import launch_monitor_ubuntu_2004


class VPCParameterReplacementException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class Incorrect_Instance_States_Exception(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class PrawsLauncher:
    """
    This class is used to create launch EC2 instances
    """

    def __init__(self, instance_config, aws_client_credentials={}):
        print(colored("PAWS Image Launcher - initialising", 'white', attrs=['bold']))
        self.instance_config = instance_config
        self.region_name = self.replacer(instance_config['aws_region'], True)

        awsClientArgs = {
            'region_name': self.region_name
        }
        if 'aws_access_key_id' in aws_client_credentials:
            awsClientArgs['aws_access_key_id'] = aws_client_credentials['aws_access_key_id']

        if 'aws_secret_access_key' in aws_client_credentials:
            awsClientArgs['aws_secret_access_key'] = aws_client_credentials['aws_secret_access_key']

        if 'aws_session_token' in aws_client_credentials:
            awsClientArgs['aws_session_token'] = aws_client_credentials['aws_session_token']

        self.ec2client = boto3.client('ec2', **awsClientArgs)

        self.log_step_start(" - looking up VPC_ID..")
        sys.stdout.flush()
        self.vpc_id      = self.replacer(instance_config['vpc_id'], True)
        self.log_step_end()

        self.vpc_data = None
        self.security_groups = {}
        self.subnets = {}

    STATE__RETRY = 'retrying'
    STATE__FAILED = 'failed'
    STATE__COMPLETED = 'completed'

    # All possible states
    valid_states = [STATE__RETRY, STATE__FAILED, STATE__COMPLETED]

    # States of steps the no furthe progress can be expected.
    final_states = [STATE__FAILED, STATE__COMPLETED]

    def get_vpc_security_groups(self):
        """
        Gets a dictionary of the security groups for the VPC

        :return:
        """
        response = self.ec2client.describe_security_groups(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': [self.vpc_id]
                 }
            ]
        )

        self.security_groups = {}
        for item in response.get('SecurityGroups'):
            group_name = item['GroupName']
            self.security_groups[group_name] = item

        return self.security_groups

    def get_vpc_subnets(self):
        """
        Gets a dictionary of the subnets for the VPC

        :return:
        """
        if self.vpc_id is None:
            return None

        response = self.ec2client.describe_subnets(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': [self.vpc_id]
                 }
            ]
        )

        self.subnets = {}
        for item in response.get('Subnets'):
            if 'Tags' in item:
                key = self.get_tag_value(item['Tags'], 'Name')
            else:
                key = item['SubnetId']

            if key is None:
                key = item['SubnetId']

            self.subnets[key] = item

        return self.subnets

    @staticmethod
    def get_tag_value(tags, key):
        for item in tags:
            if item['Key'] == key:
                return item['Value']

        return None

    def _lazy_load_security_groups(self, for_replacer=False):
        if len(self.security_groups) < 1:
            if for_replacer:
                self.log_replace_lookup("sg's.")
                self.get_vpc_security_groups()
                self.log_replace_lookup(".")
            else:
                self.get_vpc_security_groups()

    def _lazy_load_subnets(self, for_replacer=False):
        if len(self.subnets) > 0:
            return
        if for_replacer:
            self.log_replace_lookup("sn's.")
            self.get_vpc_subnets()
            self.log_replace_lookup(".")
        else:
            self.get_vpc_subnets()


    def _lazy_load(self):
        self._lazy_load_security_groups()
        self._lazy_load_subnets()

    def _get_subnet_ids_for_regexp(self, reg_expression):
        r = []
        for subnet_name, subnet_data in self.subnets.items():
            if re.match(reg_expression, subnet_name):
                r.append(subnet_data['SubnetId'])

        return r

    def _get_internet_gateway_id(self):
        r = self.ec2client.describe_internet_gateways(
            Filters=[{'Name': 'attachment.vpc-id', 'Values': [self.vpc_id]}]
        ).get('InternetGateways')
        if len(r) == 0:
            self.internet_gateway_id = None
            return None
        self.internet_gateway_id = r[0]['InternetGatewayId']
        return self.internet_gateway_id

    def log_step_start(self, msg):
        print("%-90s " % msg, end="")
        self.timer_start_step = datetime.datetime.now()
        sys.stdout.flush()

    def log_step_end(self):
        total_delta = datetime.datetime.now() - self.timer_start_step

        print(colored("[done]", 'green') + " (" + str(total_delta) + ")")


    def log_replace_lookup(self, msg):
        print(msg, end="")
        sys.stdout.flush()

    def replacer(self, item, ignore_not_found=False):
        """
        This function replaces strings in list or dictionaries and populates them with the associated AWS resource IDs.

        e.g.

        {
            foo:[
                 "__SUBNETS_RX__[.*Public.*]"
            ]
        }

        will be converted to
        {
            foo:[
                 "subnet-e0d1b885",
                 "subnet-2f8de34a"
            ]
        }

        where subnets with id, subnet-2f8de34a, and subnet-e0d1b885 match the regexp ".*Public.*"

        :param item:
        :param ignore_not_found: boolean Ignore when a replacement is not found.
        :return:
        """
        if isinstance(item, dict):
            new_item = {}
            for key, value in item.items():
                new_item[key] = self.replacer(item[key], ignore_not_found)
            return new_item

        if isinstance(item, string_types):
            if item == '__DEFAULT_VPC_ID__':
                return self.ec2client.describe_vpcs(Filters=[{'Name':'is-default', 'Values': ["true"]}])["Vpcs"][0]["VpcId"]

            m = re.search("^__VPC_ID__\[(.*)\]$", item)
            if m is not None:
                vpc_name = m.group(1)
                self.log_replace_lookup("vpc.")
                r = self.ec2client.describe_vpcs(Filters=[{'Name':'tag:Name', 'Values': [vpc_name]}])["Vpcs"]
                if len(r) != 1:
                    raise VPCParameterReplacementException("Did not find exactly one VPC with tag:Name of %s" % vpc_name)

                return r[0]["VpcId"]

            if item == '__VPC_ID__':
                return self.vpc_id

            if "__VPC_ID__" in item:
                item = str.replace(item, "__VPC_ID__", self.vpc_id)

            if "__AWS_REGION__" in item:
                item = str.replace(item, "__AWS_REGION__", self.region_name)

            if item == '__INTERNET_GATEWAY_ID__':
                if self.internet_gateway_id is None:
                    self.internet_gateway_id = self._get_internet_gateway_id()

                return self.internet_gateway_id


            if item == '__AMI_ID__[amzn-ami-2015.09.f-amazon-ecs-optimized]':
                aws_owner_ecs_user = '591542846629'
                r = self.ec2client.describe_images(
                    Owners=[aws_owner_ecs_user],
                    Filters=[
                        {'Name': 'name', 'Values': ['amzn-ami-2015.09.f-amazon-ecs-optimized']}
                    ]
                )
                return r.get('Images')[0]['ImageId']

            if item == '__AMI_ID__[amzn-ami-2015.09.e-amazon-ecs-optimized]':
                aws_owner_ecs_user = '591542846629'
                r = self.ec2client.describe_images(
                    Owners=[aws_owner_ecs_user],
                    Filters=[
                        {'Name': 'name', 'Values': ['amzn-ami-2015.09.e-amazon-ecs-optimized']}
                    ]
                )
                return r.get('Images')[0]['ImageId']

            if item == '__AMI_ID__[ecs.2015.09.b]':
                aws_owner_ecs_user = '591542846629'

                r = self.ec2client.describe_images(
                    Owners=[aws_owner_ecs_user],
                    Filters=[
                        {'Name': 'name', 'Values': ['amzn-ami-2015.09.b-amazon-ecs-optimized']}
                    ]
                )
                return r.get('Images')[0]['ImageId']

            if item == '__AMI_ID__[ubuntu-trusty-14.04-amd64-server]':
                aws_owner_amazon_user = '137112412989'
                r = self.ec2client.describe_images(
                    Owners=[],
                    Filters=[
                        {'Name': 'name', 'Values': ['*hvm/ubuntu-trusty-14.04-amd64-server*']}
                    ]
                )
                return r.get('Images')[0]['ImageId']

            m = re.search("^__AMI_ID__\[([^:]*):(.*)\]$", item)
            if m is not None:
                aws_image_owner_id = m.group(1)
                ami_name_filter  = m.group(2)
                owner_name_map = {
                    "CanonicalUser1": "099720109477",
                    "CanonicalUser2": "137112412989"
                }
                self.log_replace_lookup("ami.")
                if aws_image_owner_id in owner_name_map:
                   aws_image_owner_id = owner_name_map[aws_image_owner_id]

                arguments = {
                    'Owners':[aws_image_owner_id],
                    'Filters':[
                        {'Name': 'name', 'Values': [ami_name_filter]}
                    ]
                }
                r = self.ec2client.describe_images(**arguments)
                self.log_replace_lookup(".")
                return r.get('Images')[0]['ImageId']

            if item == '__SYSTEM_TAG_VALUE__':
                return self.system_tag_value

            m = re.search("^__SSL_CERTIFICATE_ID__\[(.*)\]$", item)
            if m is not None:
                ssl_certificate_name = m.group(1)
                return self.get_ssl_certificate_id_for_name(ssl_certificate_name)

            m = re.search("^__SECURITY_GROUP__\[(.*)\]$", item)
            if m is not None:
                security_group_name = m.group(1)
                self._lazy_load_security_groups(for_replacer=True)
                if security_group_name in self.security_groups:
                    return self.security_groups[security_group_name]['GroupId']
                else:
                    if not ignore_not_found:
                        raise VPCParameterReplacementException(
                            "Unable to find security group, `" + security_group_name +
                            "`, when performing parameter replacement"
                        )

            m = re.search("^__SUBNET_ID__\[(.*)\]$", item)
            if m is not None:
                subnet_name = m.group(1)
                self._lazy_load_subnets()
                if subnet_name not in self.subnets:
                    raise VPCParameterReplacementException(
                        "Unable to find subnet with name, `" + subnet_name +
                        "`, when performing parameter replacement"
                    )
                return self.subnets[subnet_name]['SubnetId']

            m = re.search("^__SUBNETS_RX_COMMA_LIST__\[(.*)\]$", item)
            if m is not None:
                reg_expression = m.group(1)
                self._lazy_load()
                subnet_ids = self._get_subnet_ids_for_regexp(reg_expression)
                return ",".join(subnet_ids)

            return item

        if isinstance(item, list):
            new_item = []
            for value in item:
                if isinstance(value, string_types):
                    m = re.search("^__SUBNETS_RX__\[(.*)\]$", value)
                    if m is not None:
                        reg_expression = m.group(1)
                        self._lazy_load()
                        subnet_ids = self._get_subnet_ids_for_regexp(reg_expression)
                        for subnet_id in subnet_ids:
                            new_item.append(subnet_id)
                        continue

                new_item.append(self.replacer(value, ignore_not_found))
            return new_item

        # Otherwise...
        return item


    def launch_instance(self, debug_only=False):
        self.log_step_start(" - looking up replacements in instance_config..")
        instance_config = self.replacer(self.instance_config)
        self.log_step_end()

        script_dir = os.path.dirname(os.path.realpath(__file__)) + "/"

        self.log_step_start(" - Converting cloud-init files to mime multipart")
        cloud_init_files = self.replacer(self.instance_config['cloud_init_files'])

        if 'load_paws_cloud_init_first' in self.instance_config:
            print("WARNING - Spelling error, please use load_praws_cloud_init_first instead")
            self.instance_config['load_praws_cloud_init_first'] = self.instance_config.pop('load_paws_cloud_init_first')

        if self.instance_config.get('load_praws_cloud_init_first', True):
            if 'praws_ami_type' not in self.instance_config:
                print("WARNING: Assuming the ami type is ubuntu-14.04")
                self.instance_config['praws_ami_type'] = 'ubuntu-14.04'

            if self.instance_config['praws_ami_type'] in ['ubuntu-14.04']:
                cloud_init_files.insert(0, script_dir + "cloud-init-base/base-cloud-config.yaml")
                cloud_init_files.insert(1, script_dir + "cloud-init-base/part-handler.py")

            elif self.instance_config['praws_ami_type'] in ['ubuntu-18.04', 'ubuntu-20.04']:
                cloud_init_files.insert(0, script_dir + "cloud-init-base/base-cloud-config.yaml")
            else:
                raise Exception("Unhandled AMI type, `%s`" % self.instance_config['praws_ami_type'])

        mp = convert_files_to_multipart(cloud_init_files)
        self.log_step_end()

        self.log_step_start(" - Compressing mime multipart")
        ofile = io.BytesIO()
        gfile = gzip.GzipFile(fileobj=ofile, filename = "-", mode="w")
        gfile.write(bytes(mp, 'utf-8'))
        gfile.close()
        zmp = ofile.getvalue()
        self.log_step_end()

        self.log_step_start(" - checking values")
        config_key = "InstanceType"
        if config_key not in instance_config:
            instance_config[config_key] = 't2.micro'
            print(" - defaulting %s to '%s'" % (config_key, instance_config[config_key]))

        config_key = "count"
        if config_key not in instance_config:
            instance_config[config_key] = 1
            print(" - defaulting count to %d" % instance_config[config_key])
        self.log_step_end()

        self.log_step_start(" - Launching %d instance(s)" % instance_config['count'])
        arguments = {
            "ImageId": instance_config['ImageId'],
            "MinCount": instance_config['count'],
            "MaxCount": instance_config['count'],
            "KeyName": instance_config['KeyName'],
            "UserData": zmp,
            "InstanceType": instance_config['InstanceType'],
        }
        if 'SubnetId' in instance_config:
            arguments['SubnetId'] = self.replacer(instance_config['SubnetId'], ignore_not_found=False)

        if 'SecurityGroupIds' in instance_config:
            arguments['SecurityGroupIds'] = instance_config['SecurityGroupIds']

        if 'IamInstanceProfile' in instance_config:
            arguments['IamInstanceProfile'] = instance_config['IamInstanceProfile']

        if 'NetworkInterfaces' in instance_config:
            arguments['NetworkInterfaces'] = instance_config['NetworkInterfaces']

        if 'BlockDeviceMappings' in instance_config:
            arguments['BlockDeviceMappings'] = instance_config['BlockDeviceMappings']

        if debug_only:
            print("======== About to run with following arguments =====")
            pprint(arguments)
            return

        self.launched_instances = self.ec2client.run_instances(**arguments)['Instances']
        self.launched_instance_ids = list(map(lambda instance: instance['InstanceId'], self.launched_instances))
        self.log_step_end()

        if "tags" in instance_config:
            arg_tags = list(map(lambda kv: {'Key':kv[0], "Value":kv[1]}, instance_config['tags'].items()))
            self.log_step_start(" - Tagging the instance(s)")
            self.ec2client.create_tags(Resources=self.launched_instance_ids, Tags=arg_tags)
            self.log_step_end()

        self.log_step_start(" - Waiting for instances to start\n")
        self.wait_for_instances_to_start(self.launched_instance_ids)
        self.log_step_end()

        self.log_step_start(" - Getting id to IP map\n")
        id_to_ip_map = self.instance_id_to_elastic_ip_map(self.launched_instance_ids)
        pprint(id_to_ip_map)
        self.log_step_end()

        return self.launched_instance_ids

        # time.sleep(10)
        #        console = self.ec2client.get_console_output(InstanceId=self.launched_instance_ids[0])
        #        print(console)
        #        time.sleep(30)
        #        console = self.ec2client.get_console_output(InstanceId=self.launched_instance_ids[0])
        #        print(console)

    spinner = ["-", "\\", "|", "/"]
    spinner_idx = 0

    def wait_for_instances_to_start(self, instance_ids):
        all_instances_started = False
        spinner = ["-", "\\", "|", "/"]
        spin_idx = 0
        while not all_instances_started:
            response = self.ec2client.describe_instance_status(InstanceIds=instance_ids, IncludeAllInstances=True)
            instance_states = list(map(lambda instance_status: instance_status['InstanceState']['Name'], response['InstanceStatuses']))
            state_counts = {}
            for state in instance_states:
                if state not in state_counts:
                    state_counts[state] = 1
                else:
                    state_counts[state] = state_counts[state] +1

            # From http://boto3.readthedocs.org/en/latest/reference/services/ec2.html#EC2.Client.describe_instance_status
            # 'pending'|'running'|'shutting-down'|'terminated'|'stopping'|'stopped'
            if 'shutting-down' in state_counts or 'stopping' in state_counts or 'stopped' in state_counts or 'terminated' in state_counts:
                raise Incorrect_Instance_States_Exception(
                    "After calling the instance launch command, some of the instances are in states related to terminating / stopping"
                )
            the_text = " - Instance states - " + str.join(", ",map(lambda kv: kv[0] + ": " + str(kv[1]), state_counts.items())) + " " + spinner[spin_idx]
            print("\r%-90s " % the_text, end="")
            spin_idx = (spin_idx + 1) % 4
            if 'running' in state_counts and state_counts['running'] == len(instance_ids):
                all_instances_started = True
                return
            time.sleep(1)

    def instance_id_to_elastic_ip_map(self,  instance_ids):
        instance_info = self.ec2client.describe_instances(InstanceIds=instance_ids)
        # Create a dictonary of instance_id to public addresses map
        instance_addresses = {rsv['Instances'][0]['InstanceId']: rsv['Instances'][0]['PublicIpAddress'] for rsv in instance_info['Reservations']}
        return instance_addresses

    def open_ssh_socket(self, address, port=22):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(0.5)
        sock.connect((address, port))
        return sock

    def open_ssh_transport(self, socket):
        t = paramiko.Transport(socket)
        t.start_client()
        return t

    def auth_ssh_transport_with_agent(self, t, username):
        agent = paramiko.Agent()
        agent_keys = agent.get_keys()
        if len(agent_keys) == 0:
            return False

        for key in agent_keys:
            try:
                t.auth_publickey(username, key)
                return True
            except paramiko.SSHException:
                pass
        return False

    def monitor_instances_ssh(self, instance_type, instance_ids, username='ubuntu'):
        instance_info = self.ec2client.describe_instances(InstanceIds=instance_ids)

        # Create a dictionary of instance_id to public addresses map
        instance_addresses = {rsv['Instances'][0]['InstanceId']: rsv['Instances'][0]['PublicIpAddress'] for rsv in instance_info['Reservations']}

        instance_ssh_sockets = {}
        instance_ssh_transports = {}
        instance_authenticated_transports = {}
        self.instance_step_states = {}
        scp_clients = {}

        instance_ssh_clients = {}

        #        pprint(instance_info['Reservations'][0]['Instances'][0]['PublicIpAddress'])
        #        pprint(instance_info['Reservations'][0]['Instances'][0]['InstanceId'])
        instance_states = {}
        completed_instances = 0
        agent_keys = paramiko.Agent().get_keys()

        if instance_type == 'ubuntu-14.04':
            distro_instance_monitor = launch_monitor_ubuntu.InstancesMonitorUbuntu14_04(self)
        elif instance_type == 'ubuntu-18.04':
            distro_instance_monitor = launch_monitor_ubuntu_1804.InstancesMonitorUbuntu18_04(self)
        elif instance_type == 'ubuntu-20.04':
            distro_instance_monitor = launch_monitor_ubuntu_2004.InstancesMonitorUbuntu20_04(self)
        else:
            raise Exception("Unhandled instance type `%s`" % instance_type)

        instance_launch_steps = ['createSSHSocket', 'createSSHTransport', 'authenticateSSHTransport']
        instance_launch_steps.extend(distro_instance_monitor.instance_steps())

        self.print_steps_with_letters(instance_launch_steps)
        print("\n")

        while completed_instances < len(instance_ids):
            completed_instances = 0
            self.print_steps_progress_by_letters(instance_launch_steps, len(instance_ids))

            for instance_id in instance_ids:
                if instance_id in instance_states and instance_states[instance_id] in self.final_states:
                    completed_instances += 1
                    continue

                if instance_id not in self.instance_step_states:
                    self.instance_step_states[instance_id] = {}

                if instance_id not in instance_ssh_sockets:
                    # print("Creating socket to %s ..." % instance_id)
                    try:
                        instance_ssh_sockets[instance_id] = self.open_ssh_socket(instance_addresses[instance_id])
                    except socket.error as serr:
                        self.instance_step_states[instance_id]['createSSHSocket'] = self.STATE__RETRY
                        continue
                    self.instance_step_states[instance_id]['createSSHSocket'] = self.STATE__COMPLETED

                if instance_id not in instance_ssh_transports:
                    # print("Creating SSH transport to %s ... (%s)" % (instance_id, instance_addresses[instance_id]))
                    instance_ssh_transports[instance_id] = self.open_ssh_transport(instance_addresses[instance_id])
                    self.instance_step_states[instance_id]['createSSHTransport'] = self.STATE__COMPLETED

                if instance_id not in instance_authenticated_transports:
                    # print("Authenticating SSH transport to %s ..." % instance_id)
                    r = self.auth_ssh_transport_with_agent(instance_ssh_transports[instance_id], username)
                    if r:
                        instance_authenticated_transports[instance_id] = True
                        self.instance_step_states[instance_id]['authenticateSSHTransport'] = self.STATE__COMPLETED
                    else:
                        time.sleep(0.2)
                        self.instance_step_states[instance_id]['authenticateSSHTransport'] = self.STATE__RETRY
                        continue

                try:
                    instance_states[instance_id] = distro_instance_monitor.get_instance_state(self, instance_id, instance_ssh_transports[instance_id])

                except FileNotFoundError:
                    pass

        failed_instances = 0
        for instance_id in instance_ids:
            if instance_id in instance_states and instance_states[instance_id] == self.STATE__FAILED:
                failed_instances += 1
                continue

        if failed_instances:
            print("\nLAUNCH FAILED (please check for errors)")
        else:
            print("\nALL FINISHED (please check for errors)")

    def print_steps_with_letters(self, steps):
        print("Steps to launch are as follows:")
        ascii_code = 97 # (a)
        for step in steps:
            print("  (%s) %s" % ( str(chr(ascii_code)), step))
            ascii_code = ascii_code + 1

    def print_steps_progress_by_letters(self, steps, len_instance_ids):
        ascii_code = 97 # (a)
        text = ""
        for step in steps:
            count_states = {}
            for state in self.valid_states:
                count_states[state] = 0

            for instance_id in self.instance_step_states:
                if step not in self.instance_step_states[instance_id]:
                    continue
                state = self.instance_step_states[instance_id][step]
                count_states[state] += 1

            text = text + "(%s) " % str(chr(ascii_code))
            for state in self.valid_states:
                if count_states[state] == 0:
                    continue
                text = text + "%d %s, " % (count_states[state], state)
            ascii_code = ascii_code + 1
        self.spinner_idx = (self.spinner_idx + 1) % len(self.spinner)
        print("\r%s Step status for %d instances: %s" % (self.spinner[self.spinner_idx], len_instance_ids, text), end="")

    def monitor_instances(self, instance_type, instance_ids, instance_states={}):
        completed_instances = 0
        while completed_instances < len(instance_ids):
            for instance_id in instance_ids:
                if instance_id in instance_states and instance_states[instance_id] in self.final_states:
                    continue
                console = self.ec2client.get_console_output(InstanceId=instance_id)
                state = self.get_instance_state_from_console(instance_type, instance_id, console['Output'])
                instance_states[instance_id] = state
                if state in self.final_states:
                    completed_instances = completed_instances + 1

        pprint(instance_states)

    def get_instance_state_from_console(self, instance_type, instance_id, console_text):
        if instance_type in ['ubuntu-14.04', 'ubuntu-18.04', 'ubuntu-20.04']:
            m = re.search(".*Unhandled non-multipart \(text/x-not-multipart\) userdata:.*", console_text)
            if m is not None:
                print("ERROR: The instance, %s, has an unhandled multipart!! ==============" % instance_id)
                print(m.group(0))
                print("ERROR: ====================================================================\n")
                return "error"

            pprint(console_text)
            raise VPCParameterReplacementException("WIP")
