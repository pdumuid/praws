FROM ubuntu:14.04

RUN sed -i 's=archive.ubuntu.com=mirror.internode.on.net/pub/ubuntu=' /etc/apt/sources.list

RUN apt-get update && apt-get install -y awscli jq python3 python3-openssl python3-pip openssh-client mysql-client pandoc screen groff && \
    pip3 install pypandoc && \
    pip3 install termcolor pwgen sshtunnel pymysql awscli boto3 six && \
    apt-get install -y openssh-server && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install scp

COPY ./ /praws-scripts/praws/
WORKDIR /praws-scripts/

CMD [ /bin/bash ]
