#!/usr/bin/env python3
from __future__ import print_function
from six import string_types

# This python class will be moved into baws at some-stage for re-use.
import json
from pprint import pprint

import boto3
import re
import os
import time
import Crypto.PublicKey.RSA

import botocore
from termcolor import colored
import copy
import sys
import deepdiff
import base64
import hashlib
from praws.builder import Builder
from praws.route53_utils import get_hosted_zones_for_domain_name


from praws.exceptions import VPCNotImplementedYet
from praws.exceptions import IncorrectStateException
from praws.exceptions import RDSNotFoundException
from praws.exceptions import VPCParameterReplacementException

from praws.utils import as_nice_json


def _security_group_ingress_cleaner_for_diff(data_in):
    # This function cleans up the ingress values to make diff work better.
    data = copy.copy(data_in)
    keys_to_make_empty_list = ['Ipv6Ranges', 'PrefixListIds', 'UserIdGroupPairs']
    for idx, record in enumerate(data):
        data[idx]['IpRanges']  = sorted(data[idx]['IpRanges'], key=lambda x: x['CidrIp'])

        for key_to_make_empty_list in keys_to_make_empty_list:
            if key_to_make_empty_list not in data[idx]:
                data[idx][key_to_make_empty_list] = []

    return sorted(data, key=_security_group_ingress_sort_keygen)


def _security_group_ingress_sort_keygen(data):
    a_key = data['IpProtocol']
    if 'FromPort' in data and 'ToPort' in data:
        a_key += 'PortRange-%d-to-%d' % (data['FromPort'], data['ToPort'])

    return a_key


def _desired_security_groups_ingress_present(desired_ingress, current_ingresses):
    """
    This is a quick hacky way to see if the desired_ingress is in the current_ingresses.
    :param desired_ingress:
    :param current_ingresses:
    :return:
    """
    desired_ingress_copied = copy.copy(desired_ingress)
    if 'PrefixListIds' not in desired_ingress_copied:
        desired_ingress_copied['PrefixListIds'] = []

    if 'Ipv6Ranges' not in desired_ingress_copied:
        desired_ingress_copied['Ipv6Ranges'] = []

    v1 = as_nice_json(desired_ingress_copied)

    # print("\n======DESIRED =============\n");    pprint(v1)
    for current_ingress in current_ingresses:
        current_ingress_copied = copy.copy(current_ingress)
        for idx in range(0, len(current_ingress_copied['UserIdGroupPairs'])):
            current_ingress_copied['UserIdGroupPairs'][idx].pop('UserId', None)
        v2 = as_nice_json(current_ingress_copied)
        if v2 == v1:
            return True
        # print("\n===================\n");        pprint(v2);        print("\n===================\n")
    return False


class VPCBuilder(Builder):
    """
    :type ec2client:

    """

    def __init__(self, region_name, vpc_name, system_tag_value, use_unicode=False, boto3_sts_role_arns=None):
        """
        Initialises a VPC Builder instance

        :param region_name:
        :return:
        """
        super(VPCBuilder, self).__init__(region_name, system_tag_value, use_unicode=use_unicode, boto3_sts_role_arns=boto3_sts_role_arns)

        self.vpc_name = vpc_name

        self.vpc_data = None
        self.vpc_id = None

        self._current_instance_id = None
        self._current_route_table_id = None

        self._current_subnet_id = None
        self._default_security_group = None

        self.security_groups = {}
        self.subnets = {}
        self.vpc_endpoint_ids = {}

        self._az_letter = None
        if vpc_name is not None:
            self.get_vpc_data()

        self.internet_gateway_id = None

    def _deep_diff_ok_or_file(
            self, desired_value, current_value, ignore_order, diff_filename_prefix,
            match_message, mismatch_message
    ):
            dd = deepdiff.DeepDiff(desired_value, current_value, ignore_order=ignore_order)
            if (len(dd.keys()) == 0):
                print(match_message)
            else:
                print(mismatch_message)
                f1, f2 = self._create_comparison_json_files(
                    diff_filename_prefix,
                    desired_value,
                    current_value
                )
                print("  - For details, compare:\n     %s\n  vs %s" % (f1, f2))

    def _get_boto_resource(self, resource_name):
        if resource_name not in self._boto_resources:
            self._boto_resources[resource_name] = boto3.resource(resource_name, region_name=self.region_name)
        return self._boto_resources[resource_name]

    def _get_hosted_zone_for_dn(self, dn):
        if self.hosted_zones is None:
            self.hosted_zones = self._get_boto_client('route53').list_hosted_zones()['HostedZones']
        # Add a dot
        dn_one_dot_trail = re.sub('\.+$', '', dn) + "."

        match_len = 0
        match_hosted_zone = None
        for hosted_zone in self.hosted_zones:
            name = hosted_zone['Name']
            name_len = len(name)
            if dn_one_dot_trail.endswith(name) and name_len > match_len:
                match_len = name_len
                match_hosted_zone = re.sub('^/hostedzone/', '', hosted_zone['Id'])

        return match_hosted_zone

    def _route53_alias_dn_to_elb(self, dn, load_balancer_name="production-service-cms"):
        dn_one_dot_trail = re.sub('\.+$', '', dn) + "."
        # Get the hosted zone
        hosted_zone_id = self._get_hosted_zone_for_dn(dn)

        load_balancer_info = self._get_boto_client('elb').describe_load_balancers(
            LoadBalancerNames=[load_balancer_name]
        )['LoadBalancerDescriptions'][0]

        if load_balancer_info['Scheme'] == 'internal':
            dns_name = load_balancer_info['DNSName']
            hz_id = load_balancer_info['CanonicalHostedZoneNameID']
        else:
            dns_name = load_balancer_info['CanonicalHostedZoneName']
            hz_id = load_balancer_info['CanonicalHostedZoneNameID']

        change_batch_changes = [{
            'Action': 'UPSERT',
            'ResourceRecordSet': {
                'Name': dn_one_dot_trail,
                'Type': 'A',
                'AliasTarget': {
                    'DNSName': dns_name,
                    'HostedZoneId': hz_id,
                    'EvaluateTargetHealth': False
                }
            }
        }]
        self._get_boto_client('route53').change_resource_record_sets(
            HostedZoneId=hosted_zone_id,
            ChangeBatch={
                'Comment': 'Automated update',
                'Changes': change_batch_changes
            }
        )

    def _route53_alias_dn_to_null(self, dn):
        dn_one_dot_trail = re.sub('\.+$', '', dn) + "."
        # Get the hosted zone
        hosted_zone_id = self._get_hosted_zone_for_dn(dn)

        change_batch_changes = [{
            'Action': 'UPSERT',
            'ResourceRecordSet': {
                'Name': dn_one_dot_trail,
                'Type': 'A',
                'TTL': 60,
                'ResourceRecords': [{'Value': '0.0.0.0'}],
            }
        }]
        self._get_boto_client('route53').change_resource_record_sets(
            HostedZoneId=hosted_zone_id,
            ChangeBatch={
                'Comment': 'Automated update',
                'Changes': change_batch_changes
            }
        )

    def get_vpc_data(self):
        r = self._get_boto_client('ec2').describe_vpcs(Filters=[{'Name': 'tag:Name', 'Values': [self.vpc_name]}])
        if len(r.get("Vpcs")) > 0:
            self.vpc_data = r.get("Vpcs")[0]
            self.vpc_id = self.vpc_data['VpcId']

    def get_vpc_security_groups(self):
        """
        Gets a dictionary of the security groups for the VPC

        :return:
        """
        if self.vpc_id is None:
            return None

        response = self._get_boto_client('ec2').describe_security_groups(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': [self.vpc_id]
                 }
            ]
        )

        self.security_groups = {}
        for item in response.get('SecurityGroups'):
            group_name = item['GroupName']
            self.security_groups[group_name] = item

        return self.security_groups

    def get_vpc_subnets(self):
        """
        Gets a dictionary of the subnets for the VPC

        :return:
        """
        if self.vpc_id is None:
            return None

        response = self._get_boto_client('ec2').describe_subnets(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': [self.vpc_id]
                 }
            ]
        )

        self.subnets = Builder.subnet_response_to_keyed_list(response.get('Subnets'))

    def _lazy_load_security_groups(self, for_replacer=False):
        if len(self.security_groups) < 1:
            if for_replacer:
                self.log_replace_lookup("sg's.")
                self.get_vpc_security_groups()
                self.log_replace_lookup(".")
            else:
                self.get_vpc_security_groups()

    def _lazy_load_subnets(self, for_replacer=False):
        if len(self.subnets) > 0:
            return
        if for_replacer:
            self.log_replace_lookup("sn's.")
            self.get_vpc_subnets()
            self.log_replace_lookup(".")
        else:
            self.get_vpc_subnets()

    def _lazy_load(self):
        self._lazy_load_security_groups()
        self._lazy_load_subnets()

    def _get_subnet_ids_for_regexp(self, reg_expression):
        r = []
        for subnet_name, subnet_data in self.subnets.items():
            if re.match(reg_expression, subnet_name):
                r.append(subnet_data['SubnetId'])

        return r

    def _get_internet_gateway_id(self):
        r = self._get_boto_client('ec2').describe_internet_gateways(
            Filters=[{'Name': 'attachment.vpc-id', 'Values': [self.vpc_id]}]
        ).get('InternetGateways')
        if len(r) == 0:
            self.internet_gateway_id = None
            return None
        self.internet_gateway_id = r[0]['InternetGatewayId']
        return self.internet_gateway_id

    def log_replace_lookup(self, msg):
        print(msg, end="")
        sys.stdout.flush()

    def string_replacer(self, item, ignore_not_found=False):
        if item == '__DEFAULT_VPC_ID__':
            return (
                self._get_boto_client('ec2').describe_vpcs(
                    Filters=[{'Name': 'is-default', 'Values': ["true"]}]
                )["Vpcs"][0]["VpcId"],
                False
            )

        m = re.search("^__VPC_ID__\[(.*)\]$", item)
        if m is not None:
            vpc_name = m.group(1)
            self.log_replace_lookup("vpc.")
            r = self._get_boto_client('ec2').describe_vpcs(Filters=[{'Name': 'tag:Name', 'Values': [vpc_name]}])["Vpcs"]
            if len(r) != 1:
                raise VPCParameterReplacementException(
                    "Did not find exactly one VPC with tag:Name of %s" % vpc_name
                )

            return r[0]["VpcId"], False

        if item == '__VPC_ID__':
            return self.vpc_id, False

        item, keep_replacing = super(VPCBuilder, self).string_replacer(item, ignore_not_found=ignore_not_found)
        if not keep_replacing:
            return item, keep_replacing

        if "__VPC_ID__" in item:
            item = str.replace(item, "__VPC_ID__", self.vpc_id)

        if "__AZ_LETTER__" in item:
            if self._az_letter is None:
                if not ignore_not_found:
                    raise VPCParameterReplacementException("AZ letter not set yet!")
                return item, False
            item = str.replace(item, "__AZ_LETTER__", self._az_letter)

        m = re.search("__VPC_ENDPOINT_ID__\[(.*)\]", item)
        if m is not None:
            service_name = m.group(1)
            if service_name not in self.vpc_endpoint_ids:
                r = self._get_boto_client('ec2').describe_vpc_endpoints(Filters=[
                    { 'Name': 'vpc-id', 'Values': [self.vpc_id] },
                    {'Name': 'service-name', 'Values': [service_name]}
                ])
                if len(r['VpcEndpoints']) < 1:
                    if ignore_not_found:
                        # Don't bother keep replacing when we haven't found anything.
                        return item, False
                    raise VPCParameterReplacementException(
                        "Replacement, `__VPC_ENDPOINT_ID__[%s]` not found" % service_name
                    )
                if len(r['VpcEndpoints']) > 1:
                    raise VPCParameterReplacementException(
                        "Replacement, `__VPC_ENDPOINT_ID__[%s]` found too many (more than 1)" % service_name
                    )
                self.vpc_endpoint_ids[service_name] = r['VpcEndpoints'][0]['VpcEndpointId']

            return self.vpc_endpoint_ids[service_name], False

        m = re.search("^__ROUTE_TABLE_ID__\[pr:(.*)\]$", item)
        if m is not None:
            praws_reference = m.group(1)
            lookup_filters = [
                {'Name':'tag:PRAWS-VPCBuilder-Reference', 'Values': [praws_reference]},
                {'Name':'vpc-id', 'Values':[self.vpc_id]}
            ]
            r = self._get_boto_client('ec2').describe_route_tables(Filters=lookup_filters)
            if len(r['RouteTables']) != 1:
                raise VPCParameterReplacementException(
                    "Replacement, `%s` not found" % item
                )
            return r['RouteTables'][0]['RouteTableId'], False

        if item == '__CURRENT_ROUTE_TABLE_ID__':
            if self._current_route_table_id is None:
                if ignore_not_found:
                    return item, False
                raise VPCParameterReplacementException(
                    "Replacement, `__CURRENT_ROUTE_TABLE_ID__` not possible when outside of a sub-step"
                )

            return self._current_route_table_id, False

        if item == '__CURRENT_SUBNET_ID__':
            if self._current_subnet_id is None:
                if ignore_not_found:
                    return item, False
                raise VPCParameterReplacementException(
                    "Replacement, `__CURRENT_SUBNET_ID__` not possible when outside of a sub-step"
                )

            return self._current_subnet_id, False

        if item == '__INTERNET_GATEWAY_ID__':
            if self.internet_gateway_id is None:
                self.internet_gateway_id = self._get_internet_gateway_id()

            return self.internet_gateway_id, False

        if item == '__CURRENT_INSTANCE_ID__':
            if self._current_instance_id is None:
                if ignore_not_found:
                    return item, False
                raise VPCParameterReplacementException(
                    "Replacement, `__CURRENT_INSTANCE_ID__` not possible when outside of a sub-step"
                )

            return self._current_instance_id, False

        if item == '__AMI_ID__[amzn-ami-2015.09.f-amazon-ecs-optimized]':
            aws_owner_ecs_user = '591542846629'
            r = self._get_boto_client('ec2').describe_images(
                Owners=[aws_owner_ecs_user],
                Filters=[
                    {'Name': 'name', 'Values': ['amzn-ami-2015.09.f-amazon-ecs-optimized']}
                ]
            )
            return r.get('Images')[0]['ImageId'], False

        if item == '__AMI_ID__[amzn-ami-2015.09.e-amazon-ecs-optimized]':
            aws_owner_ecs_user = '591542846629'
            r = self._get_boto_client('ec2').describe_images(
                Owners=[aws_owner_ecs_user],
                Filters=[
                    {'Name': 'name', 'Values': ['amzn-ami-2015.09.e-amazon-ecs-optimized']}
                ]
            )
            return r.get('Images')[0]['ImageId'], False

        if item == '__AMI_ID__[ecs.2015.09.b]':
            aws_owner_ecs_user = '591542846629'

            r = self._get_boto_client('ec2').describe_images(
                Owners=[aws_owner_ecs_user],
                Filters=[
                    {'Name': 'name', 'Values': ['amzn-ami-2015.09.b-amazon-ecs-optimized']}
                ]
            )
            return r.get('Images')[0]['ImageId'], False

        if item == '__AMI_ID__[ubuntu-trusty-14.04-amd64-server]':
            aws_owner_amazon_user = '137112412989'
            r = self._get_boto_client('ec2').describe_images(
                Owners=[],
                Filters=[
                    {'Name': 'name', 'Values': ['*hvm/ubuntu-trusty-14.04-amd64-server*']}
                ]
            )
            return r.get('Images')[0]['ImageId'], False

        m = re.search("^__AMI_ID__\[([^:]*):(.*)\]$", item)
        if m is not None:
            aws_image_owner_id = m.group(1)
            ami_name_filter = m.group(2)
            owner_name_map = {
                "CanonicalUser1": "099720109477",
                "CanonicalUser2": "137112412989",
                "AmazonUser1": "591542846629"
            }
            self.log_replace_lookup("ami.")
            if aws_image_owner_id in owner_name_map:
                 aws_image_owner_id = owner_name_map[aws_image_owner_id]

            arguments = {
                'Owners': [aws_image_owner_id],
                'Filters': [
                    {'Name': 'name', 'Values': [ami_name_filter]}
                ]
            }
            r = self._get_boto_client('ec2').describe_images(**arguments)
            self.log_replace_lookup(".")
            return r.get('Images')[0]['ImageId'], False

        m = re.search("^__SECURITY_GROUP__\[(.*)\]$", item)
        if m is not None:
            security_group_name = m.group(1)
            self._lazy_load_security_groups(for_replacer=True)
            if security_group_name in self.security_groups:
                return self.security_groups[security_group_name]['GroupId'], False
            else:
                if not ignore_not_found:
                    raise VPCParameterReplacementException(
                        "Unable to find security group, `" + security_group_name +
                        "`, when performing parameter replacement"
                    )

        # pr: denotes that this is a praws reference rather than an aws tag
        # ie to ignore the Name tag in the AWS console and use the PRAWS-VPCBuilder-Reference tag
        m = re.search("^__SUBNET_ID__\[pr:(.*)\]$", item)
        if m is not None:
            praws_reference = m.group(1)
            lookup_filters = [
                {'Name':'tag:PRAWS-VPCBuilder-Reference', 'Values': [praws_reference]},
                {'Name':'vpc-id', 'Values':[self.vpc_id]}
            ]
            r = self._get_boto_client('ec2').describe_subnets(Filters=lookup_filters)
            if len(r['Subnets']) != 1:
                raise VPCParameterReplacementException(
                    "Replacement, `%s` did not find exactly one." % item
                )
            return r['Subnets'][0]['SubnetId'], False

        m = re.search("^__SUBNET_ID__\[(.*)\]$", item)
        if m is not None:
            subnet_name = m.group(1)
            self._lazy_load_subnets()
            if subnet_name not in self.subnets:
                raise VPCParameterReplacementException(
                    "Unable to find subnet with name, `" + subnet_name +
                    "`, when performing parameter replacement"
                )
            return self.subnets[subnet_name]['SubnetId'], False

        m = re.search("^__SUBNETS_RX_COMMA_LIST__\[(.*)\]$", item)
        if m is not None:
            reg_expression = m.group(1)
            self._lazy_load()
            subnet_ids = self._get_subnet_ids_for_regexp(reg_expression)
            return ",".join(subnet_ids), False

        # Otherwise...
        return item, True

    def list_replacer(self, item, ignore_not_found=False):
        new_item = []
        for value in item:
            if isinstance(value, string_types):
                m = re.search("^__SUBNETS_RX__\[(.*)\]$", value)
                if m is not None:
                    reg_expression = m.group(1)
                    self._lazy_load()
                    subnet_ids = self._get_subnet_ids_for_regexp(reg_expression)
                    for subnet_id in subnet_ids:
                        new_item.append(subnet_id)
                    continue

            new_item.append(self.replacer(value, ignore_not_found))
        return new_item

    def _autoscale_get_next_name(self, lc_name_prefix):
        print("  * finding other launch-configs with prefix, `" + lc_name_prefix + "`")
        r = self._get_boto_client('autoscaling').describe_launch_configurations()
        lcs_name_as_key = {}

        for lc_item in r.get('LaunchConfigurations'):
            name = lc_item['LaunchConfigurationName']
            lcs_name_as_key[name] = lc_item

        number = 1
        next_name = lc_name_prefix + "%02d" % number

        while next_name in lcs_name_as_key:
            number += 1
            next_name = lc_name_prefix + "%02d" % number

        return next_name

    def _play_step(self, step_config):
        """
        The official function for playing a step.

        :param step_config:
        :return:
        """
        if step_config['client'] in ["ecs", "iam", "organizations", "route53", "s3", "sdb", "ses", "sns", "sqs"]:
            return super(VPCBuilder, self)._play_step(step_config)

        if step_config['client'] == 'ec2':
            return self._play_step_ec2(step_config)
        elif step_config['client'] == 'elb':
            return self._play_step_elb(step_config)
        elif step_config['client'] == 'autoscaling':
            return self._play_step_autoscaling(step_config)
        elif step_config['client'] == 'rds':
            return self._play_step_rds(step_config)
        elif step_config['client'] == 'elasticache':
            return self._play_step_elasticache(step_config)
        elif step_config['client'] == 'swf':
            return self._play_step_swf(step_config)
        elif step_config['client'] == 'python-exec':
            return self._play_step_python_exec(step_config)
        else:
            raise ValueError("Unknown client, `" + step_config['client'] + "`")

    def _play_step_python_exec(self, step_config):
        """
        Handle playing of EC2 steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)
        arguments['vpc_builder'] = self
        step_config['function'](**arguments)

    def _play_step_elb(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('elb')
        function_name = step_config['function']
        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        client_method(**arguments)

        # PLAY-POST::elb::create_load_balancer
        # - handle the attribute, 'LoadBalancerAttributes'
        if function_name == 'create_load_balancer' and step_config['LoadBalancerAttributes'] is not None:
            load_balancer_attributes = self.replacer(step_config['LoadBalancerAttributes'])
            arguments_for_modify_attributes = {
                'LoadBalancerName': arguments['LoadBalancerName'],
                'LoadBalancerAttributes': load_balancer_attributes
            }
            self._get_boto_client('elb').modify_load_balancer_attributes(**arguments_for_modify_attributes)

        # PLAY-POST::elb::create_load_balancer
        # - handle the attribute, 'dns_to_alias'
        if function_name == 'create_load_balancer' and step_config['dns_to_alias'] is not None:
            dns_to_alias = self.replacer(step_config['dns_to_alias'])
            for dn in dns_to_alias:
                print("Updating route53 to point `%s` to the ELB" % dn)
                self._route53_alias_dn_to_elb(dn, arguments['LoadBalancerName'])

    def _get_hosted_zones_for_domain_name(self, dn):
        client = self._get_boto_client("route53")

        hosted_zone_ids = []

        hosted_zones_data = client.list_hosted_zones()['HostedZones']
        for hosted_zone_data in hosted_zones_data:
            if hosted_zone_data['Name']  == dn:
                hosted_zone_id = re.sub('^/hostedzone/', '', hosted_zone_data['Id'])
                hosted_zone_ids.append(hosted_zone_id)
        return hosted_zone_ids

    def _play_step_ec2(self, step_config):
        """
        Handle playing of EC2 steps.

        :param step_config:
        :return:
        """

        client = self._get_boto_client('ec2')
        function_name = step_config['function']

        # PLAY::ec2::custom_instances_can_exist
        # - do nothing (the step is only useful in revert)
        if function_name == "custom_instances_can_exist":
            print(" - nothing to do, (instance can always exist) - this is a 'revert-only' step.")
            return

        # PLAY::ec2::per_az:create_route_table
        if function_name == 'per_az:create_route_table':
            for az_letter in step_config['az_letters_to_create_in']:
                self._az_letter = az_letter
                step_config2 = copy.copy(step_config)
                step_config2['function'] = 'create_route_table'
                self._play_step_ec2(step_config2)
            self._az_letter = None
            return

        # PLAY::ec2::per_az:create_subnet
        if function_name == 'per_az:create_subnet':
            for az_letter in step_config['az_letters_to_create_in']:
                self._az_letter = az_letter
                step_config2 = copy.copy(step_config)
                step_config2['function'] = 'create_subnet'
                step_config2['arguments']['CidrBlock'] = step_config2['CidrBlockPerAZ'][az_letter]
                self._play_step_ec2(step_config2)
            self._az_letter = None
            return

        # PLAY::ec2::per_az:custom:create_or_replace_route
        if function_name == 'per_az:custom:create_or_replace_route':
            for az_letter in step_config['az_letters_to_create_in']:
                self._az_letter = az_letter
                step_config2 = copy.copy(step_config)
                step_config2['function'] = 'custom:create_or_replace_route'
                self._play_step_ec2(step_config2)
            self._az_letter = None
            return

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        # PLAY::ec2::custom:create_or_replace_route
        if function_name == 'custom:create_or_replace_route':
            r = client.describe_route_tables(RouteTableIds=[arguments['RouteTableId']])
            if len(r['RouteTables']) != 1:
                raise Exception("Expected to find at least on route table!")
            route_table_info = r['RouteTables'][0]
            function_name = 'create_route'
            for route_info in route_table_info['Routes']:
                if 'DestinationCidrBlock' in route_info and route_info['DestinationCidrBlock'] == arguments['DestinationCidrBlock']:
                    function_name = 'replace_route'

        # PLAY::ec2::custom:add_vpc_endpoint_to_route_tables
        if function_name == "custom:add_vpc_endpoint_to_route_tables":
            client.modify_vpc_endpoint(
                VpcEndpointId=arguments['VpcEndpointId'],
                AddRouteTableIds=arguments['RouteTableIds']
            )
            return

        # PLAY::ec2::custom:wait_for_state
        if function_name == "custom:wait_for_state":
            instance_ids = arguments['InstanceIds']

            start_time = time.time()
            this_time = start_time

            all_pass = False
            max_wait_time = arguments['MaxWaitTime']
            while not all_pass and ((this_time - start_time) < max_wait_time):
                r = client.describe_instance_status(InstanceIds=instance_ids)
                if len(r['InstanceStatuses']) > 0:
                    all_pass = True
                for instance_status_data in r['InstanceStatuses']:
                    instance_state = instance_status_data['InstanceState']['Name']
                    if instance_state not in arguments['DesiredStates']:
                        all_pass = False
                    if instance_state in arguments['ExceptionalStates']:
                        raise Exception("An instance is in an exceptional state!")

                this_time = time.time()
                sys.stdout.write(".")
                sys.stdout.flush()
                time.sleep(2)
            if not all_pass:
                raise Exception("Never managed to reach the desired states in the desired time.")
            return

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        # PRE-PLAY::ec2::create_vpc
        # - avoid creating once if one already exists with the same name
        if function_name == 'create_vpc':
            self.get_vpc_data()
            if self.vpc_id is not None:
                raise Exception("Attempt to create VPC when it already exists")

        # PRE-PLAY::ec2::import_key_pair
        # - read public key content from a local file.
        if function_name == 'import_key_pair' and 'getPublicKeyMaterialFromFile' in step_config:
            with open(self.replacer(step_config['getPublicKeyMaterialFromFile']), "r") as f:
                arguments['PublicKeyMaterial'] = f.read()

        # PRE-PLAY::ec2::create_vpc_endpoint
        # - convert PolicyDocument to a string.
        if function_name == "create_vpc_endpoint" and 'PolicyDocument' in arguments:
            arguments['PolicyDocument'] = as_nice_json(arguments['PolicyDocument'])

        try:
            r = client_method(**arguments)
        except Exception as e:
            print("Error occurred calling EC2 method, `%s` with arguments:" % function_name)
            pprint(arguments)
            raise e

        # POST-PLAY::ec2::run_instances
        # - tag with prawsResourceReference
        if function_name == 'run_instances':
            if 'prawsResourceReference' in step_config:
                instance_ids = list(map((lambda x: x['InstanceId']), r['Instances']))
                praws_resource_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
                print(" - Adding Praws Reference Tag (%s)" % praws_resource_reference)
                client.create_tags(
                    Resources=instance_ids,
                    Tags=[{'Key': 'PRAWS-VPCBuilder-Reference', 'Value': praws_resource_reference}]
                )
            else:
                print(
                    "Warning: The steps does not contain a prawsResourceReference."
                    "  You will not be able to reference the instances later in verification and deletion!"
                )

            for instance_data in r['Instances']:
                self._current_instance_id = instance_data['InstanceId']
                for sub_step in step_config['subSteps']:
                    self.play_sub_step(sub_step)

            self._current_instance_id = None
            return

        # POST-PLAY::ec2::create_internet_gateway
        # - play sub-steps
        if function_name == 'create_internet_gateway':
            self.internet_gateway_id = r.get('InternetGateway')['InternetGatewayId']
            for sub_step in step_config['subSteps']:
                self.play_sub_step(sub_step)
            return

        # POST-PLAY::ec2::create_subnet
        # - tag with prawsResourceReference
        # - play sub-steps
        # - Handle 'associateWithRouteTable'
        if function_name == 'create_subnet':
            subnet_id = r['Subnet']['SubnetId']
            if 'prawsResourceReference' in step_config:
                praws_resource_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
                print(" - Adding Praws Reference Tag (%s)" % praws_resource_reference)
                client.create_tags(
                    Resources=[subnet_id],
                    Tags=[{'Key': 'PRAWS-VPCBuilder-Reference', 'Value': praws_resource_reference}]
                )
            else:
                print(
                    "Warning: A route table created without prawsResourceReference."
                    "  You will not be able to reference it later!"
                )

            if 'associateWithRouteTable' in step_config:
                route_table_info = self.replacer(copy.copy(step_config['associateWithRouteTable']))
                print(" - associating with route table (%s)" % self.replacer(route_table_info['DisplayName']))
                client.associate_route_table(SubnetId=subnet_id, RouteTableId=route_table_info['Id'])

            if 'subSteps' not in step_config:
                return

            self._current_subnet_id = subnet_id
            for sub_step in step_config['subSteps']:
                self.play_sub_step(sub_step)
            self._current_subnet_id = None
            return

        # POST-PLAY::ec2::create_route_table
        # - handle 'prawsResourceReference' attribute
        # - play substeps
        if function_name == 'create_route_table':
            route_table_id = r['RouteTable']['RouteTableId']

            if 'prawsResourceReference' in step_config:
                praws_resource_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
                print(" - Adding Praws Reference Tag (%s)" % praws_resource_reference)
                client.create_tags(
                    Resources=[route_table_id],
                    Tags=[{'Key': 'PRAWS-VPCBuilder-Reference', 'Value': praws_resource_reference}]
                )
            else:
                print(
                    "Warning: A route table created without prawsResourceReference."
                    "  You will not be able to reference it later!"
                )

            if 'subSteps' not in step_config:
                return

            self._current_route_table_id = route_table_id
            for sub_step in step_config['subSteps']:
                self.play_sub_step(sub_step)
            self._current_route_table_id = None
            return

        # POST-PLAY::ec2::create_security_group
        # - play substeps
        if function_name == 'create_security_group':
            security_group_id = r['GroupId']
            if 'ingress_ip_permissions' in step_config:
                authorize_ingress_achievable = True
                for ip_permissions in step_config['ingress_ip_permissions']:
                    print(" - Adding ingress_ip_permissions")
                    missing_groups = []
                    if ('removeIfSecurityGroupMissing' in ip_permissions):
                        for required_group_name in ip_permissions['removeIfSecurityGroupMissing']:
                            if required_group_name not in self.security_groups:
                                print("The security group '%s' does not exist, cannot apply ingress" % (required_group_name))
                                missing_groups.append(required_group_name)
                                authorize_ingress_achievable = False

                if authorize_ingress_achievable:
                        client.authorize_security_group_ingress(
                            GroupId=security_group_id,
                            IpPermissions=self.replacer(copy.copy(step_config['ingress_ip_permissions']))
                        )

            # Reload security groups
            self.get_vpc_security_groups()
            if 'subSteps' in step_config:
                for sub_step in step_config['subSteps']:
                    self.play_sub_step(sub_step)
            return

        # POST-PLAY::ec2::create_vpc
        # - ensure that the VPC is always named.
        if function_name == 'create_vpc':
            self.vpc_data = r.get("Vpc")
            self.vpc_id = self.vpc_data['VpcId']
            self.play_sub_step({
                'description': 'Create Tags',
                'client': 'ec2',
                'function': 'create_tags',
                'arguments': {
                    'Resources': [
                        self.vpc_id
                    ],
                    'Tags': [
                        {'Key': 'Name', 'Value': self.vpc_name},
                        {'Key': 'System', 'Value': self.system_tag_value}
                    ]
                }
            })

    def update_images_in_container_definitions(self, container_definitions, docker_image_version_map):
        for i, val in enumerate(container_definitions):
            docker_image = container_definitions[i]['image'].split(":")[0]
            if docker_image in docker_image_version_map:
                container_definitions[i]['image'] = docker_image + ":" + docker_image_version_map[docker_image]
            print(" - info: uses image '%s' (after all replacements) " % container_definitions[i]['image'])
        return container_definitions

    def _play_step_swf(self, step_config):
        """
        Handle playing of SWF steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('swf')
        function_name = step_config['function']

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        client_method(**arguments)

    def _play_step_elasticache(self, step_config):
        """
        Handle playing of Elasticache steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('elasticache')
        function_name = step_config['function']

        if function_name == 'custom_wait_for_cache_cluster_available':
            cluster_ids = [arguments['CacheClusterId']]
            return self._wait_for_state_elasticache_cache_clusters(cluster_ids, ['available'])

        if function_name == 'custom_wait_for_replication_group_available':
            rep_g_info = self._wait_for_state_elasticache_replication_group(
                arguments['ReplicationGroupId'], ['available']
            )

            if 'dns_to_cname' not in arguments:
                return True

            primary_endpoint_address = rep_g_info['NodeGroups'][0]['PrimaryEndpoint']['Address']

            for dn in arguments['dns_to_cname']:
                dn_one_dot_trail = re.sub('\.+$', '', dn) + "."
                print(" - Setting DN, `%s` to CNAME to `%s`" % (dn_one_dot_trail, primary_endpoint_address))
                # Get the hosted zone
                hosted_zone_id = self._get_hosted_zone_for_dn(dn_one_dot_trail)

                change_batch_changes = [{
                    'Action': 'UPSERT',
                    'ResourceRecordSet': {
                        'Name': dn_one_dot_trail,
                        'Type': 'CNAME',
                        'TTL': 60,
                        'ResourceRecords': [{'Value': primary_endpoint_address}],
                    }
                }]
                self._get_boto_client('route53').change_resource_record_sets(
                    HostedZoneId=hosted_zone_id,
                    ChangeBatch={
                        'Comment': 'Automated update',
                        'Changes': change_batch_changes
                    }
                )
            return True

        if function_name == 'per_az:create_cache_cluster':
            for az_letter in step_config['az_letters_to_create_in']:
                fqn_az = self.region_name + az_letter
                arguments['PreferredAvailabilityZone'] = fqn_az
                arguments['CacheClusterId'] = arguments['CacheClusterId'].replace("__AZ_LETTER__", az_letter).replace("__AZ__", fqn_az)
                client.create_cache_cluster(**arguments)
            return

        if function_name == 'per_az:custom_wait_for_cache_cluster_available':
            cluster_ids = []
            for az_letter in step_config['az_letters_to_create_in']:
                fqn_az = self.region_name + az_letter
                cache_cluster_id = arguments['CacheClusterId'].replace("__AZ_LETTER__", az_letter).replace("__AZ__", fqn_az)
                cluster_ids.append(cache_cluster_id)
            return self._wait_for_state_elasticache_cache_clusters(cluster_ids, ['available'])

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        client_method(**arguments)

    def _wait_for_state_elasticache_replication_group(self, replication_group_id, desiredStatuses, desire_no_replication_group=False, exceptionalStates=[]):
        client = self._get_boto_client('elasticache')

        last_status = ""
        while True:
            is_found = True
            try:
                cache_cluster_info = client.describe_replication_groups(ReplicationGroupId=replication_group_id)['ReplicationGroups'][0]
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ReplicationGroupNotFoundFault" and desire_no_replication_group:
                    print(" - Replication Group already deleted")
                    return
                else:
                    raise e

            current_status = cache_cluster_info['Status']

            if last_status != current_status:
                last_status = current_status
                print(" - In the state, `%s`" % last_status)

            if current_status in exceptionalStates:
                raise Exception("In a state (%s) that will never reach target state" % current_status)

            if current_status in desiredStatuses:
                return cache_cluster_info

            time.sleep(1)
            sys.stdout.write(".")
            sys.stdout.flush()


    def _wait_for_state_elasticache_cache_clusters(self, clusterIds, desiredStatuses, desire_no_cache_clusters=False):
        client = self._get_boto_client('elasticache')

        last_status = {}
        while True:
            all_cache_clusters = client.describe_cache_clusters()['CacheClusters']

            all_in_desired_status = True
            cc_count = 0
            for cc in all_cache_clusters:
                cc_id = cc['CacheClusterId']
                if cc_id not in clusterIds:
                    continue
                cc_count += 1
                current_status = cc['CacheClusterStatus']
                if current_status not in desiredStatuses:
                    all_in_desired_status = False

                if cc_id not in last_status or last_status[cc_id] != current_status:
                    last_status[cc_id] = current_status
                    print(" - Cluster `%s` in the state, `%s`" % (cc_id, current_status))

            if cc_count < 1:
                if desire_no_cache_clusters:
                    print(" - Cache cluster(s) no longer present")
                    return True
                raise Exception("There are no cache clusters that match.")

            if all_in_desired_status:
                return True

            time.sleep(1)
            sys.stdout.write(".")
            sys.stdout.flush()

    def _play_step_rds(self, step_config):
        """
        Handle playing of RDS steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('rds')
        function_name = step_config['function']

        if function_name == 'custom_db_is_deleted':
            last_status = ""
            while True:
                try:
                    instances = self._get_boto_client('rds').describe_db_instances(
                        DBInstanceIdentifier=arguments['DBInstanceIdentifier']
                    ).get('DBInstances')
                except botocore.exceptions.ClientError as e:
                    if e.response['Error']['Code'] == "DBInstanceNotFound":
                        return
                    else:
                        raise e
                if len(instances) == 0:
                    print("")
                    return

                instance_status = instances[0]['DBInstanceStatus']
                if last_status != instance_status:
                    last_status = instance_status
                    print("In the state, `%s`" % instance_status, end="")

                if instance_status in ['creating', 'available']:
                    raise IncorrectStateException(
                        "RDS is in a state (`%s`) that will never lead to being deleted!" % instance_status
                    )
                time.sleep(1)
                sys.stdout.write(".")
                sys.stdout.flush()

        if function_name == 'custom_db_is_accessible':
            instance_is_accessible = False
            last_status = ""
            endpoint = None
            while not instance_is_accessible:
                instances = self._get_boto_client('rds').describe_db_instances(
                    DBInstanceIdentifier=arguments['DBInstanceIdentifier']
                ).get('DBInstances')

                if len(instances) == 0:
                    raise RDSNotFoundException("Unable to find RDS instance")

                instance_status = instances[0]['DBInstanceStatus']
                if last_status != instance_status:
                    last_status = instance_status
                    print("In the state, `%s`" % instance_status, end="")

                if instance_status in ['deleting']:
                    raise IncorrectStateException(
                        "RDS is in a state (`%s`) that will never lead to being available!" % instance_status
                    )

                if instance_status in ['available', 'modifying']:
                    endpoint = instances[0]['Endpoint']
                    instance_is_accessible = True
                    print("")
                else:
                    time.sleep(2)
                    sys.stdout.write(".")
                    sys.stdout.flush()

            print("Adding endpoint URL to existing stored credentials.")
            for store_location in arguments['stored_credentials_to_update']:
                json_data_str = self.get_rds_credentials_for_locations(store_location)
                json_data = json.loads(json_data_str)
                json_data['connection']['Endpoint'] = endpoint
                self.store_json_in_location(json.dumps(json_data), store_location)
            return

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        client_method(**arguments)

        if function_name == 'create_db_instance' and ('store_credentials_locations' in step_config):

            # Please note that the end-point info is not known at this stage!
            # (whilst creating)
            credentials_info_json = json.dumps({
                'masterUser': {
                    'username': arguments['MasterUsername'],
                    'password': arguments['MasterUserPassword']
                },
                'connection': {
                    'DBInstanceIdentifier': arguments['DBInstanceIdentifier']
                }
            })
            self._store_rds_credentials_in_locations(credentials_info_json, step_config['store_credentials_locations'])

    def _store_rds_credentials_in_locations(self, json_data, store_credentials_locations):
        for store_credential_location in store_credentials_locations:
            self.store_json_in_location(json_data, store_credential_location)

    def store_json_in_location(self, json_data, store_location):
        if store_location['destination'] == 's3':
            arguments = self.replacer(store_location['arguments'])
            print(" - storing the following credentials at s3://" + arguments['Bucket'] + "/" + arguments['Key'])
            arguments['Body'] = json_data
            self._get_boto_client('s3').put_object(**arguments)
            return

        if store_location['destination'] == 'file':
            path = self.replacer(store_location['path'])
            print(" - storing the credentials at file://" + path)
            f = open(path, 'w')
            f.write(json_data)
            f.close()
            return
        raise VPCNotImplementedYet(
            "Have not handled the store_json destination, `%s` yet." % store_location['destination']
        )

    def get_rds_credentials_for_locations(self, store_credential_location):
        if store_credential_location['destination'] == 's3':
            arguments = self.replacer(store_credential_location['arguments'])
            print(" - Retrieving credentials from s3://" + arguments['Bucket'] + "/" + arguments['Key'])
            r = self._get_boto_client('s3').get_object(Bucket=arguments['Bucket'], Key=arguments['Key'])
            return r['Body'].read().decode(encoding='UTF-8')

        if store_credential_location['destination'] == 'file':
            raise ValueError("Destination of file not implemented yet")

    def _play_step_autoscaling(self, step_config):
        """
        Handle playing of Autoscaling steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('autoscaling')
        function_name = step_config['function']

        if function_name == 'create_auto_scaling_group':
            print("PreStep - Creating launch-configuration")
            launch_config_settings = step_config['preCommandStep_launchConfigurationSettings']
            create_lc_arguments = copy.copy(launch_config_settings['arguments'])
            create_lc_arguments = self.replacer(create_lc_arguments)

            lc_name = create_lc_arguments['LaunchConfigurationName']
            m = re.search("^(.*)__LC_NEXT_NUMBER__$", lc_name)
            if m is not None:
                lc_name_prefix = m.group(1)
                next_name = self._autoscale_get_next_name(lc_name_prefix)
                create_lc_arguments['LaunchConfigurationName'] = next_name

            ec2_client = self._get_boto_client('ec2')

            self._autoscale_helper_replace_block_device_mappings(ec2_client, create_lc_arguments)

            client.create_launch_configuration(**create_lc_arguments)
            launch_configuration_name = create_lc_arguments['LaunchConfigurationName']

            print("Creating auto-scale group using created launch-config `" + launch_configuration_name + "`")
            arguments['LaunchConfigurationName'] = launch_configuration_name
            client.create_auto_scaling_group(**arguments)

            print("Creating scaling policies")
            for original_policy_arguments in step_config["postCommandStep_putScalingPolicies"]:
                policy_arguments = copy.copy(original_policy_arguments)
                policy_arguments['AutoScalingGroupName'] = arguments['AutoScalingGroupName']
                print("Creating scaling policy, `%s`.." % policy_arguments['PolicyName'])
                client.put_scaling_policy(**policy_arguments)
            return

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        client_method(**arguments)

    def _autoscale_helper_replace_block_device_mappings(self, ec2_client, desired_create_lc_arguments):
        desired_ami = desired_create_lc_arguments['ImageId']
        ami_block_devices_by_device_name = {
            x['DeviceName']: x for x in ec2_client.describe_images(ImageIds=[desired_ami])['Images'][0]['BlockDeviceMappings']
        }

        def _block_device_item_replacer(r):
            if r['Ebs'].pop("__USE_AMI_DEFAULTS_FOR_DEVICE_NAME__", False):
                device_name = r['DeviceName']
                for ebs_k in ami_block_devices_by_device_name[device_name]['Ebs']:
                    if ebs_k not in r['Ebs']:
                        r['Ebs'][ebs_k] = ami_block_devices_by_device_name[device_name]['Ebs'][ebs_k]

            return r

        new_block_device_mappings = [
            _block_device_item_replacer(r) for r in desired_create_lc_arguments.get('BlockDeviceMappings', [])
        ]

        new_block_device_mappings = list(sorted(new_block_device_mappings, key=lambda x: x['DeviceName']))

        # If any of the block devices are snapshots, None of the block devices can be encrypted.
        if [r for r in new_block_device_mappings if 'SnapshotId' in r['Ebs']]:
            for block_device_map in new_block_device_mappings:
                block_device_map['Ebs']['Encrypted'] = False

        desired_create_lc_arguments['BlockDeviceMappings'] = new_block_device_mappings


    def _revert_step(self, step_config):
        if step_config['client'] in ["ecs", "iam", "organizations", "route53", "s3", "sdb", "ses", "sns", "sqs", "logs"]:
            return super(VPCBuilder, self)._revert_step(step_config)

        if step_config['client'] == 'ec2':
            return self._revert_step_ec2(step_config)

        if step_config['client'] == 'elb':
            return self._revert_step_elb(step_config)

        if step_config['client'] == 'autoscaling':
            return self._revert_step_autoscaling(step_config)

        if step_config['client'] == 'rds':
            return self._revert_step_rds(step_config)

        if step_config['client'] == 'elasticache':
            return self._revert_step_elasticache(step_config)

        if step_config['client'] == 'swf':
            return self._revert_step_swf(step_config)

        if step_config['client'] == 'sdb':
            return self._revert_step_sdb(step_config)

        if step_config['client'] == 'python-exec':
            return self._revert_step_python_exec(step_config)

        raise ValueError("Unknown client, `" + step_config['client'] + "`")

    def _revert_step_ec2(self, step_config):
        function_name = step_config['function']

        # REVERT::ec2::per_az:create_subnet
        if function_name == 'per_az:create_subnet':
            for az_letter in step_config['az_letters_to_create_in']:
                self._az_letter = az_letter
                step_config2 = copy.copy(step_config)
                step_config2['function'] = 'create_subnet'
                step_config2['arguments']['CidrBlock'] = step_config2['CidrBlockPerAZ'][az_letter]
                self._revert_step_ec2(step_config2)
            self._az_letter = None
            return

        if function_name == 'per_az:create_route_table':
            for az_letter in step_config['az_letters_to_create_in']:
                self._az_letter = az_letter
                step_config2 = copy.copy(step_config)
                step_config2['function'] = 'create_route_table'
                self._revert_step_ec2(step_config2)
            self._az_letter = None
            return

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('ec2')

        if function_name == 'create_vpc':
            client.delete_vpc(VpcId=self.vpc_id)
            self.vpc_data = None
            self.vpc_id = None
            return

        if function_name == 'create_internet_gateway':
            internet_gateway_id = self._get_internet_gateway_id()
            if internet_gateway_id is None:
                print("Gateway already removed")
                return

            client.detach_internet_gateway(VpcId=self.vpc_id, InternetGatewayId=internet_gateway_id)
            client.delete_internet_gateway(InternetGatewayId=internet_gateway_id)
            self.internet_gateway_id = None
            return

        if function_name == 'create_subnet':
            if 'prawsResourceReference' not in step_config:
                raise ValueError(
                    "The prawsResourceReference was not added when creating the route table. Cannot verify."
                )
            praws_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
            print(" - looking up subnet with the tag, `PRAWS-VPCBuilder-Reference` set to `%s`" % praws_reference)
            lookup_filters = [{'Name': 'tag:PRAWS-VPCBuilder-Reference', 'Values': [praws_reference]}]
            if 'VpcId' in arguments:
                lookup_filters.append({'Name': 'vpc-id', 'Values': [arguments['VpcId']]})
            r = client.describe_subnets(
                Filters=lookup_filters
            )
            if len(r['Subnets']) == 0:
                print(" - No subnet with the reference exists. assuming already deleted")
                return

            if len(r['Subnets']) > 1:
                raise ValueError("Too many subnets exist with the reference")
                return

            print(" - deleting subnet with reference, `%s`" % praws_reference)
            r = client.delete_subnet(SubnetId=r['Subnets'][0]['SubnetId'])
            return

        if function_name == 'create_route_table':
            if 'prawsResourceReference' not in step_config:
                raise ValueError(
                    "The prawsResourceReference was not added when creating the route table. Cannot revert."
                )

            praws_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
            print(" - looking up route table with the tag, `PRAWS-VPCBuilder-Reference` set to `%s`" % praws_reference)

            lookup_filters = [{'Name': 'tag:PRAWS-VPCBuilder-Reference', 'Values': [praws_reference]}]
            if 'VpcId' in arguments:
                lookup_filters.append({'Name': 'vpc-id', 'Values': [arguments['VpcId']]})

            r = client.describe_route_tables(
                Filters=lookup_filters
            )
            if len(r['RouteTables']) < 1:
                print(" - No route table with the reference exists. assuming already deleted")
                return

            if len(r['RouteTables']) > 1:
                raise ValueError("Too many route table exist with the reference")
                return

            print(" - deleting route table")
            route_table_id = r['RouteTables'][0]['RouteTableId']
            client.delete_route_table(RouteTableId=route_table_id)
            return

        if function_name == 'create_security_group':
            security_groups = client.describe_security_groups(
                Filters=[
                    {'Name': "vpc-id", 'Values': [self.vpc_id]},
                    {'Name': "group-name", 'Values': [arguments['GroupName']]}
                ]).get('SecurityGroups')
            if len(security_groups) == 0:
                print(" - Security group already deleted!")
                return

            security_group = security_groups[0]
            if len(security_group['IpPermissionsEgress']) > 0:
                client.revoke_security_group_egress(
                    GroupId=security_group['GroupId'], IpPermissions=security_group['IpPermissionsEgress']
                )

            if len(security_group['IpPermissions']) > 0:
                client.revoke_security_group_ingress(
                    GroupId=security_group['GroupId'], IpPermissions=security_group['IpPermissions']
                )

            client.delete_security_group(GroupId=security_group['GroupId'])
            return

        # REVERT::ec2:::authorize_security_group_ingress
        if function_name == 'authorize_security_group_ingress':
            try:
                revoke_arguments = self.replacer({
                    'GroupId': step_config['arguments']['GroupId'],
                    'IpPermissions': step_config['arguments']['IpPermissions']
                })
                client.revoke_security_group_ingress(**revoke_arguments)
            except VPCParameterReplacementException:
                print(
                    " - either the security group, or the permissions refer to a security group that is no longer"
                    " present. Likely all has been deleted."
                )
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "InvalidPermission.NotFound":
                    print(" - Permissions already deleted")
                else:
                    raise e
            return

        # REVERT::ec2:::run_instances
        if function_name == 'run_instances':
            if 'prawsResourceReference' not in step_config:
                raise ValueError(
                    "The prawsResourceReference does not exist in the step. Cannot verify."
                )
            praws_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
            lookup_filters = [{'Name':'tag:PRAWS-VPCBuilder-Reference', 'Values': [praws_reference]}]
            lookup_filters.append({'Name':'vpc-id', 'Values':[self.vpc_id]})
            r = client.describe_instances(Filters=lookup_filters)
            if len(r['Reservations']) > 1:
                raise Exception("found more than 1 reservations")

            if len(r['Reservations']) == 0:
                print(" - No reservations found, assuming already deleted")
                return

            if len(r['Reservations'][0]['Instances']) > 1:
                raise Exception("found more than 1 instances")

            if len(r['Reservations'][0]['Instances']) == 0:
                print(" - No instances found, assuming already deleted")
                return

            instance_id = r['Reservations'][0]['Instances'][0]['InstanceId']
            print(" - Terminating instance with id, `%s`." % instance_id)
            client.terminate_instances(InstanceIds=[instance_id])
            return

        if function_name == 'custom_instances_can_exist':
            describe_filters = [
                {'Name': 'vpc-id', 'Values': [self.vpc_id]},
                {'Name': 'instance-state-name', 'Values': ['pending', 'running', 'shuttingdown', 'stopping', 'stopped']}
            ]
            reservations = client.describe_instances(Filters=describe_filters).get('Reservations')
            print(" - Terminating %d instances in the VPC:" % len(reservations))
            instance_ids_to_delete = []
            for reservation in reservations:
                for instance in reservation['Instances']:
                    client.modify_instance_attribute(
                        InstanceId=instance['InstanceId'],
                        DisableApiTermination={'Value': False}
                    )
                    print("   * %s Disabling termination protection " % instance['InstanceId'])
                    instance_ids_to_delete.append(instance['InstanceId'])

            if len(instance_ids_to_delete) > 0:
                client.terminate_instances(InstanceIds=instance_ids_to_delete)
            else:
                print(" - No instances to delete!")
                return

            start_time = time.time()
            this_time = start_time
            instance_count = 99
            max_wait_time = 60
            while (instance_count > 0) and ((this_time - start_time) < max_wait_time):
                instances = client.describe_instances(Filters=describe_filters).get('Reservations')
                instance_count = len(instances)
                this_time = time.time()
                sys.stdout.write(".")
                sys.stdout.flush()

            if instance_count > 0:
                print("WARNING: Instance count is still greater than 0 after %d seconds" % max_wait_time)

            print(" - Sleeping for 40 seconds to ensure instances have release associations")
            time.sleep(40)
            return

        if function_name == 'import_key_pair':
            self._get_boto_client('ec2').delete_key_pair(KeyName=arguments['KeyName'])
            return

        if function_name == 'create_vpc_endpoint':
            describe_filters = []
            describe_filters.append({
                'Name': 'vpc-id',
                'Values': [self.vpc_id]
            })
            describe_filters.append({
                'Name': 'service-name',
                'Values': [arguments['ServiceName']]
            })
            r = client.describe_vpc_endpoints(Filters=describe_filters)

            if len(r['VpcEndpoints']) < 1:
                print("VPC endpoint does not exist.. assuming already deleted")
                return

            if len(r['VpcEndpoints']) > 1:
                raise Exception("Too many VPC endpoints obtained.. not sure what to do")

            r = client.delete_vpc_endpoints(VpcEndpointIds=[r['VpcEndpoints'][0]['VpcEndpointId']])
            print(" - deleted vpc endpoint")
            return

        if function_name == "custom:add_vpc_endpoint_to_route_tables":
            client.modify_vpc_endpoint(
                VpcEndpointId=arguments['VpcEndpointId'],
                RemoveRouteTableIds=arguments['RouteTableIds']
            )
            return

        raise ValueError("Unhandled revert for function ec2.`" + function_name + "`")

    def _revert_step_python_exec(self, step_config):
        """
        Handle playing of EC2 steps.

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)
        arguments['vpc_builder'] = self
        step_config['revert_function'](**arguments)

    def _revert_step_rds(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        client = self._get_boto_client('rds')
        function_name = step_config['function']

        if function_name == 'create_db_subnet_group':
            try:
                client.delete_db_subnet_group(DBSubnetGroupName=arguments['DBSubnetGroupName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "DBSubnetGroupNotFoundFault":
                    print(" - Subgroup already deleted")
                else:
                    raise e
            return

        if function_name == 'create_db_parameter_group':
            try:
                client.delete_db_parameter_group(DBParameterGroupName=arguments['DBParameterGroupName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "DBParameterGroupNotFound":
                    print(" - Parameter already deleted")
                else:
                    raise e
            return

        if function_name == 'create_db_instance':
            r = None
            try:
                r = client.describe_db_instances(DBInstanceIdentifier=arguments['DBInstanceIdentifier'])['DBInstances']
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "DBInstanceNotFound":
                    print(" - DB instance already deleted")
                    return
                else:
                    raise e

            db_status = r[0]['DBInstanceStatus']
            count = 0
            while db_status == 'creating':
                if count == 0:
                    print("The status is `" + db_status + "`, and cannot be deleted yet, waiting...", end="")
                    count += 1
                sys.stdout.write(".")
                sys.stdout.flush()
                time.sleep(1)
                r = client.describe_db_instances(DBInstanceIdentifier=arguments['DBInstanceIdentifier'])['DBInstances']
                db_status = r[0]['DBInstanceStatus']

            try:
                client.delete_db_instance(
                    DBInstanceIdentifier=arguments['DBInstanceIdentifier'],
                    SkipFinalSnapshot=True
                )
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "InvalidDBInstanceState":
                    print("")
                else:
                    raise e
            return

        raise ValueError("Unhandled revert for function rds.`" + function_name + "`")

    def _revert_step_swf(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        client = self._get_boto_client('swf')
        function_name = step_config['function']

        if function_name == 'register_domain':
            print("Refusing to deprecate domain. (it is not possible to undo a SWF domain deprecation)!")
            return

        if function_name == 'register_workflow_type':
            print("Refusing to deprecate workflow type (it is not possible to undo a SWF workflow type deprecation)!")
            return

        raise ValueError("Unhandled revert for function elasticache.`" + function_name + "`")

    def _revert_step_elasticache(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        client = self._get_boto_client('elasticache')
        function_name = step_config['function']

        if function_name == 'create_cache_subnet_group':
            try:
                client.delete_cache_subnet_group(CacheSubnetGroupName=arguments['CacheSubnetGroupName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "CacheSubnetGroupNotFoundFault":
                    print(" - Subnet group already deleted")
                else:
                    raise e
            return

        if function_name == 'per_az:create_cache_cluster':
            for az_letter in step_config['az_letters_to_create_in']:
                fqn_az = self.region_name + az_letter
                arguments['PreferredAvailabilityZone'] = fqn_az
                arguments['CacheClusterId'] = arguments['CacheClusterId'].replace(
                    "__AZ_LETTER__", az_letter
                ).replace("__AZ__", fqn_az)
                try:
                    client.delete_cache_cluster(CacheClusterId=arguments['CacheClusterId'])
                except botocore.exceptions.ClientError as e:
                    if e.response['Error']['Code'] == "CacheClusterNotFound":
                        print(" - Cache cluster already deleted")
                    else:
                        raise e

            return
        if function_name == 'create_replication_group':
            try:
                replication_groups = client.describe_replication_groups(
                    ReplicationGroupId=arguments['ReplicationGroupId']
                )['ReplicationGroups']
                if len(replication_groups) == 0:
                    # Never seen it reach here, exception thrown instead??
                    print(" - Already deleted")
                    return

                if replication_groups[0]['Status'] == 'deleting':
                    print(" - Already in deleting state")
                    return

                client.delete_replication_group(ReplicationGroupId=arguments['ReplicationGroupId'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ReplicationGroupNotFoundFault":
                    print(" - Replication group already deleted.")
                else:
                    raise e
            return

        if function_name == 'create_cache_cluster':
            try:
                cache_cluster_info = client.describe_cache_clusters(
                    CacheClusterId=arguments['CacheClusterId']
                )['CacheClusters'][0]
                if cache_cluster_info['CacheClusterStatus'] in 'deleting':
                    print(" * Already deleting")
                    return
                client.delete_cache_cluster(CacheClusterId=arguments['CacheClusterId'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "CacheClusterNotFound":
                    print(" - Cache Cluster already deleted")
                else:
                    raise e
            return
        if function_name == 'per_az:custom_wait_for_cache_cluster_available':
            clusterIds = []
            for az_letter in step_config['az_letters_to_create_in']:
                fqn_az = self.region_name + az_letter
                cache_cluster_id = arguments['CacheClusterId'].replace(
                    "__AZ_LETTER__", az_letter
                ).replace("__AZ__", fqn_az)
                clusterIds.append(cache_cluster_id)

            return self._wait_for_state_elasticache_cache_clusters(
                clusterIds, ['deleted'], desire_no_cache_clusters=True
            )

        if function_name == 'custom_wait_for_cache_cluster_available':
            clusterIds = [arguments['CacheClusterId']]
            return self._wait_for_state_elasticache_cache_clusters(
                clusterIds, ['deleted'], desire_no_cache_clusters=True
            )

        if function_name == 'custom_wait_for_replication_group_available':
            replicationGroupId = arguments['ReplicationGroupId']

            self._wait_for_state_elasticache_replication_group(
                replicationGroupId,
                ['deleted'],
                desire_no_replication_group=True,
                exceptionalStates=['available']
            )
            if 'dns_to_cname' not in arguments:
                return True

            for dn in arguments['dns_to_cname']:
                dn_one_dot_trail = re.sub('\.+$', '', dn) + "."
                print(" - Setting DN, `%s` with A record to `terminated.resource`" % dn_one_dot_trail)
                # Get the hosted zone
                hosted_zone_id = self._get_hosted_zone_for_dn(dn_one_dot_trail)

                change_batch_changes = [{
                    'Action': 'UPSERT',
                    'ResourceRecordSet': {
                        'Name': dn_one_dot_trail,
                        'Type': 'CNAME',
                        'TTL' : 60,
                        'ResourceRecords': [{'Value': 'terminated.resource'}],
                    }
                }]
                self._get_boto_client('route53').change_resource_record_sets(
                    HostedZoneId=hosted_zone_id,
                    ChangeBatch={
                        'Comment': 'Automated update',
                        'Changes': change_batch_changes
                    }
                )

            return True

        raise ValueError("Unhandled revert for function elasticache.`" + function_name + "`")

    def _revert_step_elb(self, step_config):
        arguments_copy = copy.copy(step_config['arguments'])

        client = self._get_boto_client('elb')
        function_name = step_config['function']

        if function_name == "create_load_balancer":
            load_balancer_name = self.replacer(arguments_copy['LoadBalancerName'])
            client.delete_load_balancer(LoadBalancerName=load_balancer_name)

            if step_config['dns_to_alias'] is not None:
                dns_to_alias = self.replacer(step_config['dns_to_alias'])
                for dn in dns_to_alias:
                    print(
                        " - Updating route53 to point `%s` to 0.0.0.0 with 60 TTL (normally its 500 TTL)" % dn
                    )
                    self._route53_alias_dn_to_null(dn)
            return

    def _revert_step_autoscaling(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        client = self._get_boto_client('autoscaling')
        function_name = step_config['function']

        # REVERT::autoscaling::create_auto_scaling_group
        if function_name == 'create_auto_scaling_group':
            autoscaling_group_info = client.describe_auto_scaling_groups(
                AutoScalingGroupNames=[arguments['AutoScalingGroupName']]
            ).get('AutoScalingGroups')
            if len(autoscaling_group_info) == 0:
                print(" - Does not exist, must already be deleted")
                return

            print(" - Deleting auto-scaling group")
            client.delete_auto_scaling_group(AutoScalingGroupName=arguments['AutoScalingGroupName'], ForceDelete=True)

            m = re.search(
                "^(.*)__LC_NEXT_NUMBER__$",
                step_config['preCommandStep_launchConfigurationSettings']['arguments']['LaunchConfigurationName']
            )
            if m is not None:
                lc_name_prefix = m.group(1)

                print(" - Deleting launch configuration scripts")
                launch_configs = client.describe_launch_configurations().get('LaunchConfigurations')

                for launch_config in launch_configs:
                    if re.search("^" + lc_name_prefix + "\d*", launch_config["LaunchConfigurationName"]) is not None:
                        print(" - Deleting launch-config: %s" % launch_config["LaunchConfigurationName"])
                        client.delete_launch_configuration(
                            LaunchConfigurationName=launch_config["LaunchConfigurationName"]
                        )

            return

        if function_name == 'update_auto_scaling_group':
            if 'reverseArguments' not in step_config:
                raise ValueError("Reverting of 'create_auto_scaling_group' requres reverseArguments to be set")
            arguments = copy.copy(step_config['reverseArguments'])
            arguments = self.replacer(arguments)
            client.update_auto_scaling_group(**arguments)
            return

        raise ValueError("Unhandled revert for function autoscaling.`" + function_name + "`")

    # ==================================== VERIFY STEPS ====================================
    def _verify_step(self, step_config):
        self.clear_logs()
        if step_config['client'] not in ["ec2", "autoscaling"]:
            return super(VPCBuilder, self)._verify_step(step_config)

        if step_config['client'] == 'autoscaling':
            return self._verify_step_autoscaling(step_config)

        if step_config['client'] == 'ec2':
            return self._verify_step_ec2(step_config)

        raise ValueError("Verify step for boto client `%s` not implemented yet" % step_config['client'])

    def _verify_step_ec2(self, step_config):

        client = self._get_boto_client('ec2')
        function_name = step_config['function']

        if function_name == 'per_az:create_route_table':
            for az_letter in step_config['az_letters_to_create_in']:
                self._az_letter = az_letter
                step_config2 = copy.copy(step_config)
                step_config2['function'] = 'create_route_table'
                self._verify_step_ec2(step_config2)
            return

        # VERIFY::ec2::per_az:create_subnet
        if function_name == 'per_az:create_subnet':
            for az_letter in step_config['az_letters_to_create_in']:
                self._az_letter = az_letter
                step_config2 = copy.copy(step_config)
                step_config2['function'] = 'create_subnet'
                step_config2['arguments']['CidrBlock'] = step_config2['CidrBlockPerAZ'][az_letter]
                self._verify_step_ec2(step_config2)
            return

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments, ignore_not_found=True)

        if function_name == 'create_vpc':
            r = self._get_boto_client('ec2').describe_vpcs(Filters=[{'Name': 'tag:Name', 'Values': [self.vpc_name]}])
            if len(r.get("Vpcs")) == 0:
                print("%s VPC does NOT exist" % self.s_cross)
                return
            print("%s VPC does exist" % self.plus)
            if self.show_verify_details:
                pprint(r["Vpcs"][0])
            if r["Vpcs"][0]['CidrBlock'] != arguments['CidrBlock']:
                print("  => %s CidrBlock do NOT match" % self.s_cross)
            else:
                print("  => %s CidrBlock matches" % self.plus)
            return

        if function_name == 'create_internet_gateway':
            r = self._get_boto_client('ec2').describe_internet_gateways(
                Filters=[{'Name': 'attachment.vpc-id', 'Values': [self.vpc_id]}]
            ).get('InternetGateways')
            if len(r) == 0:
                print("%s Gateway does NOT exist" % self.s_cross)
                return
            print("%s Gateway does exists" % self.plus)
            if self.show_verify_details:
                pprint(r[0])
            return

        if function_name == 'create_route_table':
            if 'prawsResourceReference' not in step_config:
                raise ValueError(
                    "The prawsResourceReference was not added when creating the route table. Cannot verify."
                )
            praws_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
            lookup_filters = [{'Name': 'tag:PRAWS-VPCBuilder-Reference', 'Values': [praws_reference]}]
            if 'VpcId' in arguments:
                lookup_filters.append({'Name':'vpc-id', 'Values':[arguments['VpcId']]})
            r = client.describe_route_tables(
                Filters=lookup_filters
            )
            if len(r['RouteTables']) == 0:
                print("%s Route table does NOT exist for praws_reference, `%s`" % (self.s_cross, praws_reference))
                return
            print("%s Route table exists for praws_reference `%s` (not verifying much more yet)" % (self.plus, praws_reference))
            print("%s ToDo - Verify more info about route table" % self.s_info)
            if self.show_verify_details:
                pprint(r['RouteTables'][0])

            return

        if function_name == 'create_subnet':
            if 'prawsResourceReference' not in step_config:
                raise ValueError(
                    "The prawsResourceReference was not added when creating the route table. Cannot verify."
                )
            praws_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
            lookup_filters = [{'Name':'tag:PRAWS-VPCBuilder-Reference', 'Values': [praws_reference]}]
            if 'VpcId' in arguments:
                lookup_filters.append({'Name':'vpc-id', 'Values':[arguments['VpcId']]})
            r = client.describe_subnets(
                Filters=lookup_filters
            )
            if len(r['Subnets']) != 1:
                print("%s Subnet with praws reference `%s` does NOT exist" % (self.s_cross, praws_reference))
                return
            subnet_id = r['Subnets'][0]['SubnetId']

            print("%s Subnet with praws reference `%s` exists" % (self.plus, praws_reference))

            if 'associateWithRouteTable' not in step_config:
                print(
                    "  => %s Not checking the route table association ('associateWithRouteTable' not a step attribute)"
                    % (self.s_info)
                )
            else:
                r2 = client.describe_route_tables(
                    Filters=[
                        {'Name': 'vpc-id', 'Values': [self.vpc_id]},
                        {'Name': 'association.subnet-id', 'Values': [subnet_id]},
                    ]
                )
                if len(r2['RouteTables']) != 1:
                    print("  => %s subnet not associated with any route table." % self.s_cross)
                elif r2['RouteTables'][0]['RouteTableId'] == self.replacer(copy.copy(step_config['associateWithRouteTable']['Id'])):
                    print(
                        "  => %s subnet is associated with correct route table (%s)."
                        % (self.plus, self.replacer(step_config['associateWithRouteTable']['DisplayName']))
                    )
                else:
                    print(
                        "  => %s subnet not associated with correct route table (%s)."
                        % (self.s_cross, self.replacer(step_config['associateWithRouteTable']['DisplayName']))
                    )

            if self.show_verify_details:
                pprint(r['Subnets'][0])

            string_properties_to_compare = ['CidrBlock']
            if 'AvailabilityZone' in arguments:
                string_properties_to_compare.append('AvailabilityZone')
            if 'Ipv6CidrBlock' in arguments:
                string_properties_to_compare.append('Ipv6CidrBlock')

            for string_property in string_properties_to_compare:
                if r["Subnets"][0][string_property] != arguments[string_property]:
                    print("  => - %s do NOT match (%s desired, %s actual value)" % (string_property, arguments[string_property], r["Subnets"][0][string_property]))
                else:
                    print("  => %s %s matches (%s)" % (self.plus, string_property, arguments[string_property]))
            return

        if function_name == 'create_security_group':
            security_groups = client.describe_security_groups(
                Filters=[
                    {'Name': "vpc-id", 'Values': [self.vpc_id]},
                    {'Name': "group-name", 'Values': [arguments['GroupName']]}
                ]).get('SecurityGroups')
            if len(security_groups) == 0:
                print("- Security group does NOT exist")
                return
            print("%s Security group does exist (not verifying much yet)" % self.plus)

            if 'ingress_ip_permissions' in step_config:

                desired_ingress_ip_permissions = _security_group_ingress_cleaner_for_diff(self.replacer(copy.copy(step_config['ingress_ip_permissions'])))
                actual_ingress_ip_permissions = _security_group_ingress_cleaner_for_diff(security_groups[0]['IpPermissions'])

                dd = deepdiff.DeepDiff(desired_ingress_ip_permissions, actual_ingress_ip_permissions, ignore_order=True)
                if (len(dd.keys()) == 0):
                    print("%s ingress_ip_permissions match!" % self.plus)
                else:
                    f1, f2 = self._create_comparison_json_files(
                        "security-group--%s--ingress_ip_permissions" % (arguments['GroupName']),
                        desired_ingress_ip_permissions,
                        actual_ingress_ip_permissions
                    )
                    print("%s match of ingress_ip_permissions is uncertain!\n  - For details, compare:\n     %s\n  vs %s" % (self.s_info, f1, f2))

            return

        # VERIFY::ec2:::run_instances
        if function_name == 'run_instances':
            if 'prawsResourceReference' not in step_config:
                raise ValueError(
                    "The prawsResourceReference does not exist in the step. Cannot verify."
                )
            praws_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
            lookup_filters = [{'Name':'tag:PRAWS-VPCBuilder-Reference', 'Values': [praws_reference]}]
            lookup_filters.append({'Name':'vpc-id', 'Values':[self.vpc_id]})
            r = client.describe_instances(Filters=lookup_filters)
            if len(r['Reservations']) > 1:
                raise Exception("found more than 1 reservations")

            if len(r['Reservations']) == 0:
                print(" %s Instances NOT found" % self.s_cross)
                return

            if len(r['Reservations'][0]['Instances']) > 1:
                raise Exception("found more than 1 instances")

            if len(r['Reservations'][0]['Instances']) == 0:
                print(" %s Instances NOT found" % self.s_cross)
                return

            print(" %s 1 Instance found" % self.plus)
            return


        # VERIFY::ec2:::authorize_security_group_ingress
        if function_name == 'authorize_security_group_ingress':
            try:
                arguments = self.replacer(copy.copy(step_config['arguments']))
            except VPCParameterReplacementException:
                print("%s Unable to perform replacements - Security group likely non-existent" % self.s_cross)
                return

            r = client.describe_security_groups(GroupIds=[arguments['GroupId']])
            security_group = r['SecurityGroups'][0]

            if len(security_group['IpPermissions']) < 1:
                print("%s No IpPermissions exist" % self.s_cross)
                return

            all_present = True
            missing_in_permissions = []
            for desired_security_groups_ingress in arguments['IpPermissions']:
                if not _desired_security_groups_ingress_present(desired_security_groups_ingress, security_group['IpPermissions']):
                    missing_in_permissions.append(desired_security_groups_ingress)
                    all_present = False

            if all_present:
                print("\n%s All ingress permissions added by this step are present.." % self.plus)
            else:
                print("== Missing Permissions ==============")
                pprint(missing_in_permissions)
                print("== Current Permissions ==============")
                pprint(security_group['IpPermissions'])
                print("=====================================")

                print("\n%s Not all ingress permissions added by this step are present.." % self.s_cross)
            print("%s Not ensuring there are not more ingress permissions than desired yet" % self.s_info)
            return

        if function_name == 'import_key_pair':
            try:
                dk = client.describe_key_pairs(KeyNames=[arguments['KeyName']])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "InvalidKeyPair.NotFound":
                    print("%s No keypair present" % self.s_cross)
                    return
                else:
                    raise e

            if len(dk) > 0:
                # Could verify fingerprint via:
                # openssl rsa -in /tmp/aws-keypairs/keypair-maxmine-generic.rsa -pubout -outform DER | openssl md5 -c
                if 'getPublicKeyMaterialFromFile' in step_config:
                    with open(self.replacer(step_config['getPublicKeyMaterialFromFile']), "r") as f:
                        public_key_material = f.read()
                else:
                    public_key_material = arguments['PublicKeyMaterial']

                rsa_pub_key = Crypto.PublicKey.RSA.importKey(public_key_material)

                hex_digest = hashlib.md5(rsa_pub_key.exportKey('DER')).hexdigest()


                if dk['KeyPairs'][0]['KeyFingerprint'].replace(':', '') == hex_digest:
                    print("%s Key exists and finger print matches." % self.plus)
                else:
                    print("%s Key exists but finger print does not matches." % self.s_cross)
            else:
                print("- No keypair present")
            return

        if function_name == 'create_vpc_endpoint':
            describe_filters = []
            describe_filters.append({
                'Name': 'vpc-id',
                'Values': [self.vpc_id]
            })
            describe_filters.append({
                'Name': 'service-name',
                'Values': [arguments['ServiceName']]
            })
            r = client.describe_vpc_endpoints(Filters=describe_filters)

            if len(r['VpcEndpoints']) < 1:
                print(" %s S3 VPC Endpoint does NOT exist." % self.s_cross)
                return
            if len(r['VpcEndpoints']) > 1:
                raise Exception("Too many VPC endpoints obtained.. not sure what to do")
            vpc_endpoint = r['VpcEndpoints'][0]
            print(" %s S3 VPC Endpoint exists." % self.plus)

            dd = deepdiff.DeepDiff(arguments['PolicyDocument'], json.loads(vpc_endpoint['PolicyDocument']), ignore_order=True)
            if (len(dd.keys()) == 0):
                print("   => %s PolicyDocument match!" % self.plus)
            else:
                print("   => - PolicyDocument do NOT match!")
                pprint(dd)
                print("- Current ----------------------------------------")
                pprint(json.loads(vpc_endpoint['PolicyDocument']))
                print("- Desired ----------------------------------------")
                pprint(arguments['PolicyDocument'])
                print("--------------------------------------------------")

            return
        if function_name == "custom:add_vpc_endpoint_to_route_tables":
            if arguments['VpcEndpointId'] is None:
                # Assume the replacement threw an error
                print("%s VPC endpoint does not even exist" % self.s_cross)
                return
            r = client.describe_vpc_endpoints(VpcEndpointIds=[arguments['VpcEndpointId']])
            if len(r['VpcEndpoints']) < 1:
                print("%s VPC endpoint does not even exist" % self.s_cross)
                return
            if len(r['VpcEndpoints']) > 1:
                raise Exception("Too many VPC endpoints obtained.. not sure what to do")
            current_route_table_ids = r['VpcEndpoints'][0]['RouteTableIds']
            desired_but_not_assigned = set(arguments['RouteTableIds']) - set(current_route_table_ids)
            assigned_but_not_requested = set(current_route_table_ids) - set(arguments['RouteTableIds'])
            if len(desired_but_not_assigned) == 0:
                print(" %s Desired route tables are present." % self.plus)
            elif len(desired_but_not_assigned) == len(arguments['RouteTableIds']):
                print(" %s All desired route tables are not present" % self.s_cross)
            else:
                print(" %s !! Some but not all of the desired route tables are related.." % self.s_cross)

            if len(assigned_but_not_requested) > 0:
                pprint(assigned_but_not_requested)
                print(" !!!! Warning, there are other route tables assigned to this endpoint.")
            else:
                print(" . No other route tables are associated.")
            return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")


    def _verify_step_autoscaling(self, step_config):

        ec2_client = self._get_boto_client('ec2')
        client = self._get_boto_client('autoscaling')
        function_name = step_config['function']

        # VERIFY::autoscaling::update_auto_scaling_group
        if function_name == 'update_auto_scaling_group':
            arguments = copy.copy(step_config['arguments'])
            arguments = self.replacer(arguments)

            autoscaling_group_info = client.describe_auto_scaling_groups(
                AutoScalingGroupNames=[arguments['AutoScalingGroupName']]
            ).get('AutoScalingGroups')
            if len(autoscaling_group_info) == 0:
                print(" %s Group does not exist" % self.s_cross)
                return
            print(
                (" %s Instance count: %d" % (self.s_info, len(autoscaling_group_info[0]['Instances'])))
                + (", Max Size: %d" % autoscaling_group_info[0]['MaxSize'])
                + (", Min Size: %d" % autoscaling_group_info[0]['MinSize'])
                + (", Desired Capacity: %d" % autoscaling_group_info[0]['DesiredCapacity'])
            )
            return

        # VERIFY::autoscaling::create_auto_scaling_group
        if function_name == 'create_auto_scaling_group':
            desired_step_details = {'function': function_name}
            current_step_details = {'function': function_name}

            arguments = copy.copy(step_config['arguments'])
            arguments = self.replacer(arguments)

            desired_step_details['arguments'] =  arguments

            autoscaling_group_name = arguments['AutoScalingGroupName']
            matching_autoscaling_groups_info = client.describe_auto_scaling_groups(
                AutoScalingGroupNames=[autoscaling_group_name]
            ).get('AutoScalingGroups')

            if len(matching_autoscaling_groups_info) == 0:
                print(" %s Autoscaling group, `%s` does not exist." % (self.s_cross, autoscaling_group_name))
                return

            current_step_details['arguments'] = copy.copy(matching_autoscaling_groups_info[0])
            desired_step_details['arguments'] = arguments

            # ==================================================
            # 1. Clean up the current step details
            # ==================================================
            if 'DesiredCapacity' not in desired_step_details['arguments']:
                current_step_details['arguments'].pop('DesiredCapacity')

            if 'ServiceLinkedRoleARN' not in desired_step_details['arguments']:
                current_step_details['arguments'].pop('ServiceLinkedRoleARN')

            if  desired_step_details['arguments']['LaunchConfigurationName'] == '__LAUNCH_CONFIG_NAME__':
                desired_step_details['arguments'].pop('LaunchConfigurationName')
                current_step_details['arguments'].pop('LaunchConfigurationName', None)

            for key_to_ignore in ['CreatedTime', 'Instances', 'AutoScalingGroupARN', 'SuspendedProcesses']:
                current_step_details['arguments'].pop(key_to_ignore)
            enabled_metrics = current_step_details['arguments'].pop('EnabledMetrics')
            print(" %s Property, `EnabledMetrics` is not being verified" % (self.s_info))

            if 'VPCZoneIdentifier' in current_step_details['arguments']:
                current_step_details['arguments'].pop('AvailabilityZones')


            # ==================================================
            # 2. Clean up the desired step details
            # ==================================================
            # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/autoscaling.html#AutoScaling.Client.create_auto_scaling_group
            default_create_args = {
                'DefaultCooldown': 300,
                'HealthCheckGracePeriod': 0,
                'LoadBalancerNames': [],
                'NewInstancesProtectedFromScaleIn': False, # Observed (not in docs)
                'TargetGroupARNs': [],
                'TerminationPolicies': ['Default'],
            }
            for k in default_create_args:
                if k not in desired_step_details['arguments']:
                    desired_step_details['arguments'][k] = default_create_args[k]

            def _add_tag_defaults(tag_item):
                tag_item['PropagateAtLaunch'] = tag_item.get('PropagateAtLaunch', True)
                tag_item['ResourceType'] = 'auto-scaling-group'
                tag_item['ResourceId'] = autoscaling_group_name
                return tag_item

            desired_step_details['arguments']['Tags'] = [_add_tag_defaults(x) for x in desired_step_details['arguments'].get('Tags', [])]

            desired_step_details['arguments']['VPCZoneIdentifier'] = ",".join(sorted(desired_step_details['arguments']['VPCZoneIdentifier'].split(",")))
            current_step_details['arguments']['VPCZoneIdentifier'] = ",".join(sorted(current_step_details['arguments']['VPCZoneIdentifier'].split(",")))

            current_step_details['arguments']['Tags'] = list(sorted(current_step_details['arguments']['Tags'], key=lambda x: x['Key']))
            desired_step_details['arguments']['Tags'] = list(sorted(desired_step_details['arguments'].get('Tags', []), key=lambda x: x['Key']))


            # ============================================================
            # 3. Launch Configuration (old)
            # ============================================================
            print(" %s Group does exist" % self.plus)

            m = re.search(
                "^(.*)__LC_NEXT_NUMBER__$",
                step_config['preCommandStep_launchConfigurationSettings']['arguments']['LaunchConfigurationName']
            )
            if m is not None:
                lc_name_prefix = m.group(1)
                launch_configs = client.describe_launch_configurations().get('LaunchConfigurations')
                launch_config_count = 0
                for launch_config in launch_configs:
                    if re.search("^" + lc_name_prefix + "\d*", launch_config["LaunchConfigurationName"]) is not None:
                        launch_config_count += 1
                print(" %s %s launch configurations found with prefix, `%s`" % (self.s_info, launch_config_count, lc_name_prefix))

            # ============================================================
            # 4. Prep the current launch config for comparison
            # ============================================================
            current_launch_configuration_name = matching_autoscaling_groups_info[0]['LaunchConfigurationName']
            print(" %s current_launch_configuration_name = `%s" % (self.s_info, current_launch_configuration_name))
            current_launch_r = client.describe_launch_configurations(LaunchConfigurationNames=[current_launch_configuration_name])

            current_launch_configuration = current_launch_r['LaunchConfigurations'][0]
            current_step_details['preCommandStep_launchConfigurationSettings'] = current_launch_configuration

            for key_to_ignore in ['CreatedTime']:
                current_launch_configuration.pop('CreatedTime')
            current_launch_configuration.pop('LaunchConfigurationARN')
            current_launch_configuration.pop('LaunchConfigurationName')

            current_user_data_str = base64.b64decode(current_launch_configuration.pop('UserData'))
            current_launch_configuration['UserData--AsAList'] = current_user_data_str.decode('ascii').split("\n")

            if len(current_launch_configuration['IamInstanceProfile']) > 0 and 'arn:aws:iam' not in current_launch_configuration['IamInstanceProfile']:
                current_launch_configuration['IamInstanceProfile'] = 'arn:aws:iam::%s:instance-profile/%s' % (self._get_account_id(), current_launch_configuration['IamInstanceProfile'])

            def _block_device_item_default_filler(r):
                """Fills any missing information that AWS might have omitted, but shouldn't have"""
                if 'Encrypted' not in r['Ebs']:
                    r['Ebs']['Encrypted'] = False

                return r

            current_launch_configuration['BlockDeviceMappings'] = [
                _block_device_item_default_filler(r) for r in current_launch_configuration['BlockDeviceMappings']
            ]
            current_launch_configuration['BlockDeviceMappings'] = list(sorted(
                current_launch_configuration['BlockDeviceMappings'], key=lambda x: x['DeviceName']
            ))


            # ============================================================
            # 5. Prep the desired launch config for comparison
            # ============================================================
            launch_config_settings = step_config['preCommandStep_launchConfigurationSettings']
            desired_create_lc_arguments = self.replacer(copy.copy(launch_config_settings['arguments']))
            default_lc_config = {
                'ClassicLinkVPCSecurityGroups': [],
                'EbsOptimized': False,
                'InstanceMonitoring': { 'Enabled': True },
                'KernelId': "",
                'RamdiskId': "",
            }

            for lc_key in default_lc_config:
                if lc_key not in desired_create_lc_arguments:
                    desired_create_lc_arguments[lc_key] = default_lc_config[lc_key]

            self._autoscale_helper_replace_block_device_mappings(ec2_client, desired_create_lc_arguments)

            desired_step_details['preCommandStep_launchConfigurationSettings'] = desired_create_lc_arguments
            desired_create_lc_arguments.pop('LaunchConfigurationName')
            desired_create_lc_arguments['UserData--AsAList'] = desired_create_lc_arguments.pop('UserData').split("\n")

            self._deep_diff_ok_or_file(
                desired_step_details,
                current_step_details,
                ignore_order=True,
                diff_filename_prefix="autoscale-group--%s" % autoscaling_group_name,
                match_message=" %s Autoscale groups settings match" % self.plus,
                mismatch_message=" %s Autoscale groups settings do NOT match" % self.s_cross
            )
            return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")

    # ==================================== UPDATE STEPS ====================================
    def _update_step(self, step_config):
        self.clear_logs()
        if step_config['client'] not in ["autoscaling", "ec2", "elb", "sns", "sqs"]:
            return super(VPCBuilder, self)._update_step(step_config)

        if step_config['client'] == 'autoscaling':
            return self._update_step_autoscaling(step_config)

        if step_config['client'] == 'ec2':
            return self._update_step_ec2(step_config)

        if step_config['client'] == 'elb':
            return self._update_step_elb(step_config)

        else:
            raise ValueError("Unknown client, `" + step_config['client'] + "`")

        raise ValueError("Update step not implemented yet")

    def _update_step_ec2(self, step_config):

        client = self._get_boto_client('ec2')
        function_name = step_config['function']

        # UPDATE::ec2::per_az:create_subnet
        if function_name == 'per_az:create_subnet':
            for az_letter in step_config['az_letters_to_create_in']:
                self._az_letter = az_letter
                step_config2 = copy.copy(step_config)
                step_config2['function'] = 'create_subnet'
                step_config2['arguments']['CidrBlock'] = step_config2['CidrBlockPerAZ'][az_letter]
                self._update_step_ec2(step_config2)
            return

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        # UPDATE::ec2::per_az:create_security_group
        if function_name == 'create_security_group':
            security_group_id = self.replacer('__SECURITY_GROUP__[' + arguments['GroupName'] + ']')

            if 'ingress_ip_permissions' in step_config:

                r = client.describe_security_groups(GroupIds=[security_group_id])
                security_group = r['SecurityGroups'][0]

                print(" - revoking existing ingress (TODO: avoid removing existing that will be later added.)")
                if len(security_group['IpPermissions']) > 0:
                    client.revoke_security_group_ingress(
                        GroupId=security_group['GroupId'], IpPermissions=security_group['IpPermissions']
                    )

                print(" - adding ingress_ip_permissions")
                client.authorize_security_group_ingress(
                    GroupId=security_group_id,
                    IpPermissions=self.replacer(copy.copy(step_config['ingress_ip_permissions']))
                )
            if 'subSteps' in step_config:
                for sub_step in step_config['subSteps']:
                    self.update_step(sub_step, True)
                print(" - WARNING: Only updating the steps as listed above")

            return

        # UPDATE::ec2::create_subnet
        if function_name == 'create_subnet':
            if 'prawsResourceReference' not in step_config:
                raise ValueError(
                    "The prawsResourceReference was not added when creating the route table. Cannot update."
                )
            praws_reference = self.replacer(copy.copy(step_config['prawsResourceReference']))
            lookup_filters = [{'Name':'tag:PRAWS-VPCBuilder-Reference', 'Values': [praws_reference]}]
            if 'VpcId' in arguments:
                lookup_filters.append({'Name':'vpc-id', 'Values':[arguments['VpcId']]})
            r = client.describe_subnets(
                Filters=lookup_filters
            )
            if len(r['Subnets']) != 1:
                raise Exception("Subnet with praws reference `%s` does NOT exist" % praws_reference)

            subnet_id = r['Subnets'][0]['SubnetId']

            if 'associateWithRouteTable' not in step_config:
                print(
                    "  => %s Not checking the route table association ('associateWithRouteTable' not a step attribute)"
                    % (self.s_info)
                )
            else:
                route_table_info = self.replacer(copy.copy(step_config['associateWithRouteTable']))
                print(
                    " - associating subnet having praws reference, `%s` with route table, `%s`"
                    % (praws_reference, self.replacer(route_table_info['DisplayName']))
                )
                client.associate_route_table(SubnetId=subnet_id, RouteTableId=route_table_info['Id'])
            print(" - warning - not updating anything else")
            return

        if function_name == 'create_tags':
            print(" - WARNING - create_tags update has not been implemented yet!")
            return

        if function_name == 'authorize_security_group_ingress':
            r = client.describe_security_groups(GroupIds=[arguments['GroupId']])
            security_group = r['SecurityGroups'][0]

            print(" - Revoking existing ingress (TODO: avoid removing existing that will be later added.)")
            if len(security_group['IpPermissions']) > 0:
                 client.revoke_security_group_ingress(
                     GroupId=security_group['GroupId'], IpPermissions=security_group['IpPermissions']
                 )

            print(" - Adding ingress")
            client.authorize_security_group_ingress(**arguments)
            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")


    def _update_step_autoscaling(self, step_config):

        client = self._get_boto_client('autoscaling')
        function_name = step_config['function']

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        if function_name == 'create_auto_scaling_group':
            print("PreStep - Creating launch-configuration")
            launch_config_settings = step_config['preCommandStep_launchConfigurationSettings']
            create_lc_arguments = copy.copy(launch_config_settings['arguments'])
            create_lc_arguments = self.replacer(create_lc_arguments)

            lc_name = create_lc_arguments['LaunchConfigurationName']
            m = re.search("^(.*)__LC_NEXT_NUMBER__$", lc_name)
            if m is not None:
                lc_name_prefix = m.group(1)
                next_name = self._autoscale_get_next_name(lc_name_prefix)
                create_lc_arguments['LaunchConfigurationName'] = next_name

            ec2_client = self._get_boto_client('ec2')
            self._autoscale_helper_replace_block_device_mappings(ec2_client, create_lc_arguments)

            client.create_launch_configuration(**create_lc_arguments)
            launch_configuration_name = create_lc_arguments['LaunchConfigurationName']

            print("UpdateMain - Updating autoscale group")
            updates_arguments = {
                'AutoScalingGroupName': arguments['AutoScalingGroupName'],
                'LaunchConfigurationName': launch_configuration_name
            }
            potential_arguments = ['AvailabilityZones', 'HealthCheckType', 'VPCZoneIdentifier']
            for arg_key in potential_arguments:
                if arg_key in arguments:
                    updates_arguments[arg_key] = arguments[arg_key]

            client.update_auto_scaling_group(**updates_arguments)

            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def _update_step_elb(self, step_config):

        client = self._get_boto_client('elb')
        function_name = step_config['function']

        arguments = copy.copy(step_config['arguments'])
        arguments = self.replacer(arguments)

        if function_name == 'create_load_balancer':
            print(" - getting load balancer detail.")

            r = self._get_boto_client('elb').describe_load_balancers(LoadBalancerNames=[arguments['LoadBalancerName']])
            load_balancer_details = r['LoadBalancerDescriptions'][0]

            print(" - deleting existing listeners")
            ports = list(map(lambda a:a['Listener']['LoadBalancerPort'], load_balancer_details['ListenerDescriptions']))
            self._get_boto_client('elb').delete_load_balancer_listeners(LoadBalancerName=arguments['LoadBalancerName'], LoadBalancerPorts=ports)
            print(" - adding new listeners")
            self._get_boto_client('elb').create_load_balancer_listeners(LoadBalancerName=arguments['LoadBalancerName'], Listeners=arguments['Listeners'])

            if step_config['LoadBalancerAttributes'] is not None:
                load_balancer_attributes = self.replacer(step_config['LoadBalancerAttributes'])
                arguments_for_modify_attributes = {
                    'LoadBalancerName': arguments['LoadBalancerName'],
                    'LoadBalancerAttributes': load_balancer_attributes
                }
                print(" - setting the attributes of the load balancer")
                self._get_boto_client('elb').modify_load_balancer_attributes(**arguments_for_modify_attributes)

            if step_config['dns_to_alias'] is not None:
                dns_to_alias = self.replacer(step_config['dns_to_alias'])
                for dn in dns_to_alias:
                    print(" - updating route53 to point `%s` to the ELB" % dn)
                    self._route53_alias_dn_to_elb(dn, arguments['LoadBalancerName'])

            print(" - WARNING: Updating of ELB is only partially implemented (listers and DNS alias only), it's actually generally ok to revert and play steps related to ELB's since ECS Service's are linked by ELB's name, and don't need to be re-associated (the just keep working after deleting and restoring ELB's).")
            return
            pprint(arguments)

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def location_for_s3url(self, s3url):
        m = re.search("^s3://([^/]*)/(.*)$", s3url)
        if m is None:
            raise Exception("Reg Exp. not matched.")

        return {
            'destination': 's3',
            'arguments': {
                'Bucket': m.group(1),
                'Key': m.group(2)
            }
        }


def security_group_step_definer(vpc_builder, short_key, description, group_name, ingress_ip_permissions=None):
    """
    Creating security groups can be unnecessarily long.. This is a utility to make this faster.

    :param vpc_builder:
    :param short_key:
    :param description:
    :param group_name:
    :param ingress_ip_permissions:
    :return:
    """
    r = {
        'shortKey': short_key,
        'description': description,
        'client': 'ec2',
        'function': 'create_security_group',
        'arguments': {
            'VpcId': '__VPC_ID__',
            'GroupName': group_name,
            'Description': vpc_builder.vpc_name + ' - ' + description
        },
        'subSteps': [
            {
                'description': 'Create Tags',
                'client': 'ec2',
                'function': 'create_tags',
                'arguments': {
                    'Resources': [
                        '__SECURITY_GROUP__[' + group_name + ']'
                    ],
                    'Tags': [
                        {'Key': 'Name', 'Value': vpc_builder.vpc_name + ' - ' + group_name},
                        {'Key': 'System', 'Value': '__SYSTEM_TAG_VALUE__'}
                    ]
                },
            }
        ]
    }
    if ingress_ip_permissions is not None:
        r['ingress_ip_permissions'] = ingress_ip_permissions

    return r
