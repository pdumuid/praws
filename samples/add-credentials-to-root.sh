#!/bin/bash

LOG_FILE=/var/log/cloudinit.add-credentials-to-root.log

echo "Installing jq / python-pop" | tee -a $LOG_FILE
DEBIAN_FRONTEND=noninteractive apt-get install -y jq python-pip >> $LOG_FILE 2>&1 || exit 1

echo "Installing awscli via pip" >> $LOG_FILE 2>&1
pip install awscli >> $LOG_FILE 2>&1 || exit 1

echo "mkdir /root/.ssh/" >> $LOG_FILE 2>&1
if [ ! -d /root/.ssh/ ]; then
    mkdir /root/.ssh/ >> $LOG_FILE 2>&1
fi

echo "Creating file, /root/.ssh/config" >> $LOG_FILE 2>&1
cat >> /root/.ssh/config <<EOF
host *
     IdentityFile ~/.ssh/deployers_id_rsa
EOF

# dev.befa.com.au
echo "Creating file, /root/.ssh/known_hosts" >> $LOG_FILE 2>&1
# bitbucket
cat >> /root/.ssh/known_hosts <<EOF
bitbucket.org,207.223.240.182 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
EOF

# bitbucket2
cat >> /root/.ssh/known_hosts <<EOF
|1|mTZbmet2BNrR94xv7V4x6ydPFnI=|nZUfeBlUzym3pZ7kRwqMTi5/vbU= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
EOF

S3_PATH_FOR_DEPLOYMENT_PRIVATE_KEY=s3://mybucket/bitbucket-ssh-keys/deployment_id_rsa

echo "Getting files from aws s3" >> $LOG_FILE 2>&1
aws s3 cp $S3_PATH_FOR_DEPLOYMENT_PRIVATE_KEY  /root/.ssh/deployers_id_rsa     >> $LOG_FILE 2>&1 || exit 1

cat > /root/.ssh/deployers_id_rsa.readme <<EOF
This key was retrieved from AWS at $S3_PATH_FOR_DEPLOYMENT_PRIVATE_KEY
EOF

echo "Running Chmod" >> $LOG_FILE 2>&1
chmod 0600 /root/.ssh/deployers_id_rsa >> $LOG_FILE 2>&1
