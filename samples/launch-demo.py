#!/usr/bin/env python3
from __future__ import print_function

import os
import boto3
from six import string_types
import re
from praws.image_launcher import PrawsLauncher
from pprint import pprint
script_dir = os.path.dirname(os.path.realpath(__file__)) + "/"

# The following config will be written as a JSON file at /etc/demo_lvm_config.json on the EC2 instance.
lvm_config = {
    'vg-staging': {
        'devices': ['/dev/xvdg'],
        'logical_volumes': {
            'docker-metadata': {'size': '2GiB'  },
            'docker-data':     {'size': '50GiB' },
            'docker-temp':     {'size': '5GiB',  'mkfs': 'ext4', 'mount_point': '/var/lib/docker/tmp/'},
            'mnt_docker':      {'size': '40GiB', 'mkfs': 'ext4', 'mount_point': '/mnt/docker/'}
        }
    }
}

instance_config = {
    'tags': {
        "Name": "SampleEC2Launch",
    },
    'count': 1,
    "InstanceType": 't2.nano',
    'aws_region': "ap-southeast-2",
    'vpc_id': "__DEFAULT_VPC_ID__",
    "load_paws_cloud_init_first": True,
    'cloud_init_files': [
        script_dir + "instance-config-demo.yaml",
        script_dir + "add-credentials-to-root.sh",
        {
            'script-gen': 'create_json_file',
            'filename': '/etc/demo_lvm_config.json',
            'content': lvm_config
        }
    ],
    'SecurityGroupIds': [
        '__SECURITY_GROUP__[anywhereSSH]',
        '__SECURITY_GROUP__[anywhereWWW]',
    ],
    'ImageId': '__AMI_ID__[CanonicalUser1:*hvm/ubuntu-trusty-14.04-amd64-server*]',
    'IamInstanceProfile': {"Arn":'arn:aws:iam::123456789013:instance-profile/ec2app_sample'},
    'BlockDeviceMappings': [
        {
            'DeviceName': '/dev/sdg',
            'Ebs': {
                'VolumeSize': 100,
                'DeleteOnTermination': True,
                'VolumeType': 'gp2',
                'Encrypted': False
            }
        }
    ]
   }

instance_config['KeyName'] = 'my-sample-key'
instance_config['InstanceType'] = "t2.medium"

pl = PrawsLauncher(instance_config)

pl.launch_instance()

## The following is still a work in progress.
##pl.monitor_instances_ssh('ubuntu-14.04', ['i-9d5e3e1f', 'i-145f3f96'])
