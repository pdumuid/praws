#
# Functions for general aws functions
#

function baws_ask_for_region() {
    echo -e "\n\nPlease select a region:"
    echo "  1 north-virginia   (us-east-1)"
    echo "  2 north-california (us-west-1)"
    echo "  3 oregon           (us-west-2)"
    echo "  4 ireland          (eu-west-1)"
    echo "  5 singapore        (ap-southeast-1)"
    echo "  6 tokyo            (ap-northeast-1)"
    echo "  7 sydney           (ap-southeast-2)"
    echo "  8 sau-paulo        (sa-east-1)"
    echo "  9 frankfurt        (eu-central-1)"
    echo ""
    echo -n "What region? "
    read REGION_KEY

    if   [ "$REGION_KEY" == "1" ]; then AWS_REGION="us-east-1";
    elif [ "$REGION_KEY" == "2" ]; then AWS_REGION="us-west-1";
    elif [ "$REGION_KEY" == "3" ]; then AWS_REGION="us-west-2";
    elif [ "$REGION_KEY" == "4" ]; then AWS_REGION="eu-west-1";
    elif [ "$REGION_KEY" == "5" ]; then AWS_REGION="ap-southeast-1";
    elif [ "$REGION_KEY" == "6" ]; then AWS_REGION="ap-northeast-1";
    elif [ "$REGION_KEY" == "7" ]; then AWS_REGION="ap-southeast-2";
    elif [ "$REGION_KEY" == "8" ]; then AWS_REGION="sa-east-1";
    elif [ "$REGION_KEY" == "9" ]; then AWS_REGION="eu-central-1";
    else
        echo "Sorry, you must specify a region."
        exit 1
    fi
    echo $AWS_REGION
}

function baws_aws_humanName_for_region() {
    if   [ "$1" == "us-east-1" ];      then  AWS_REGION_HUMAN_NAME='north-virginia';
    elif [ "$1" == "us-west-1" ];      then  AWS_REGION_HUMAN_NAME='north-california';
    elif [ "$1" == "us-west-2" ];      then  AWS_REGION_HUMAN_NAME='oregon';
    elif [ "$1" == "eu-west-1" ];      then  AWS_REGION_HUMAN_NAME='ireland';
    elif [ "$1" == "ap-southeast-1" ]; then  AWS_REGION_HUMAN_NAME='singapore';
    elif [ "$1" == "ap-northeast-1" ]; then  AWS_REGION_HUMAN_NAME='tokyo';
    elif [ "$1" == "ap-southeast-2" ]; then  AWS_REGION_HUMAN_NAME='sydney';
    elif [ "$1" == "sa-east-1" ];      then  AWS_REGION_HUMAN_NAME='sau-paulo';
    elif [ "$1" == "eu-central-1" ];   then  AWS_REGION_HUMAN_NAME='frankfurt';
    else
        echo "Sorry, you must specify a known region."
        exit 1
    fi
    echo $AWS_REGION_HUMAN_NAME
}

# USAGE: VPC_ID=vpc-79448f1c AWS_REGION=ap-southeast-1 baws_security_groups_declarer /tmp/x && source /tmp/x || exit 1
function baws_security_groups_declarer() {
    if [ -z "$AWS_REGION" ]; then  echo "AWS_REGION is not define"; return 1; fi
    if [ -z "$VPC_ID"     ]; then  echo "VPC_ID is not defined"   ; return 1; fi
    if [ -z "$1"          ]; then  echo "Arg 1 filename not defined"   ; return 1; fi

    F_OUT2=`mktemp /tmp/suho_dtso_getSG_XXX` || return 1

    aws ec2 describe-security-groups --region $AWS_REGION --filter "Name=vpc-id,Values=$VPC_ID" > $F_OUT2 || return 1
    declare -A SECURITY_GROUPS
    for NAME_AND_ID in `cat $F_OUT2 | jq -r '.SecurityGroups[] | .GroupName + "/" + .GroupId'`; do
        GROUP_NAME=`echo $NAME_AND_ID | sed 's=/.*$=='`
        GROUP_ID=`echo $NAME_AND_ID | sed 's=^.*/=='`
        SECURITY_GROUPS[$GROUP_NAME]="$GROUP_ID"
    done
    declare -p SECURITY_GROUPS > $1 || exit 1
    unlink $F_OUT2
}
