#!/usr/bin/env python2

# largely taken from python examples
# http://docs.python.org/library/email-examples.html

import os
import sys
import smtplib
# For guessing MIME type based on file name extension
import mimetypes

from email import encoders
from email.message import Message
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from optparse import OptionParser
import gzip

from pprint import pprint
import json

COMMASPACE = ', '

starts_with_mappings={
    '#include' : 'text/x-include-url',
    '#!' : 'text/x-shellscript',
    '#cloud-config' : 'text/cloud-config',
    '#cloud-config-archive' : 'text/cloud-config-archive',
    '#upstart-job'  : 'text/upstart-job',
    '#part-handler' : 'text/part-handler',
    '#cloud-boothook' : 'text/cloud-boothook'
}

class FileConversionException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


def get_type(fname, deftype):
    f = open(fname,"rb")
    line = str(f.readline())
    f.close()
    rtype = deftype

    # slist is sorted longest first
    slist = sorted(starts_with_mappings.keys(), key=lambda e: 0-len(e))
    for sstr in slist:
        if line.startswith(sstr):
            rtype = starts_with_mappings[sstr]
            break
    return(rtype)

def handle_dict_file(item):
    if 'script-gen' in item and item['script-gen']=='create_json_file':
        return handle_dict_file_create_json_file(item)
    raise FileConversionException("Unable to handle a dict file.")

def handle_dict_file_create_json_file(item):
    shell_script = "#!/bin/sh"

    if 'mkdir' in item and item['mkdir']:
        shell_script += """

echo Creating the directory for JSON file, """+item['filename']+"""
mkdir -p """+os.path.dirname(item['filename'])+"""

"""

    shell_script += """

echo Creating the JSON file, """+item['filename']+"""
cat <<END_OF_JSON > """+item['filename']+"""
"""+json.dumps(item['content'])+"""
END_OF_JSON
"""
    msg = MIMEText(shell_script, _subtype='x-shellscript')

    json_creator_script_filename = "create_json_file-" + (str.replace(item['filename'], "/", "__")) + ".sh"

    msg.add_header('Content-Disposition', 'attachment', filename=json_creator_script_filename)
    return msg

def convert_files_to_multipart(filenames):
    outer = MIMEMultipart()
    deftype = "text/plain"
    options_delim = ":"

    for item in filenames:
        if isinstance(item, dict):
            msg = handle_dict_file(item)
            outer.attach(msg)
            continue
        else:
            filename = item
        t = filename.split(options_delim, 1)
        path=t[0]
        if len(t) > 1:
            mtype = t[1]
        else:
            mtype = get_type(path, deftype)

        maintype, subtype = mtype.split('/', 1)
        if maintype == 'text':
            fp = open(path)
            # Note: we should handle calculating the charset
            msg = MIMEText(fp.read(), _subtype=subtype)
            fp.close()
        else:
            fp = open(path, 'rb')
            msg = MIMEBase(maintype, subtype)
            msg.set_payload(fp.read())
            fp.close()
            # Encode the payload using Base64
            encoders.encode_base64(msg)

        # Set the filename parameter
        msg.add_header('Content-Disposition', 'attachment',
            filename=os.path.basename(path))

        outer.attach(msg)
    return outer.as_string()

def main():
    outer = MIMEMultipart()
    #outer['Subject'] = 'Contents of directory %s' % os.path.abspath(directory)
    #outer['To'] = COMMASPACE.join(opts.recipients)
    #outer['From'] = opts.sender
    #outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'

    parser = OptionParser()

    parser.add_option("-o", "--output", dest="output",
        help="write output to FILE [default %default]", metavar="FILE",
        default="-")
    parser.add_option("-z", "--gzip", dest="compress", action="store_true",
        help="compress output", default=False)
    parser.add_option("-d", "--default", dest="deftype",
        help="default mime type [default %default]", default="text/plain")
    parser.add_option("--delim", dest="delim",
        help="delimiter [default %default]", default=":")

    (options, args) = parser.parse_args()

    if (len(args)) < 1:
        parser.error("Must give file list see '--help'")

    for arg in args:
        t = arg.split(options.delim, 1)
        path=t[0]
        if len(t) > 1:
            mtype = t[1]
        else:
            mtype = get_type(path,options.deftype)

        maintype, subtype = mtype.split('/', 1)
        if maintype == 'text':
            fp = open(path)
            # Note: we should handle calculating the charset
            msg = MIMEText(fp.read(), _subtype=subtype)
            fp.close()
        else:
            fp = open(path, 'rb')
            msg = MIMEBase(maintype, subtype)
            msg.set_payload(fp.read())
            fp.close()
            # Encode the payload using Base64
            encoders.encode_base64(msg)

        # Set the filename parameter
        msg.add_header('Content-Disposition', 'attachment',
            filename=os.path.basename(path))

        outer.attach(msg)

    if options.output is "-":
        ofile = sys.stdout
    else:
        ofile = file(options.output,"wb")

    if options.compress:
        gfile = gzip.GzipFile(fileobj=ofile, filename = options.output )
        gfile.write(outer.as_string())
        gfile.close()
    else:
        ofile.write(outer.as_string())

    ofile.close()

if __name__ == '__main__':
    main()
