import botocore
import copy
import deepdiff
import json
from pprint import pprint
from praws.utils import as_nice_json
import collections
from praws.exceptions import VPCParameterReplacementException
import re
from praws.route53_utils import get_best_parent_hosted_zone_for_dn


class APIGatewayBuilder:
    def __init__(self, builder):
        self._builder = builder
        self._current_apigateway_id = None
        self._cache_rest_api_gateways = None

        self.log_fail = self._builder.log_fail
        self.log_pass = self._builder.log_pass

    def string_replacer(self, item, ignore_not_found=False):
        Response = collections.namedtuple("Response", "result keep_replacing")

        # S_REPLACE: __CURRENT_APIGATEWAY_ID__ - The current API gateway being created.
        if "__CURRENT_APIGATEWAY_ID__" in item:
            if self._current_apigateway_id is not None:
                item = str.replace(item, "__CURRENT_APIGATEWAY_ID__", self._current_apigateway_id)

        # S_REPLACE: __APIGATEWAY__REST_API_ID__[api_name] - Looks up the rest api id for a given name.
        m = re.search(r"^__APIGATEWAY__REST_API_ID__\[(?P<api_name>.*)\]$", item)
        if m is not None:
            item_parts = m.groupdict()
            api_name = item_parts['api_name']
            api_gateways = self._get_all_rest_api_gateways()
            matching_api_gateways = [x for x in api_gateways if x['name'] == api_name]
            if len(matching_api_gateways) > 1:
                raise VPCParameterReplacementException("Too many API Gateways exist with the same name")

            if len(matching_api_gateways) != 1:
                if ignore_not_found:
                    return Response(item, keep_replacing=False)
                raise VPCParameterReplacementException("API Gateway does not exist")
            return Response(matching_api_gateways[0]['id'], keep_replacing=False)

        # Otherwise..
        return Response(item, True)

    def _play_step(self, step_config):
        """
        Handle playing of API Gateway steps

        :param step_config:
        :return:
        """
        arguments = copy.copy(step_config['arguments'])
        arguments = self._builder.replacer(arguments)

        client = self._builder._get_boto_client('apigateway')
        function_name = step_config['function']

        client_method = getattr(client, function_name)
        if not client_method:
            raise Exception("Method %s not implemented" % function_name)

        # PRE-PLAY::apigateway::create_rest_api
        if function_name == "create_rest_api":
            self._cache_rest_api_gateways = None

            api_gateways = self._get_all_rest_api_gateways()
            matching_api_gateways = [x for x in api_gateways if x['name'] == arguments['name']]
            if len(matching_api_gateways) > 0:
                raise Exception("Gateway with the name, `%s` already exists!" % arguments['name'])

            # The policy often refers to the APIGatway's id (which is not known till after creation)
            create_rest_api_policy = arguments.pop('policy', None)
            if create_rest_api_policy is None:
                print("No policy document")
            else:
                print("Unsetting the policy - it'll be applied later")
            self._cache_rest_api_gateways = None

        r = client_method(**arguments)

        # POST-PLAY::apigateway::create_domain_name
        if function_name == "create_domain_name":
            mappings_by_path = self._builder.replacer(copy.copy(step_config.get('basePathMappingsByPath', {})))
            for base_path in mappings_by_path:
                mapping_info = mappings_by_path[base_path]
                print(f" - creating mapping for `{base_path}` => api:{mapping_info['restApiId']}:stage:{mapping_info['stage']}")
                client.create_base_path_mapping(
                    domainName=arguments['domainName'],
                    restApiId=mappings_by_path[base_path]['restApiId'],
                    stage=mappings_by_path[base_path]['stage'],
                    basePath=base_path
                )

            add_hosted_zone_record = self._builder.replacer(copy.copy(step_config.get('addHostedZoneRecord', False)))
            if not add_hosted_zone_record:
                print("WARNING: `addHostedZoneRecord` is missing, the domain name will have no effect until something points to it.")
            else:
                print(" -  getting desired_nameservers_wanted in_parent_route53_record.")
                if 'functionToGetRoute53ClientForParentHostedZone' in step_config:
                    route53_client = step_config['functionToGetRoute53ClientForHostedZone']()
                else:
                    route53_client = self._builder._get_boto_client('route53')

                self._builder._upsert_route53_record(
                    route53_client,
                    arguments['domainName'],
                    'CNAME',
                    r['regionalDomainName']
                )


        # POST-PLAY::apigateway::create_rest_api
        if function_name == "create_rest_api":
            api_id = r['id']

            # The policy often refers to the APIGatway's id (which is not known till after creation)
            # The replacement of __CURRENT_APIGATEWAY_ID__ is used to address this.
            if create_rest_api_policy is not None:
                # ----------------------------------------
                print("- Setting the API policy")
                # ----------------------------------------
                create_rest_api_policy_str = json.dumps(create_rest_api_policy, sort_keys=True).replace("__CURRENT_APIGATEWAY_ID__", api_id)
                create_rest_api_policy_str = create_rest_api_policy_str.replace('"', '\\"')

                print("- Applying the policy")
                client.update_rest_api(
                    restApiId=api_id,
                    patchOperations=[
                        {
                            'op': 'replace',
                            'path': '/policy',
                            'value': create_rest_api_policy_str
                        }
                    ]
                )

            # ----------------------------------------
            print("- Creating the resources")
            # ----------------------------------------
            # copy but not replaced yet.
            resources_by_path_args = copy.copy(step_config['resourcesByPath'])

            # ensure we have all the paths we need
            def _leading_paths_keys(x):
                path_parts = x.split("/")
                prefix = ""
                path_keys = []
                for path_part in path_parts[1:]:
                    prefix += "/" + path_part
                    path_keys.append(prefix)
                return path_keys

            # Create any missing parent paths
            for r_path in list(resources_by_path_args.keys()):
                for parent_r_path in _leading_paths_keys(r_path):
                    if parent_r_path not in resources_by_path_args:
                        resources_by_path_args[parent_r_path] = {}

            # Create any missing parent paths
            resources_by_depth_and_path_args = {}
            for r_path in list(resources_by_path_args.keys()):
                depth = len([x for x in r_path if x == '/'])
                if depth not in resources_by_depth_and_path_args:
                    resources_by_depth_and_path_args[depth] = {}

                resources_by_depth_and_path_args[depth][r_path] = resources_by_path_args[r_path]

            existing_resource_items = client.get_resources(restApiId=api_id, embed=['methods'])['items']
            existing_resource_items_by_path = {x['path']: x for x in existing_resource_items}

            def _get_parent_path(path):
                parent_path = "/".join(path.split("/")[:-1])
                return "/" if parent_path == "" else parent_path

            for depth in resources_by_depth_and_path_args:
                for r_path in resources_by_depth_and_path_args[depth]:
                    if r_path in existing_resource_items_by_path:
                        continue

                    parent_path = _get_parent_path(r_path)
                    parent_id = existing_resource_items_by_path[parent_path]['id']
                    path_part = r_path.split("/")[-1]
                    print("  * Creating the resource for path, `%s`" % r_path)
                    rc_r = client.create_resource(restApiId=api_id, parentId=parent_id, pathPart=path_part)
                    rc_r.pop('ResponseMetadata')
                    existing_resource_items_by_path[r_path] = rc_r
                    # ----------------------------------------
                    print("- Creating the methods")
                    # ----------------------------------------
                    self._create_methods(
                        client,
                        api_id,
                        resource_info=existing_resource_items_by_path[r_path],
                        desired_resource_methods=resources_by_depth_and_path_args[depth][r_path].get('resourceMethods', {}),
                    )

            if 'primaryDeploymentStage' not in step_config:
                print(" - Warning - The key, `primaryDeploymentStage` is not set - not creating the primary deployment")
            else:
                # ----------------------------------------
                print("- Creating the deployment")
                # ----------------------------------------
                args = {
                    'restApiId': api_id,
                    'description': step_config['primaryDeploymentStage'].get('description', 'PRAWS deployment'),
                }
                d_r = client.create_deployment(**args)
                stage_name = step_config['primaryDeploymentStage']['stageProperties']['stageName']
                args2 = {
                    'restApiId': api_id,
                    'deploymentId': d_r['id'],
                }
                args2.update(self._builder.replacer(copy.copy(step_config['primaryDeploymentStage']['stageProperties'])))
                print("- Creating the stage with name, `%s`." % stage_name)
                stage_method_settings = args2.pop('methodSettings', None)
                stage_access_log_settings = args2.pop('accessLogSettings', None)
                client.create_stage(**args2)


                update_stage_patch_options = []

                method_settings_key_to_path_map = {
                    'metricsEnabled': '/metrics/enabled',
                    'loggingLevel': '/logging/loglevel',
                    'dataTraceEnabled': '/logging/dataTrace',
                    'throttlingBurstLimit': '/throttling/burstLimit',
                    'throttlingRateLimit': '/throttling/rateLimit',
                    'cachingEnabled': '/caching/enabled',
                    'cacheTtlInSeconds': '/caching/ttlInSeconds',
                    'cacheDataEncrypted': '/caching/dataEncrypted',
                    'requireAuthorizationForCacheControl': '/caching/requireAuthorizationForCacheControl',
                    'unauthorizedCacheControlHeaderStrategy': '/caching/unauthorizedCacheControlHeaderStrategy',
                }
                if stage_method_settings is not None:
                    print('- adding methodSettings')
                    for resource_path__http_method in stage_method_settings.keys():
                        method_setting_key = "/" + resource_path__http_method
                        for method_setting_key in stage_method_settings[resource_path__http_method]:
                            value_path = '/' + resource_path__http_method + method_settings_key_to_path_map[method_setting_key]

                            the_value = stage_method_settings[resource_path__http_method][method_setting_key]

                            update_stage_patch_options.append({
                                'op': 'replace',
                                'path': value_path,
                                'value': str(the_value),
                            })


                if stage_access_log_settings is not None:
                    print('- adding accessLogSettings')
                    for k in stage_access_log_settings:
                        update_stage_patch_options.append({
                            'op': 'replace',
                            'path': '/accessLogSettings/' + k,
                            'value': stage_access_log_settings[k]
                        })

                if len(update_stage_patch_options) > 0:
                    print("- Updating the stage having the name, `%s` with properties that cannot be set at create time." % stage_name)
                    try:
                        client.update_stage(
                            restApiId=api_id,
                            stageName=stage_name,
                            patchOperations=update_stage_patch_options
                        )
                    except:
                        print("An error occurred:")
                        pprint(update_stage_patch_options)
                        raise


    def _create_methods(self, client, api_id, resource_info, desired_resource_methods):
        for http_method in desired_resource_methods.keys():
            print(" - %s - Adding method, %s" % (resource_info['path'], http_method))

            kw_args = copy.copy(desired_resource_methods[http_method])
            method_integration = kw_args.pop('methodIntegration', None)
            method_responses = kw_args.pop('methodResponses', {})

            r_m = client.put_method(
                restApiId=api_id,
                resourceId=resource_info['id'],
                #httpMethod=http_method,
                **kw_args
            )

            if method_integration is not None:
                method_integration = self._builder.replacer(copy.copy(method_integration))
                print(" - %s - Adding method, %s - integration" % (resource_info['path'], http_method))
                integration_responses = method_integration.pop('integrationResponses', {})

                if 'httpMethod' in method_integration:
                    integration_http_method = method_integration.pop('httpMethod', None)
                    method_integration['integrationHttpMethod'] = integration_http_method

                try:
                    client.put_integration(
                        restApiId=api_id,
                        resourceId=resource_info['id'],
                        httpMethod=http_method,
                        **method_integration
                    )
                except:
                    print("An error occurred trying to add a method integration.")
                    pprint(method_integration)
                    raise

                for response_code_str in integration_responses.keys():

                    integration_response = integration_responses[response_code_str]
                    if 'application/json' in integration_response.get('responseTemplates', {}) and integration_response['responseTemplates']['application/json'] is None:
                        integration_response['responseTemplates']['application/json'] = ''

                    client.put_integration_response(
                        restApiId=api_id,
                        resourceId=resource_info['id'],
                        httpMethod=http_method,
                        #statusCode=response_code_str,
                        **(integration_responses[response_code_str])
                    )

            for response_code_str in method_responses.keys():
                print(" - %s - Adding method, %s, status code, %s" % (resource_info['path'], http_method, response_code_str))

                client.put_method_response(
                    restApiId=api_id,
                    resourceId=resource_info['id'],
                    httpMethod=http_method,
                    #statusCode=response_code_str,
                    **(method_responses[response_code_str])
                )

    def _verify_step(self, step_config):
        client = self._builder._get_boto_client('apigateway')
        function_name = step_config['function']

        # VERIFY::apigateway::custom:set_account_options
        if function_name == 'custom:set_account_options':
            arguments = copy.copy(step_config['arguments'])
            arguments = self._builder.replacer(arguments)

            get_account_response = client.get_account()
            get_account_response.pop('ResponseMetadata')

            dd = deepdiff.DeepDiff(as_nice_json(arguments), as_nice_json(get_account_response), ignore_order=True)
            if len(dd) == 0:
                self._builder.log_pass("API gateway account options match")
            else:
                self._builder._create_comparison_json_files(
                    "apigateway--account-options", arguments, get_account_response,
                    failure_message="API gateway account options do not match"
                )

            return

        # VERIFY::apigateway::create_rest_api
        if function_name == 'create_rest_api':
            api_gateway_name = self._builder.replacer(copy.copy(step_config['arguments']['name']))

            api_gateways = self._get_all_rest_api_gateways()
            matching_api_gateways = [x for x in api_gateways if x['name'] == api_gateway_name]
            if len(matching_api_gateways) > 1:
                raise Exception("Got too many matching API gateways!")

            if len(matching_api_gateways) == 0:
                self._builder.log_fail("Rest API gateway with name, `%s` does not exist" % api_gateway_name)
                return

            matching_api_gateway = matching_api_gateways[0]
            api_id = matching_api_gateway['id']

            self._builder.log_pass("Rest API gateway with name, `%s` exists with id, `%s`" % (
                api_gateway_name, api_id)
            )

            detailed_api_gateway = client.get_rest_api(restApiId=api_id)

            self._current_apigateway_id = matching_api_gateway['id']
            replaced_steps_config = self._builder.replacer(copy.deepcopy(step_config))
            self._current_apigateway_id = None

            arguments = replaced_steps_config['arguments']

            default_arguments = {
                "apiKeySource": "HEADER",
                "endpointConfiguration": {
                    "types": [
                        "EDGE"
                    ]
                },
                "policy": None,
            }
            desired_arguments = default_arguments
            desired_arguments.update(copy.copy(arguments))

            current_arguments = {
                'name': matching_api_gateway['name'],
                # version                - TODO
                # cloneFrom              - NEVER
                # binaryMediaTypes       - TODO
                # minimumCompressionSize - TODO
                'apiKeySource': matching_api_gateway['apiKeySource'],
                'endpointConfiguration': matching_api_gateway['endpointConfiguration'],
                "policy": json.loads(detailed_api_gateway.get('policy', 'null').replace('\\"', '\"')),  # No idea why we need to replace \\" with \"
                # tags                   - TODO
            }
            if 'description' in matching_api_gateway:
                current_arguments['description'] = matching_api_gateway['description']

            desired_step_args = {'arguments': desired_arguments}
            current_step_args = {'arguments': current_arguments}

            gr_r = client.get_resources(restApiId=matching_api_gateway['id'], embed=['methods'])
            desired_step_args['resourcesByPath'] = replaced_steps_config['resourcesByPath']
            def _simplify_resource(dd):
                r = copy.copy(dd)
                r.pop('id')
                r.pop('parentId', None)
                r.pop('path', None)
                r.pop('pathPart', None)
                return r

            current_step_args['resourcesByPath'] = {
                x['path']: _simplify_resource(x) for x in gr_r['items']
            }

            if 'primaryDeploymentStage' not in step_config:
                print(""" - Warning - The key, `primaryDeploymentStage` is not set - not verifying the primary deployment
                                  - Add "primaryDeploymentStage": { "stageProperties": {"stageName": "default" } }""")
            else:
                print(" - getting deployments")
                desired_primary_deployment_stage = replaced_steps_config['primaryDeploymentStage']
                r_stage = client.get_stage(restApiId=api_id, stageName=desired_primary_deployment_stage['stageProperties']['stageName'])
                #deployments = client.get_deployments(restApiId=api_id)['items']

                # Remove properties that are not checkable.
                [r_stage.pop(x, None) for x in ['ResponseMetadata', 'lastUpdatedDate', 'createdDate', 'cacheClusterStatus']]

                deployment_id = r_stage.pop('deploymentId')

                r_deployment = client.get_deployment(restApiId=api_id, deploymentId=deployment_id)
                [r_deployment.pop(x, None) for x in ['ResponseMetadata', 'createdDate', 'id', 'description']]

                current_primary_deployment_stage = r_deployment
                current_primary_deployment_stage['stageProperties'] = r_stage

                # Defaults
                desired_deployment = copy.deepcopy(desired_primary_deployment_stage)
                desired_stage_properties = desired_deployment.pop('stageProperties')
                # Set the defaults in the stageProperties
                desired_deployment['stageProperties'] = {
                    "cacheClusterEnabled": False,
                    "methodSettings": {},
                }
                desired_deployment['stageProperties'].update(desired_stage_properties)

                current_step_args['primaryDeploymentStage'] = current_primary_deployment_stage
                desired_step_args['primaryDeploymentStage'] = desired_deployment

            self._builder._assert_or_create_comparison_json_files(
                desired_step_args, current_step_args,
                'apigateway-rest-gateway--step-content--%s' % api_gateway_name,
                passing_message='The details of the API match (what we are testing so far)',
                failure_message='The details of the API do not match',
            )
            self._builder.log_info("Warning - There is likely SOO much more that should be validated.. This is just a 'firstCut'")
            return

        # VERIFY::apigateway::create_domain_name
        if function_name == 'create_domain_name':
            arguments = self._builder.replacer(copy.copy(step_config['arguments']))
            try:
                domain_name_r = client.get_domain_name(domainName=arguments['domainName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NotFoundException":
                    self._builder.log_fail(f"The domain name, `{arguments['domainName']}` does not exist.")
                    return
                raise

            args_to_ignore = [
                'ResponseMetadata',
                'certificateUploadDate',
                'domainNameStatus',
                'regionalDomainName',
                'regionalHostedZoneId',
            ]
            regional_domain_name = domain_name_r.pop('regionalDomainName')

            for arg in args_to_ignore:
                domain_name_r.pop(arg, None)

            current_arguments = domain_name_r

            desired_step_args = {'arguments': arguments}
            current_step_args = {'arguments': current_arguments}

            if 'tags' not in desired_step_args['arguments']:
                desired_step_args['arguments']['tags'] = {}

            print("- looking up base path mappings")
            desired_step_args['basePathMappingsByPath'] = self._builder.replacer(copy.copy(step_config.get('basePathMappingsByPath', {})))
            r = client.get_base_path_mappings(domainName=arguments['domainName'])
            current_step_args['basePathMappingsByPath'] = {
                item['basePath']: {'restApiId': item['restApiId'], 'stage': item['stage']}
                for item in r['items']
            }

            self._builder._assert_or_create_comparison_json_files(
                desired_step_args, current_step_args,
                f"apigateway-domain-name----{arguments['domainName']}",
                passing_message=f"The details of the domain name, `{arguments['domainName']}` match (what we are testing so far)",
                failure_message=f"The details of the domain name, `{arguments['domainName']}`  do not match",
            )

            domain_name = arguments['domainName']
            dn_one_dot_trail = re.sub(r'\.+$', '', domain_name) + "."

            add_hosted_zone_record = self._builder.replacer(copy.copy(step_config.get('addHostedZoneRecord', False)))
            if add_hosted_zone_record:
                if 'functionToGetRoute53ClientForParentHostedZone' in step_config:
                    route53_client = step_config['functionToGetRoute53ClientForHostedZone']()
                else:
                    route53_client = self._builder._get_boto_client('route53')

                    self._builder._verify_route53_record(
                        route53_client,
                        arguments['domainName'],
                        'CNAME',
                        regional_domain_name
                    )

            return

        raise ValueError("Unhandled verify for function, `" + function_name + "`")

    def _revert_step(self, step_config):
        arguments = copy.copy(step_config['arguments'])
        arguments = self._builder.replacer(arguments)

        client = self._builder._get_boto_client('apigateway')
        function_name = step_config['function']

        # REVERT::apigateway::create_rest_api
        if function_name == 'create_rest_api':
            api_gateways = self._get_all_rest_api_gateways()
            matching_api_gateways = [x for x in api_gateways if x['name'] == arguments['name']]
            if len(matching_api_gateways) > 1:
                raise Exception("Got too many matching API gateways!")

            if len(matching_api_gateways) == 0:
                print(" - Api gateway with the name, `%s` does not exist." % arguments['name'])
                return

            matching_api_gateway = matching_api_gateways[0]

            print(" - api gateway with the name, `%s` exists, having the id, `%s`." % (arguments['name'], matching_api_gateway['id']))
            print(" - deleting the api gateway with id, `%s`" % matching_api_gateway['id'])
            client.delete_rest_api(restApiId=matching_api_gateway['id'])
            self._cache_rest_api_gateways = None
            return

        # REVERT::apigateway::create_domain_name
        if function_name == 'create_domain_name':
            arguments = copy.copy(step_config['arguments'])
            arguments = self._builder.replacer(arguments)

            print(f" - Deleting domain with the name, `{arguments['domainName']}`")
            try:
                domain_name_r = client.delete_domain_name(domainName=arguments['domainName'])
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "NotFoundException":
                    print(" - previously deleted")
                    return
                raise

            return

        raise ValueError("Unhandled revert for function ecs.`" + function_name + "`")

    def _update_step(self, step_config):
        client = self._builder._get_boto_client('apigateway')
        function_name = step_config['function']

        arguments = copy.copy(step_config['arguments'])
        arguments = self._builder.replacer(arguments)

        # UPDATE::apigateway::create_domain_name
        if function_name == "create_domain_name":
            arguments = copy.copy(step_config['arguments'])
            arguments = self._builder.replacer(arguments)
            domain_name = arguments['domainName']

            r = client.get_base_path_mappings(domainName=domain_name)
            current_base_path_mappings = {item['basePath']: {'restApiId': item['restApiId'], 'stage': item['stage']} for item in r['items']}
            desired_base_path_mappings = self._builder.replacer(copy.copy(step_config.get('basePathMappingsByPath', {})))

            print("- removing, Adding or Updating mappings to match desired.")
            for base_path in current_base_path_mappings:
                if base_path not in desired_base_path_mappings:
                    print(f"- removing base path, `{base_path}`")
                    client.delete_base_path_mapping(domainName=domain_name, basePath=base_path)

            for base_path in desired_base_path_mappings:
                mapping_info = desired_base_path_mappings[base_path]
                if base_path not in current_base_path_mappings:
                    print(f"- Creating mapping for `{base_path}` => api:{mapping_info['restApiId']}:stage:{mapping_info['stage']}")
                    client.create_base_path_mapping(
                        domainName=arguments['domainName'],
                        restApiId=mapping_info['restApiId'],
                        stage=mapping_info['stage'],
                        basePath=base_path
                    )
                elif current_base_path_mappings[base_path] != desired_base_path_mappings[base_path]:
                    print(f"- Updating mapping for `{base_path}` => api:{mapping_info['restApiId']}:stage:{mapping_info['stage']}")
                    client.update_base_path_mapping(
                        domainName=arguments['domainName'],
                        basePath=base_path,
                        patchOperations=[
                            {'op': 'replace', 'path': '/stage', 'value': mapping_info['stage']},
                            {'op': 'replace', 'path': '/restapiId', 'value': mapping_info['restApiId']},
                        ]
                    )

            add_hosted_zone_record = self._builder.replacer(copy.copy(step_config.get('addHostedZoneRecord', False)))
            if add_hosted_zone_record:
                if 'functionToGetRoute53ClientForParentHostedZone' in step_config:
                    route53_client = step_config['functionToGetRoute53ClientForHostedZone']()
                else:
                    route53_client = self._builder._get_boto_client('route53')

                domain_name_r = client.get_domain_name(domainName=arguments['domainName'])

                self._builder._upsert_route53_record(
                    route53_client,
                    domain_name,
                    'CNAME',
                    domain_name_r['regionalDomainName']
                )
            return

        # UPDATE::apigateway::custom:set_account_options
        if function_name == 'custom:set_account_options':
            arguments = copy.copy(step_config['arguments'])
            arguments = self._builder.replacer(arguments)

            get_account_response = client.get_account()
            get_account_response.pop('ResponseMetadata')

            for the_key in arguments:
                dd = deepdiff.DeepDiff(as_nice_json(arguments[the_key]), as_nice_json(get_account_response.get(the_key, None)), ignore_order=True)
                if len(dd) == 0:
                    continue
                print("The key, `%s` differs" % the_key)
                if the_key == 'cloudwatchRoleArn':
                    r = client.update_account(
                        patchOperations=[
                            {
                                'op': 'replace',
                                'path': '/cloudwatchRoleArn',
                                'value': arguments[the_key],
                            }
                        ]
                    )
                else:
                    raise Exception("Unable to update apigateway account setting for `%`" % the_key)
            return

        raise ValueError("Unhandled update for function, `" + function_name + "`")

    def _get_all_rest_api_gateways(self, use_cache=False):
        if self._cache_rest_api_gateways is None or use_cache:
            client = self._builder._get_boto_client('apigateway')

            p = client.get_paginator('get_rest_apis')
            items = []
            [items.extend(x.get('items', [])) for x in p.paginate()]

            self._cache_rest_api_gateways = items

        return self._cache_rest_api_gateways
