
import re


_re_explode_s3_bucket_arn = re.compile(
    "arn:aws:s3:(?P<region_name>[^:]+):(?P<aws_user_id>\d+):(?P<name>[^:/]+)"
)


def explode_s3_bucket_arn(s3_arn, bucket_use):
    m = _re_explode_s3_bucket_arn.match(s3_arn)
    if m is None:
        raise Exception("ARN for %s is not full enough" % bucket_use)
    return m.groupdict()


_re_explode_s3_url = re.compile("s3://(?P<bucket_name>[^/]+)/(?P<object_key>.+)")
def explode_s3_url(s3_url):
    m = _re_explode_s3_url.match(s3_url)
    if m is None:
        raise Exception("s3 URL, `%s` is invalid." % s3_url)
    return m.groupdict()
