
from pprint import pprint
import sys
import time
import json
from praws.utils import as_nice_json


class GenericExecutor:
    def __init__(self):
        self.aws_builders = []
        self.groups_of_steps = []
        self._verification_logs = {}

    def _get_builders_by_short_key(self):
        builders_by_key = {}
        for builder in self.aws_builders:
            for step_info in builder.steps:
                short_key = step_info['shortKey']
                if short_key in builders_by_key:
                    raise Exception("Two of the builders have the same shortkey!")

                builders_by_key[short_key] = builder
        return builders_by_key

    def _get_all_steps_by_key(self):
        return {
            short_key: builder.get_step_for_shortkey(short_key)
            for short_key, builder in self._get_builders_by_short_key().items()
        }

    def show_shortkeys(self):
        steps_by_key = self._get_all_steps_by_key()
        purposes = {}

        max_key_length = max(map(lambda x: len(x), list(steps_by_key.keys())))

        print("The possible short keys are:")
        for step_key in sorted(steps_by_key.keys()):
            print(("  %s - %s") % (step_key.ljust(max_key_length), steps_by_key[step_key].get("purpose", "")))

    def _get_builder_with_shortkey(self, short_key):
        builders_by_short_key = self._get_builders_by_short_key()
        if short_key not in builders_by_short_key:
            raise Exception("Could not find short key, `%s`" % short_key)
        return builders_by_short_key[short_key]

    def play_for_shortkey(self, short_key):
        self._get_builder_with_shortkey(short_key).play_for_shortkey(short_key)

    def verify_for_shortkey(self, short_key):
        builder = self._get_builder_with_shortkey(short_key)
        builder.verify_for_shortkey(short_key)
        self._verification_logs[short_key] = builder.get_logs()

    def update_for_shortkey(self, short_key):
        self._get_builder_with_shortkey(short_key).update_for_shortkey(short_key)

    def revert_for_shortkey(self, short_key):
        self._get_builder_with_shortkey(short_key).revert_for_shortkey(short_key)

    def show_group_of_steps_shortkeys(self, verbose=False):
        print("Available group of steps: =============")

        for groups_of_step in self.groups_of_steps:
            key, group_config = groups_of_step
            print(" * %s - %s" % (key, group_config['description']))
            if not verbose:
                continue
            for step in group_config['steps']:
                print("    - %-6s :: %s" % (step[0], step[1]))

    def play_group_of_steps_for_shortkeys(self, gos_key):
        matching_group_of_steps = list(filter(lambda r: r[0] == gos_key, self.groups_of_steps))
        if len(matching_group_of_steps) is not 1:
            raise Exception("Could not find groups of steps for key, `%s`" % gos_key)

        _, group_config = matching_group_of_steps[0]

        print("Running Groups of steps: %s - %s" % (gos_key, group_config['description']))
        for step in group_config['steps']:
            builder = self._get_builder_with_shortkey(step[1])
            sys.stdout.flush()
            if step[0] == 'play':
                builder.play_for_shortkey(step[1])
            elif step[0] == 'update':
                builder.update_for_shortkey(step[1])
            elif step[0] == 'revert':
                builder.revert_for_shortkey(step[1])
            elif step[0] == 'verify':
                builder.verify_for_shortkey(step[1])
                self._verification_logs[step[1]] = builder.get_logs()
            elif step[0] == 'sleep':
                print("Sleeping for %d seconds" % step[1])
                time.sleep(step[1])

            else:
                raise Exception("Unknown step procedure, `%s`" % step[0])

    def store_verifications(self, filename):
        print(f"Writing verification information to {filename}")

        with open(filename, "w") as f:
            f.write(as_nice_json(self._verification_logs))
