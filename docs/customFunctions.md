
APIGateway::`custom:set_account_options`
--
This custom function is used to ensure that the general global options regarding API gateway can be
tracked.

The 'verify' of this `function_name` will call client.get_account() and compare the settings with what is within the `arguments`
property.

The `play` and `revert` are yet to be implemented, but I'd suggest:
 * Error when wanting to change an un-changable thing (e.g. `apiKeyVersion`)
 * Call `update_account()` for play only on the things that need changing.
 * For `revert` add a `asReverted` state.

(see https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/apigateway.html#APIGateway.Client.get_account)

