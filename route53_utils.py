import re

def get_hosted_zones_for_domain_name(route53_client, dn):   
    hosted_zone_ids = []
    
    hosted_zones_data = route53_client.list_hosted_zones()['HostedZones']
    for hosted_zone_data in hosted_zones_data:
        if hosted_zone_data['Name']  == dn:
            hosted_zone_id = re.sub('^/hostedzone/', '', hosted_zone_data['Id'])
            hosted_zone_ids.append(hosted_zone_id)
    return hosted_zone_ids


def get_best_parent_hosted_zone_for_dn(route53_client, dn):
        hosted_zones = route53_client.list_hosted_zones()['HostedZones']
    
        dn_one_dot_trail = re.sub(r'\.+$', '', dn) + "."

        match_len = 0
        match_hosted_zone = None
        for hosted_zone in hosted_zones:
            name = hosted_zone['Name']
            name_len = len(name)
            if dn_one_dot_trail.endswith(name) and name_len > match_len:
                match_len = name_len
                match_hosted_zone = re.sub(r'^/hostedzone/', '', hosted_zone['Id'])

        return match_hosted_zone
